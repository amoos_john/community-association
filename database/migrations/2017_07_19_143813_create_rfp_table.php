<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('name');
            $table->Integer('assocation_id');
            $table->string('location');
            $table->string('submission_date',20);
            $table->string('point_contact');
            $table->string('point_contact_phone');
            $table->text('job_description');
            $table->text('requirement');
            $table->text('project_specification');
            $table->string('image_name');
            $table->string('image');
            $table->string('image_name2');
            $table->string('image2');
            $table->string('document_name');
            $table->string('document');
            $table->Integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rfp');
    }
}
