<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotional', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('user_id');
            $table->Integer('serviceprovider_id');
            $table->string('category');
            $table->string('areas');
            $table->string('title');
            $table->string('contact_name');
            $table->string('company_name');
            $table->string('phone');
            $table->string('start_date',20);
            $table->string('end_date',20);
            $table->string('pkg_inclusion');
            $table->text('comments');
            $table->Integer('status');
            $table->Integer('deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('promotional');
    }
}
