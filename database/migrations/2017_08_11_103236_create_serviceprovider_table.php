<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceproviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_provider', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('user_id');
            $table->Integer('status');
            $table->string('name');
            $table->string('address1');
            $table->string('address2');
            $table->string('state');
            $table->string('city');
            $table->string('zip_code');
            $table->string('phone');
            $table->string('emergency_phone');
            $table->string('fax');
            $table->string('website_url');
            $table->text('description');
            $table->string('license_name');
            $table->string('license');
            $table->string('certificate_name');
            $table->string('certificate');
            $table->string('reference_name');
            $table->string('reference');
            $table->string('document_name');
            $table->string('document');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_provider');
    }
}
