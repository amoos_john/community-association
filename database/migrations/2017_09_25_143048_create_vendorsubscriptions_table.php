<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_subscriptions', function (Blueprint $table) {            
            $table->increments('id');
            $table->integer('package_id');
            $table->integer('billing_id');
            $table->integer('user_id');
            $table->string('type',20);
            $table->tinyInteger('subscription_status');
            $table->double('subscription_rate' ,8, 2);
            $table->date('subscription_date');
            $table->tinyInteger('deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vendor_subscriptions');
    }
}
