<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionaluserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_user', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('association_id');
            $table->string('name');
            $table->string('email');
            $table->string('username');
            $table->string('phone');
            $table->string('association_name');
            $table->string('position');
            $table->string('add_user',20);
            $table->string('status',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('additional_user');
    }
}
