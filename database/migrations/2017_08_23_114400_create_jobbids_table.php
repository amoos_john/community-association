<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobbidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_bids', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('job_id');
            $table->Integer('user_id');
            $table->Integer('serviceprovider_id');
            $table->string('email');
            $table->string('message');
            $table->string('document');
            $table->Integer('status');
            $table->Integer('deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_bids');
    }
}
