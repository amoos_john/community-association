<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagecombinationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_combination', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('billing_id');
            $table->Integer('user_id');
            $table->string('service_areas');
            $table->string('states');
            $table->text('county');
            $table->Integer('gallery');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('package_combination');
    }
}
