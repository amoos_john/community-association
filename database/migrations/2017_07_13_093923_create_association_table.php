<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssociationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('association', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('user_id');
            $table->Integer('package_id');
            $table->string('name');
            $table->string('state');
            $table->string('city');
            $table->string('zip_code');
            $table->string('contact_person');
            $table->string('position');
            $table->string('phone1');
            $table->string('phone2');
            $table->string('email');
            $table->string('advertisement');
            $table->string('website_url');
            $table->string('address'); 
            $table->string('google_map');
            $table->string('no_of_bulidings');
            $table->string('no_of_units');
            $table->string('association_type');
            $table->Integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('association');
    }
}
