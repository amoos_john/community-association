<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssociationsubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('association_subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('package_id');
            $table->Integer('billing_id');
            $table->Integer('user_id');
            $table->Integer('total_rfp');
            $table->Integer('subscription_status');
            $table->Integer('subscription_rate');
            $table->Integer('subscription_date');
            $table->Integer('status');
            $table->Integer('deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('association_subscriptions');
    }
}
