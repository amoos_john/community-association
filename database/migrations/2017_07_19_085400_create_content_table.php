<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type',10);
            $table->string('code',30);
            $table->string('url');
            $table->string('title');
            $table->string('subject');
            $table->text('body');
            $table->string('image');
            $table->string('metaTitle');
            $table->string('metaDescription');
            $table->string('keywords');
            $table->Integer('status');
            $table->Integer('deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('content');
    }
}
