@extends('admin/admin_template')

@section('content')
<style>
    .file-drop-area.fnc-uplaod {
    float: left;
}
img.img-responsive.img-thumbnail {
    width: 230px;
    height: 180px;
}
</style>
<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
     
        <div class="box-header with-border">
          <h3 class="box-title">Vendor Details</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
<div class="form-group">
<a href="{{ url('admin/vendors/edit/'.$id) }}" class="btn btn-primary">Edit</a>
<a href="{{ url('admin/vendors') }}" class="btn btn-danger">Cancel</a>
</div>
<div class="form-group col-sm-12">
<h2>Vendor Profile</h2>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('First Name:') !!}
    <p>{{ $model->users->first_name }}</p>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Last Name:') !!}
    <p>{{ $model->users->last_name }}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('Phone:') !!}  
    <p>{{ $model->users->phone }}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('Website URL:') !!}  
    <p>{{ $model->users->website_url }}</p>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Email:') !!}
    <p>{{ $model->users->email }}</p>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Login Name:') !!}
    <p>{{ $model->users->name }}</p>
</div>

<div class="form-group col-sm-12">
<h2>Vendor Information</h2>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Service Provider Name:') !!}
    <p>{{ $model->name }}</p>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Status:') !!}
   <p>{{ ($model->status!=0)?'Active':'Inactive' }}</p>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Address 1') !!}
    <p><?php echo $model->address; ?></p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('Address 2') !!}  
    <p><?php echo $model->address2; ?></p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('City') !!}  
    <p>{{ $model->city }}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('State') !!}  
    <p>{{ $model->state }}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('Zipcode:') !!}  
    <p>{{ $model->zip_code }}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('Office Phone:') !!} 
    <p>{{ $model->phone }}</p>
</div>

<div class="form-group col-md-6">
    {!! Form::label('Emergency Phone:') !!} 
    <p>{{ $model->emergency_phone }}</p>
</div>

<div class="form-group col-md-6">
    {!! Form::label('Fax:') !!}  
    <p>{{ $model->fax }}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('Description of Business') !!}
   <p><?php echo $model->description; ?></p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('Website URL:') !!}  
    <p>{{ $model->website_url }}</p>
</div>
            
<div class="form-group col-md-12">
    {!! Form::label('Logo') !!}
    <div class="clearfix"></div>
    <img id="imagePreview" class="img-responsive img-thumbnail" src="{{ asset('/'.$model->logo)}}" alt="Logo" />
</div>
<div class="form-group col-sm-12">
   {!! Form::label('License # 1') !!}
   <p>{{ $model->license_name }}</p>
   @if($model->license!='')
   <a href="{{ asset('/'.$model->license) }}" class="btn bg-navy btn-flat margin" target="_blank">
       <i class="fa fa-download"></i> View</a>
   @endif
</div>
<div class="form-group col-sm-12">
   {!! Form::label('Certificate of Insurance # 2') !!}
   <p>{{ $model->certificate_name }}</p>
   @if($model->certificate!='')
   <a href="{{ asset('/'.$model->certificate) }}" class="btn bg-navy btn-flat margin" target="_blank">
       <i class="fa fa-download"></i> View</a>
   @endif
</div>
<div class="form-group col-sm-12">
   {!! Form::label('References PDF # 3') !!} 
   <p>{{ $model->reference_name }}</p>
   @if($model->reference!='')
   <a href="{{ asset('/'.$model->reference) }}" class="btn bg-navy btn-flat margin" target="_blank">
       <i class="fa fa-download"></i> View</a>
   @endif
</div>
<div class="form-group col-sm-12">
   {!! Form::label('Other documents # 4') !!}
   <p>{{ $model->document_name }}</p>
   @if($model->document!='')
   <a href="{{ asset('/'.$model->document) }}" class="btn bg-navy btn-flat margin" target="_blank">
       <i class="fa fa-download"></i> View</a>
   @endif
</div>
     
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>
@endsection