<?php
$required="required";
?>
@include('admin/commons/errors')
<div class="form-group col-sm-12">
<h2>Vendor Profile</h2>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('First Name:') !!}
    {!! Form::text('first_name', $model->users->first_name , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Last Name:') !!}
    {!! Form::text('last_name', $model->users->last_name , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Phone:') !!}  
    {!! Form::text('user_phone', $model->users->phone , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Website URL:') !!}  
    {!! Form::url('user_website_url', $model->users->website_url , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Email:*') !!}
    {!! Form::email('email', $model->users->email , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Login Name:*') !!}
    {!! Form::text('login_name', $model->users->name , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-sm-12">
<em class="text-aqua">If you don't want to change password... please leave them empty</em>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Password') !!}
    {!! Form::password('password' , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Confirm Password') !!}
    {!! Form::password('password_confirmation' , array('class' => 'form-control') ) !!}
</div>

<div class="form-group col-sm-12">
<h2>Vendor Information</h2>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Service Provider Name:*') !!}
    {!! Form::text('name', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Status:*') !!}
    {!! Form::select('status', $status,null , array('class' => 'form-control',$required) ) !!}
   
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Address 1:*') !!}
    {!! Form::text('address', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Address 2:') !!}  
    {!! Form::text('address2', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Select State:') !!}  
    {!! Form::select('state',$states,null , array('class' => 'form-control','onChange' => 'getCities(this.value);') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Select City:') !!}
    <select name="city" id="selectCity" class="form-control">
    </select>
</div>
<div class="form-group col-md-6">
    {!! Form::label('Zipcode:') !!}  
    {!! Form::text('zip_code', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Office Phone:') !!}  
    {!! Form::text('phone', null , array('class' => 'form-control',$required) ) !!}
</div>

<div class="form-group col-md-6">
    {!! Form::label('Emergency Phone:') !!}  
    {!! Form::text('emergency_phone', null , array('class' => 'form-control') ) !!}
</div>

<div class="form-group col-md-6">
    {!! Form::label('Fax:') !!}  
    {!! Form::text('fax', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Description of Business') !!}
    {!! Form::textarea('description', null, ['size' => '105x3','class' => 'form-control']) !!} 
</div>

<div class="form-group col-md-6">
    {!! Form::label('Website URL:') !!}  
    {!! Form::url('website_url', null , array('class' => 'form-control') ) !!}
</div>
<div class="clearfix"></div>
<div class="form-group col-md-6">
    {!! Form::label('Logo') !!}
    {!! Form::file('logo', null,array('class'=>'form-control','accept'=>'image/*')) !!}
</div>
<div class="form-group col-md-6">
    <img id="imagePreview" class="img-responsive img-thumbnail" src="{{ asset('/'.$model->logo)}}" alt="Logo" />
</div>
<div class="form-group col-sm-12">
   {!! Form::label('License # 1') !!}
   {!! Form::text('license_name', null , array('class' => 'form-control','placeholder' => 'License # 1') ) !!}
   {!! Form::file('license', null,array('class'=>'form-control')) !!} 
   @if($model->license!='')
   <a href="{{ asset('/'.$model->license) }}" class="btn bg-navy btn-flat margin" download>
       <i class="fa fa-download"></i> Download</a>
   @endif
</div>
<div class="form-group col-sm-12">
   {!! Form::label('Certificate of Insurance # 2') !!}
   {!! Form::text('certificate_name', null , array('class' => 'form-control','placeholder' => 'Certificate of Insurance # 2') ) !!}
   {!! Form::file('certificate', null,array('class'=>'form-control')) !!}  
   @if($model->certificate!='')
   <a href="{{ asset('/'.$model->certificate) }}" class="btn bg-navy btn-flat margin" download>
       <i class="fa fa-download"></i> Download</a>
   @endif
</div>
<div class="form-group col-sm-12">
   {!! Form::label('References PDF # 3') !!} 
   {!! Form::text('reference_name', null , array('class' => 'form-control','placeholder' => 'References PDF # 3') ) !!}
   {!! Form::file('reference', null,array('class'=>'form-control')) !!}  
   @if($model->reference!='')
   <a href="{{ asset('/'.$model->reference) }}" class="btn bg-navy btn-flat margin" download>
       <i class="fa fa-download"></i> Download</a>
   @endif
</div>
<div class="form-group col-sm-12">
   {!! Form::label('Other documents # 4') !!} 
   {!! Form::text('document_name', null , array('class' => 'form-control','placeholder' => 'Other documents # 4') ) !!}
   {!! Form::file('document', null,array('class'=>'form-control')) !!}  
   @if($model->document!='')
   <a href="{{ asset('/'.$model->document) }}" class="btn bg-navy btn-flat margin" download>
       <i class="fa fa-download"></i> Download</a>
   @endif
</div>


