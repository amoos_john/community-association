@extends('admin/admin_template')
@section('content')
<!-- Main row -->
<?php

use App\Functions\Functions;

if (isset($query)) {
    $link = str_replace("search/?", "search?", $model->appends($query)->render());
} else {
    $link = $model->render();
}
?>
<section class="content-header">
    <h1>All Users</h1>
</section>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Search</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
                </div>
            </div>
            {!! Form::open(array( 'class' => 'form','url' => 'admin/vendors/users/search', 'method' => 'get')) !!}


            <div class="box-body">

                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('First Name') !!}
                            {!! Form::text('first_name',(isset($first_name))?$first_name:'' , array('class' => 'form-control') ) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('Last Name') !!}
                            {!! Form::text('last_name',(isset($last_name))?$last_name:'' , array('class' => 'form-control') ) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('Email') !!}
                            {!! Form::text('email',(isset($email))?$email:'' ,['class' => 'form-control']) !!} 
                        </div>
                    </div>
                    <div class='clearfix'></div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('Vendor Name') !!}
                            {!! Form::select('vendor_id',$vendors,(isset($vendor_id))?$vendor_id:'' ,['class' => 'form-control']) !!} 
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('Status') !!}
                            {!! Form::select('status',$status,(isset($status_id))?$status_id:'' ,['class' => 'form-control']) !!} 
                        </div>
                    </div>


                    <div class='clearfix'></div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Search</button>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <a href="{{ url('admin/vendors/users') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="row">
    <!-- Left col -->
    <div class="col-md-12">
        <!-- PRODUCT LIST -->
        <div class="box box-primary">

            @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="icon fa fa-check"></i> {!! session('success') !!}
            </div>
            @endif
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email Address</th>
                            <th>Vendor Name</th>
                            <!--<th>Register</th>
                            <th>Pwd Reset</th>-->
                            <th>Login Date</th>
                            <th>Modified Date</th>
                            <th>Created By</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($model->count()>0)
                        <?php $i = 1; ?>
                        @foreach ($model as $row)
                        <?php
                        if ($row->status == '1') {
                            $color = 'btn btn-success btn-sm';
                            $status = 'Active';
                            $title = 'Click to inactive this user';
                        } elseif ($row->status == '0') {
                            $color = 'btn btn-danger btn-sm';
                            $status = 'Inactive';
                            $title = 'Click to active this user';
                        }
                        $last_login = '';
                        if ($row->user->last_login != '') {
                            $last_login = $row->user->last_login;
                        }
                        ?>
                        <tr>
                            <td>{{ $row->id }}</td>
                            <td>{{ $row->first_name }}</td>
                            <td>{{ $row->last_name }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ (count($row->service_provider)>0)?$row->service_provider->name:'' }}</td>
                            <td>{{ ($last_login!='')?Functions::dateTimeFormat($last_login):'' }}</td>
                            <td>{{ Functions::dateTimeFormat($row->updated_at) }}</td>
                            <td>{{ (count($row->modified)>0)?$row->modified->name:'' }}</td>
                            <td><a data-href="<?php echo url('admin/vendors/users/status/' . $row->id); ?>" class="{{ $color }}" title="{{ $title }}" data-target="#confirm-status" data-toggle="modal">
                                    <i class="fa fa-user"></i>  <?php echo $status; ?></a></td>                            <td>
                                <a data-href="<?php echo url('admin/vendors/users/delete/' . $row->id); ?>" data-target="#confirm-delete"  class="btn btn-danger btn-sm"  data-toggle="modal" title="Delete"><i class="fa fa-trash"></i> </a>
                            </td> 
                        </tr>
                        <?php $i++; ?>
                        @endforeach   
                        @else
                        <tr>
                            <td colspan="7" class="text-center">Data not found!</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
                <?php echo $link; ?>
                <div><?php echo "Showing " . $model->count() . " of " . $model->total(); ?></div>
            </div>
        </div>

        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                    </div>

                    <div class="modal-body">
                        <p>Are you sure to delete this user?</p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-danger btn-ok" id="btn-ok">Delete</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confirm-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Change Status</h4>
                    </div>

                    <div class="modal-body">
                        <p>Are you sure to change status for this user?</p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <a class="btn btn-success btn-yes" id="btn-yes">Yes</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->	

@endsection