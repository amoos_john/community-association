@extends('admin/admin_template')

@section('content')
<style>
    .file-drop-area.fnc-uplaod {
    float: left;
}
img.img-responsive.img-thumbnail {
    width: 230px;
    height: 180px;
}
textarea.form-control {
    height: 200px;
    resize: none;
}
</style>
<script>

var loadImageFile = (function () 
{
    if (window.FileReader) {
        var   oPreviewImg = null, oFReader = new window.FileReader(),
            rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

       
        return function () {
            var aFiles = document.getElementById("logo").files;
            if (aFiles.length === 0) { return; }
            if (!rFilter.test(aFiles[0].type)) { 
                alert("You must select a valid image file !");
                var $el = $('#logo');
                $el.wrap('<form>').closest('form').get(0).reset();      

                return; 
            }
            
        }
        


    }

})();

</script>
<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
		@endif
        <div class="box-header with-border">
          <h3 class="box-title">Edit Vendor</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! Form::model($model, ['files' => true,'class' => 'form','url' => ['admin/vendors/update', $model->id], 'method' => 'post']) !!}
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
            <a href="{{ url('admin/vendors') }}" class="btn btn-danger">Cancel</a>
            </div>
            <!-- text input -->
            <?php $page='admin.vendors.form'?>
            @include($page)
         
          <!-- nav-tabs-custom -->
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>
<script>
    getCities('<?php echo $model->state;?>','<?php echo $model->city;?>');
    function getCities(code,city = 0)
    {
        
    var url = 'state_code=' + code +'&city=' + city;
    $.ajax({
    type: "GET",
            url: "{{ url('admin/get/cities') }}",
            data: url,
            success: function(data){
            $('#selectCity').html(data);
            },
            error: function(errormessage) {
            //you would not show the real error to the user - this is just to see if everything is working
            alert(errormessage);
            }
    });
    }</script>
@endsection