<?php
$required="required";
?>
@include('admin/commons/errors')
<div class="form-group col-sm-12">
<h2>Association Profile</h2>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Name:*') !!}
    {!! Form::text('login_name', $model->users->name , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Email:*') !!}
    {!! Form::email('user_email', $model->users->email , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Phone:') !!}  
    {!! Form::tel('user_phone', $model->users->phone , array('class' => 'form-control') ) !!}
</div>

<div class="form-group col-sm-12">
<em class="text-aqua">If you don't want to change password... please leave them empty</em>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Password') !!}
    {!! Form::password('password' , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Confirm Password') !!}
    {!! Form::password('password_confirmation' , array('class' => 'form-control') ) !!}
</div>

<div class="form-group col-sm-12">
<h2>Association Information</h2>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Name:*') !!}
    {!! Form::text('name', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Status:*') !!}
    {!! Form::select('status', $status,null , array('class' => 'form-control',$required) ) !!}
   
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Association Email:') !!}
    {!! Form::email('email', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Zipcode:') !!}  
    {!! Form::text('zip_code', null , array('class' => 'form-control') ) !!}
</div>

<div class="form-group col-md-6">
    {!! Form::label('Select State:') !!}  
    {!! Form::select('state',$states,null , array('class' => 'form-control','onChange' => 'getCities(this.value);') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Select City:') !!}
    <select name="city" id="selectCity" class="form-control">
    </select>
</div>

<div class="form-group col-md-6">
    {!! Form::label('Contact Person:') !!}  
    {!! Form::text('contact_person', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Organization Position:') !!}  
    {!! Form::text('position', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Phone:*') !!}  
    {!! Form::tel('phone', null , array('class' => 'form-control',$required) ) !!}
</div>

<div class="form-group col-md-6">
    {!! Form::label('Phone2:') !!}  
    {!! Form::tel('phone2', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Website URL:') !!}  
    {!! Form::url('website_url', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Physical Address:') !!}  
    {!! Form::text('address', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Google Map Link:') !!}  
    {!! Form::text('google_map', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Number of Buildings:') !!}  
    {!! Form::text('no_of_bulidings', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Number of Units:') !!}  
    {!! Form::text('no_of_units', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Types of Association:') !!}  
    {!! Form::text('association_type', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-sm-12">

<label>Do you wish to receive Service Provider Advertisement</label>
<label>
  <input type="radio" name="advertisement" value="1" class="minimal" <?php echo ($model->advertisement==1)?'checked':'' ;?> />
  YES
</label>
<label>
  <input type="radio" name="advertisement" value="0" class="minimal" <?php echo ($model->advertisement==0)?'checked':'' ;?> />
  NO
</label>

</div>


