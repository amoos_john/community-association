@extends('admin/admin_template')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->

        <!-- /.box -->
        <!-- general form elements disabled -->
        <div class="box box-warning">

            <div class="box-header with-border">
                <h3 class="box-title">Association Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <a href="{{ url('admin/association/edit/'.$id) }}" class="btn btn-primary">Edit</a>
                    <a href="{{ url('admin/association') }}" class="btn btn-danger">Cancel</a>
                </div>
                <div class="form-group col-sm-12">
                    <h2>Association Profile</h2>
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('Name:') !!}
                    <p>{{ $model->users->name }}</p> 
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('Email:') !!}
                    <p>{{ $model->users->email }}</p> 
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('Phone:') !!} 
                    <p>{{ $model->users->phone }}</p> 
                </div>


                <div class="form-group col-sm-12">
                    <h2>Association Information</h2>
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('Name:') !!}
                    <p>{{ $model->name }}</p> 
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('Status:') !!}
                    <p>{{ ($model->status!=0)?'Active':'Inactive' }}</p>   
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('Association Email:') !!}
                    <p>{{ $model->email }}</p> 
                </div>
               <div class="form-group col-md-6">
                    {!! Form::label('Zipcode:') !!}  
                    <p>{{ $model->zip_code }}</p>
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('City:') !!}
                    <p>{{ ($getCity!="")?$getCity->city:'' }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('State:') !!}  
                    <p>{{ ($getState!="")?$getState->state:'' }}</p>
                </div>
                
                <div class="form-group col-md-6">
                    {!! Form::label('Contact Person:') !!} 
                    <p>{{ $model->contact_person }}</p>

                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('Organization Position:') !!} 
                    <p>{{ $model->position }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('Phone:') !!}  
                    <p>{{ $model->phone }}</p>
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('Phone2:') !!}
                    <p>{{ $model->phone2 }}</p>

                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('Website URL:') !!}  
                    <p>{{ $model->website_url }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('Physical Address:') !!}  
                    <p>{{ $model->address }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('Number of Buildings:') !!}  
                    <p>{{ $model->no_of_bulidings }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('Number of Units:') !!} 
                    <p>{{ $model->no_of_units }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('Types of Association:') !!}  
                    <p>{{ $model->association_type }}</p>
                </div>
                <div class="form-group col-sm-12">

                    <label>Do you wish to receive Service Provider Advertisement</label>
                    <label>
                        <?php echo ($model->advertisement == 1) ? '<i class="fa fa-check"></i>' : ''; ?> 
                        YES
                    </label>
                    <label>
                        <?php echo ($model->advertisement == 0) ? '<i class="fa fa-check"></i>' : ''; ?> 
                        NO
                    </label>

                </div>

                <div class="form-group col-md-12">
                    {!! Form::label('Google Map Link:') !!} 
                    @if($model->google_map!='')
                   <iframe src="<?php echo $model->google_map ;?>" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                   @endif
                </div>


            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@endsection