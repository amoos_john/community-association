@extends('admin/admin_template')

@section('content')
<style>
    .file-drop-area.fnc-uplaod {
    float: left;
}
img.img-responsive.img-thumbnail {
    width: 230px;
    height: 180px;
}
textarea.form-control {
    height: 200px;
    resize: none;
}
</style>
<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
		@endif
        <div class="box-header with-border">
          <h3 class="box-title">Edit Association</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! Form::model($model, ['files' => true,'class' => 'form','url' => ['admin/association/update', $model->id], 'method' => 'post']) !!}
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
            <a href="{{ url('admin/association') }}" class="btn btn-danger">Cancel</a>
            </div>
            <!-- text input -->
            <?php $page='admin.association.form'?>
            @include($page)
         
          <!-- nav-tabs-custom -->
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script>

$('input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
   
    getCities('<?php echo $model->state;?>','<?php echo $model->city;?>');
    function getCities(code,city = 0)
    {
        
    var url = 'state_code=' + code +'&city=' + city;
    $.ajax({
    type: "GET",
            url: "{{ url('admin/get/cities') }}",
            data: url,
            success: function(data){
            $('#selectCity').html(data);
            },
            error: function(errormessage) {
            //you would not show the real error to the user - this is just to see if everything is working
            alert(errormessage);
            }
    });
    }
</script>
@endsection