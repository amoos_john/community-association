<div class="modal fade" id="multiple" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
           <div class="modal-dialog">
           <div class="modal-content">
               <div class="bg-light-blue-active modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Properties/Rooms</h4>
                </div>
            
              <div class="modal-body">
            {!! Form::open(array( 'class' => 'form','url' => ''.$url, 'files' => true)) !!}
            {!! Form::hidden('id',$id,array('id'=>'id')) !!}
            <table class="table table-hover">
              <thead>
              <tr>
                <th><a id="allcheck">All</a> | <a  id="alluncheck">None</a></th>
                <th><div class="box-tools">
                <div class="input-group input-group-sm" style="width: 300px;">
                  <input type="text" name="search" id="search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">

                    <button type="button" id="btnclear" class="btn btn-default">Clear</button>
                  </div>
                </div>
              </div>
               </th>
              </tr>
              </thead>
              <tbody class="list_property" id="list_property">
             @foreach($properties as $row)
              <?php $checked=''; ?>
                 
                @if($module=='rules')
                 @foreach($row->property_booking_rules as $booking_rules)
                    <?php
                       if($booking_rules->rules_id==$id)
                       {
                            $checked='checked';
                       }
                    ?>
                @endforeach
                @elseif($module=='fees')
                 @foreach($row->property_taxes_fees as $taxes_fees)
                    <?php
                       if($taxes_fees->feestaxes_id==$id)
                       {
                            $checked='checked';
                       }
                    ?>
                @endforeach
                @elseif($module=='special')
                 @foreach($row->property_specials as $special)
                    <?php
                       if($special->special_id==$id)
                       {
                            $checked='checked';     
                       }
                    ?>
                @endforeach
                   @endif
               <tr>
                  <td><input type="checkbox" class="checkbox" value="{{ $row->id }}" name="property_id[]" <?php echo $checked; ?>/></td>
                  <td>{{ $row->internal_name }}</td>
               </tr>
             @endforeach 
              </tbody>
          </table>
              </div>
                  
               <div class="modal-footer">
               
                <button type="submit" class="btn btn-success">Ok</button>

                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                 {!! Form::close() !!}
              </div>
            </div>
        </div>
</div>
<script>

    $("#search").on('keyup',function(){
        
        search('<?php echo $module; ?>');
    });
    $("#btnclear").on('click',function(){
        
        $('#search').val('');
        search('<?php echo $module; ?>');
    });
    $("#btnsearch").on('click',function(){
       
        search('<?php echo $module; ?>');
    });
    function search(module)
    {
        var search = $('#search').val();
        var id = $('#id').val();
       
        var datasend = 'search='+search+'&id='+id+'&module='+module;
        
        $.ajax({
            type: "GET",
            url: "<?php echo url('admin/property/multiple'); ?>",
            data: datasend,
            cache:false,
            success: function(data){
    
                $('#list_property').html(data);
                
            }
            
        });
    }
    
   $("#allcheck").click(function(){

      $('.checkbox').prop('checked', true);
   
 });
  $("#alluncheck").click(function(){
  
 $('.checkbox').prop('checked', false);
     
 });
</script>