<aside class="main-sidebar">
    <section class="sidebar">
         <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ url('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
       
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
             <li>
                 <a href="{{ url('admin') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
                </a>
               
            </li>
            @if(Auth::user()->role_id==1)
             <li class="treeview">
                 <a href="#">
                    <i class="fa fa-user-secret"></i> <span>Admin</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{ url('admin/users') }}"><i class="fa fa-circle-o text-blue"></i> List Admin</a></li>
                    <li><a href="{{ url('admin/users/create') }}"><i class="fa fa-circle-o text-aqua"></i> Add New Admin</a></li>
                </ul>
            </li>
            
             <li class="treeview">
                 <a href="#">
                    <i class="fa fa-users"></i> <span>Vendors</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{ url('admin/vendors') }}"><i class="fa fa-circle-o text-blue"></i> List Vendor</a></li>
                    <li><a href="{{ url('admin/vendors/users') }}"><i class="fa fa-circle-o text-aqua"></i> List all the additional users</a></li>
                </ul>
            </li>
             <li class="treeview">
                 <a href="#">
                    <i class="fa fa-check-square"></i> <span>Association</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{ url('admin/association') }}"><i class="fa fa-circle-o text-blue"></i> List Association</a></li>
                    <li><a href="{{ url('admin/association/users') }}"><i class="fa fa-circle-o text-aqua"></i> List all the additional users</a></li>
                </ul>
            </li>
            <!--
            <li class="treeview">
                 <a href="#">
                    <i class="fa fa-bullhorn"></i> <span>Promotion Announcement</span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="#"><i class="fa fa-circle-o text-blue"></i> List Announcement</a></li>
                    <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> Promotional package details</a></li>
                </ul>
            </li>
            <li>
                 <a href="#">
                    <i class="fa fa-thumbs-o-up text-blue"></i> <span>Open Job Bids</span>
                </a>
               
            </li>
            
            <li>
                 <a href="#">
                    <i class="fa fa-arrow-circle-right text-blue"></i> <span>RFP(s)</span>
                </a>
               
            </li>
            <li class="treeview">
                 <a href="#">
                    <i class="fa fa-shopping-cart"></i> <span>Package Managers</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="#"><i class="fa fa-circle-o"></i> List Package</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Add New Package</a></li>
                </ul>
            </li>
            <li>
                 <a href="#">
                    <i class="fa fa-cart-plus"></i> <span>Order Management</span>
                </a>
               
            </li>
            -->
            @endif
          
        </ul>
    </section>
</aside>