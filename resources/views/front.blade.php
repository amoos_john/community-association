<!DOCTYPE html>
<html lang="en" class="broken-image-checker">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--iPhone from zooming form issue (maximum-scale=1, user-scalable=0)-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title>{{ (isset($page_title))?$page_title:'Home' }} | {{Config::get('params.site_name')}}</title>

	<link rel="icon" type="image/png" href="{{ asset('front/images/favicon.png')}}">
	
    <!-- Bootstrap -->
        <link href="{{ asset('front/css/bootstrap.min.css')}}" rel="stylesheet">
	

	<link rel="stylesheet" href="{{ asset('front/css/stylized.css')}}">
        <link rel="stylesheet" href="{{ asset('front/style.css')}}">
        <link rel="stylesheet" href="{{ asset('front/css/colorized.css')}}">
        <link rel="stylesheet" href="{{ asset('front/css/animate.css')}}">
        <link rel="stylesheet" href="{{ asset('front/css/font-awesome.min.css')}}">
        
        
	
	<!-- jQuery -->
	<!--[if (!IE)|(gt IE 8)]><!-->
        <script src="{{ asset('front/js/jquery-2.2.4.min.js')}}"></script>
	   
	<!--<![endif]-->


	<!--[if lte IE 8]>
	  <script src="js/jquery1.9.1.min.js"></script>
	<![endif]-->
	
	<!--browser selector-->
	<script src="{{ asset('front/js/css_browser_selector.js')}}" type="text/javascript"></script>
        

	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	<script src="{{ asset('front/extralized/bootstrap-datepicker.js')}}"></script>
	<!-- Angular JS -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular.min.js"></script>  
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular-route.min.js"></script>
  
  </head>
 <body class="transition nav-plusminus slide-navbar slide-navbar--right">
      
  @include('front/common/header')
  <main id="page-content">
  @yield('content')
  @include('front/common/footer') 
  </main>	    

    <!--Bootstrap-->
    <script src="{{ asset('front/js/bootstrap.min.js')}}"></script>
	<!--./Bootstrap-->
	
	<!--Major Scripts-->
    <script src="{{ asset('front/js/viewportchecker.js')}}"></script>
    <script src="{{ asset('front/js/kodeized.js')}}"></script>
	<!--./Major Scripts-->	
<script>
	var myCollapse = '[data-toggle="collapse"]';
var myCollapseParent = '[class*="-group"], [class*="-area"]';
jQuery(myCollapse).click(function() {
  jQuery(this).parents(myCollapseParent).toggleClass(" / is-active select");
});
</script>



<script>
jQuery(".fnc-uplaod [type=file]").on("change", function() {
  /*
	Name of file will show in label as text *input#ID need to = label[for=#ID]
  */
  var file = this.files[0].name;
  var dflt = jQuery(this).attr("placeholder");
  if (jQuery(this).val() != "") {
    jQuery(this).next().text(file);
  } else {
    jQuery(this).next().text(dflt);
  }
});

$( document ).ready(function() {   
    $("input[type=tel]").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         return false;
    }
   });
   $( "input[name=first_name],input[name=last_name],input[name=username],input[name=position]" ).keypress(function(e) {
    var key = e.keyCode;
    var yourInput = $(this).val();
    re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);

    if (key >= 48 && key <= 57) {
        e.preventDefault();
    }
    else if(isSplChar)
    {
            var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
            $(this).val(no_spl_char);
            return false;
            e.preventDefault();
    }
    });

   
   $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
}); 

$('#selectState .form-group input').click(function(){
 
 $('#selCity').text('Select City');
 $('[data-target="#selectState"]').trigger( "click" );
 $('#selState').text($(this).attr('id'));
 $('[data-target="#selectCity"]').trigger("click");
});

$("body").on("click","#selectCity .form-group input",function(){

 $('[data-target="#selectCity"]').trigger( "click" );
 $('#selCity').text($(this).attr('id'));

});    

</script>

</body>
</html>