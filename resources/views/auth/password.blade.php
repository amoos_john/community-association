@extends('front')

@section('content')
<style>
    .forgot-fom {
    display: table;
    margin: 20px auto;
    float:none;
}
</style>
<section class="forgot-password-area btn-effect--ripple">
    <div class="container">
		
    <div class="hed hed-sub">
            <h2>Forgot Password</h2>
            <p>Enter your Email Address below</p>
    </div>

    <div class="forgot-fom fom-bottomline--focus fnc-fom col-sm-6 mb50">
        @if (session('status'))
        <div class="alert alert-success alert-dismissible">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('status') }}
        </div>
        @endif

        @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissible">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}" name="loginform">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="input-group w100">
                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                <input id="email" type="email" class="form-control" name="email" placeholder="Email Address*" required="">
          </div>

          <div class="clearfix"></div>
          <div class="login__submit text-right">
                <button type="submit" class="btn btn-primary ">Submit</button>
          </div>
        </form>
    </div>
 
    </div>
</section>
@endsection
