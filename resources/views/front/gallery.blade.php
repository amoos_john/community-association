@extends('front')

@section('content')
<style>
    .progal__img img
    {
        width:100%;
        height: 190px;
    }
</style>
<section class="services-area" ng-app="myApp" ng-controller="MyCtrl">
<div class="container">
    <form method="get" id="frm_search" name="frm_search">
<div class="box-holder">
<div class="service__area col-sm-6 fnc-fom">
<div class="hed"><h3>Service Area</h3></div>
    
<div class="row ">
    <?php $i = 0; ?>
    @foreach($areas as $area)
    @if($i==0)
    <div class="col-sm-6">
    @endif
        <div class="form-group">
            <input type="checkbox" name="areas[]"  class="service-area" value="{{ $area->id }}" id="{{ $area->name }}" />
            <mark></mark><label for="{{ $area->name }}">{{ $area->name }}</label>
        </div>
    <?php $i++; ?>
    @if($i==6)
    </div>
    <?php $i = 0; ?>
    @endif
    @endforeach
</div>
</div>

</div>

<div class="service__loc col-sm-6  accordion-arr fnc-fom">

        <div class="hed"><h3>Service Location</h3></div>


        <div class="form-group col-sm-12  p0">

    <div class="panel-title style-select">
            <a type="button" data-toggle="collapse" data-target="#selectState" class="collapsed">
        <i class="icon"><img src="{{ asset('front/images/icon-state.png')}}" alt="" /></i> State </a>
    </div>

    <div id="selectState" class="style-select-option collapse ">

     @foreach($states as $row)

        <div class="form-group">
            <input type="radio" name="states"  onChange="getCounties('{{ $row->state_code }}')" value="{{ $row->state_code }}" id="{{ $row->state }}" /><mark></mark>
             <label for="{{ $row->state }}">{{ $row->state }}</label>

        </div>

    @endforeach

    </div>

    </div>

   <div class="form-group  col-sm-12 p0">

    <div class="panel-title style-select">
            <a type="button" data-toggle="collapse" data-target="#selectCounty" class="collapsed">
         <i class="icon"><img src="{{ asset('front/images/icon-state.png')}}" alt="" /></i> County </a>
    </div>


    <div id="selectCounty" class="all style-select-option collapse ">
        
    </div>


    </div>
        <div class="buttons text-right mb30">
                <button class="btn btn-primary">Select All</button>

                <button type="button" class="btn btn-primary" id="search">Search Gallery</button>
        </div>

</div>
    </form>
</div>

</section>
<section class="progal-area" >
<div class="container">
    <div  id="load"></div>
    <div class="row" id="search-result">
       
    </div>
</div>
<div class="modal fade job-desc-modal" id="viewProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close " data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4></h4>
      </div>
      <div class="modal-body overload fnc-fom accordion-arr" id="details-modal">
        			
      </div>
    </div>
  </div>
</div>  
</section>
<script>
var myApp = angular.module('myApp', []);

    myApp.controller('MyCtrl', function($scope){
        $scope.lists = [
            {'vl' : 1, 'state' : false},
            {'vl' : 2, 'state' : false},
            {'vl' : 3, 'state' : false},
            {'vl' : 4, 'state' : false},
            {'vl' : 5, 'state' : false}
        ];

        $scope.change = function(id){
            $scope.lists[id].state = !$scope.lists[id].state;
            console.log($scope.lists);
        };
    })


</script>

<script type="text/javascript">
    function getCounties(code)
    {
      var url = 'state_code='+code;
       $.ajax({ 
         type: "GET",
         url: "{{ url('vendor/get-counties') }}",
         data: url,
         success: function(data){
            $('#selectCounty').html(data);

        },
         error: function(errormessage) {
               //you would not show the real error to the user - this is just to see if everything is working
               alert(errormessage);    

         }
        }); 
    }
$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        var url = $(this).attr('href'); 
        var getval = url.split('=');
        var value=getval[1];
        //$("html, body").animate({ scrollTop: 0 }, "slow");
        searchPagination(value);
        return false;

    });

});
function viewProfile(id)
{
    $.ajax({
        type: 'GET',
        url: '<?php echo  url("gallery/profile"); ?>/'+id,
        success: function(data) 
        {
            //$('#rfp-sub-modal').toggle();
            $('#details-modal').html(data);
        }
    });
}

</script>
@include('front.common.jquery')
@endsection