@extends('front')

@section('content')

	<section class="slider-area slider-home hover-ctrl fadeft anime-down delay1s" >
		<div id="carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">
		
		  <ol class="carousel-indicators thumbs">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">

			<div class="item active bg-cntr bg-cvr" style="background-image:url('{{ url('front/images/slide1.jpg')}}');"></div>
			
			<div class="item" style="background-image:url('{{ url('front/images/slide2.jpg')}}');"></div>
			
		  </div>

		  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		  
		</div>
		
	</section>
	
	
	
	<section class="progal-area">
		<div class="container">
			
				
				<div class="hed anime-down"><h2>Project Gallery</h2></div>
				
				<div class="progal-box col-sm-4 anime-left delay1s">
					<div class="progal__inr">
						<div class="progal__img "><img src="{{ url('front/images/project1.jpg')}}" alt=""></div>
						<div class="progal__cont">
							<h3>United Field Inc</h3>
							<div class="cont">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
Lorem make a type specimen book.</div>
							<div class="lnk-btn view-btn"><a href="#">View Profile</a></div>
						</div>
					</div>
				</div>
			
			<div class="progal-box col-sm-4 anime-in">
					<div class="progal__inr">
						<div class="progal__img"><img src="{{ url('front/images/project2.jpg')}}" alt=""></div>
						<div class="progal__cont">
							<h3>United Field Inc</h3>
							<div class="cont">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
Lorem make a type specimen book.</div>
							<div class="lnk-btn view-btn"><a href="#">View Profile</a></div>
						</div>
					</div>
				</div>
			
			
			<div class="progal-box col-sm-4 anime-right delay1s">
					<div class="progal__inr">
						<div class="progal__img"><img src="{{ url('front/images/project3.jpg')}}" alt=""></div>
						<div class="progal__cont">
							<h3>United Field Inc</h3>
							<div class="cont">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
Lorem make a type specimen book.</div>
							<div class="lnk-btn view-btn"><a href="#">View Profile</a></div>
						</div>
					</div>
				</div>
			
		</div>
	</section>
	
	
	
	<section class="easy-area bg-cvr" style="background-image:url('{{ url('front/images/etc-bg.jpg')}}')">
		<div class="container">
			
			<div class="hed anime-down"><h2>Easy As</h2></div>
			
				<div class="easy__steps shake-icon list-col-3 text-center">
					<ul>
						<li class="anime-flipInX">
							<mark>1</mark>
							<div class="icon"><a href="{{url('register')}}"><img src="{{ url('front/images/easy-icon1.png')}}" alt=""></a></div>
							<span>Register</span>
						</li>
						<li class="anime-flipInX delay1s">
							<mark>2</mark>
							<div class="icon"><a href="{{url('register')}}"><img src="{{ url('front/images/easy-icon2.png')}}" alt=""></a></div>
							<span>Pick Your Package</span>
						</li>
						<li class="anime-flipInX delay2s">
							<mark>3</mark>
							<div class="icon"><img src="{{ url('front/images/easy-icon3.png')}}" alt=""></div>
							<span>Get Started</span>
						</li>
					</ul>
				</div>
			
			
		</div>
	</section>
@endsection