@extends('front')

@section('content')

<section class="registeration-area btn-effect--ripple">
	<div class="container">
		
		<div class="hed">
			<h2>Registration Selection</h2>
		</div>
		
		
		<div class="forgot-fom fom-bottomline--focus fnc-fom col-sm-5 pul-cntr mb50">
			
			  <div class="buttons text-center">
					<a class="btn btn-primary w100 mb30" href="{{ url('association/register') }}">Association Registration</a>
					<a class="btn btn-primary w100" href="{{ url('vendor/packages') }}">Vendor Registration</a>
			  </div>
			
		</div>
 
	</div>
</section>
@endsection