@extends('front')

@section('content')

<section class="registeration-area btn-effect--ripple">
	<div class="container">
		
        <div class="hed">
                <h2>THANK YOU!</h2>
        </div>
        <div class="forgot-fom fom-bottomline--focus fnc-fom col-sm-5 pul-cntr mb50 text-center">
                <img src="{{ asset('front/images/thankyou.png')}}" alt="">
                <br/>
                 <p>Thank you for choosing us to serve your needs. 
                 </p>
                 @if(isset(Auth::user()->id))
                    @if (Session::has('success'))
                    <div class="alert alert-success" alert-dismissible>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-check"></i> &nbsp  {!! session('success') !!}</h4>
                    
                        <p>Your Package has been upgraded successfully!</p>
                    </div>
                @endif
                    <a href="{{ url('association/dashboard') }}" class="btn btn-primary">Back to Dashboard</a>
                 @else
                    <p>Registration email has been successfully send to your email</p>
                 @endif

        </div>
 
	</div>
</section>
@endsection