@extends('front')

@section('content')

<section class="registeration-area btn-effect--ripple">
	<div class="container">
		
        <div class="hed">
                <h2>THANK YOU!</h2>
        </div>
        <div class="forgot-fom fom-bottomline--focus fnc-fom col-sm-5 pul-cntr mb50 text-center">
                <img src="{{ asset('front/images/cancel.png')}}" alt="">
                <br/>
                 <p>Your payment cannot proceed!</p>
               
                 @if(isset(Auth::user()->id))
                    @if (Session::has('error'))
                    <div class="alert alert-success" alert-dismissible>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p><i class="icon fa fa-check"></i> &nbsp  {!! session('error') !!}</p>
                      
                    </div>
                @endif
                    <a href="{{ url('association/dashboard') }}" class="btn btn-primary">Back to Dashboard</a>
                 @else
                    <p>Registration email has been successfully send to your email</p>
                 @endif

        </div>
 
	</div>
</section>
@endsection