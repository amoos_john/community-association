@extends('front')

@section('content')


<div class="container">
	<section>
	<div class="container-fluid">
		<div class="row">
<div class="destina col-sm-9 bg-white">
	<h3>{{ $content->title }}</h3>

        {{ @strip_tags($content->body) }} 
</div></div>
</div>

       
</section>
</div>
				
@endsection