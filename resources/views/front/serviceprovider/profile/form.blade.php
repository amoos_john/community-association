<?php
$required = "required";
?>
<div class="form-group col-sm-6">
    <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-username.png')}}" alt="" /></i></span>
        <input id="firstName" type="text" class="form-control" value="{{ $model->first_name }}" name="first_name" placeholder="First Name*" <?php echo $required; ?>/>
    </div>
</div>
<div class="form-group col-sm-6">
    <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-username.png')}}" alt="" /></i></span>
        <input id="firstName" type="text" class="form-control" value="{{ $model->last_name }}" name="last_name" placeholder="Last Name">
    </div>
</div>
<div class="form-group col-sm-12">
    <div class="input-group">

        <div class="panel-title style-select">
            <a type="button" data-toggle="collapse" data-target="#selectCategory" class="collapsed">
                <i class="icon"><img src="{{ asset('front/images/icon-state.png')}}" alt="" /></i> Category </a>
        </div>


        <div id="selectCategory" class="style-select-option collapse ride ">
            @foreach($areas as $area)
            <div class="form-group">
                <input type="radio" name="service_id" class="service-area" value="{{ $area->id }}" id="{{ $area->name }}" <?php echo ($area->id==$provider->service_id)?'checked':'' ?>/>
                <mark></mark><label for="{{ $area->name }}">{{ $area->name }}</label>
            </div>
            @endforeach


        </div>
    </div>
</div>

<div class="form-group col-sm-12">
    <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-lock.png')}}" alt="" /></i></span>
        <input id="loginName" type="text" class="form-control" name="name" value="{{ $model->name }}" placeholder="Login Name">
    </div>
</div>

<div class="form-group col-sm-12">
    <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-key.png')}}" alt="" /></i></span>
        <input id="password" type="password" class="form-control" name="password" placeholder="Password"/>
    </div>
</div>

<div class="form-group col-sm-12">
    <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-key.png')}}" alt="" /></i></span>
        <input id="confirmPassword" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password"/>
    </div>
</div>
<div class="form-group col-sm-8">

    <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-photo.png')}}" alt="" /></i> Add Logo</span>

    <div class="file-drop-area fnc-uplaod">
        <span class="btn  btn-info">Choose Files</span>
        <input class="file-input" type="file" name="logo" id="logo"  accept="image/*"/>
        <label for="Picture1">No file Chosen</label>
    </div>

</div>
<div class="form-group col-sm-4 text-right" >
    <img id="imagePreview" class="img-responsive img-thumbnail" src="{{ asset('/'.$provider->logo)}}" alt="Logo" />
</div>


<div class="btns col-sm-12 text-right">
    <button type="submit" class="btn btn-primary">Update</button>
</div>

