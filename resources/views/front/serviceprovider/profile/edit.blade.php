@extends('front')

@section('content')
<style>
    .file-drop-area.fnc-uplaod {
    float: left;
}
img.img-responsive.img-thumbnail {
    width: 230px;
    height: 180px;
}
</style>
<script>

var loadImageFile = (function () 
{
    if (window.FileReader) {
        var   oPreviewImg = null, oFReader = new window.FileReader(),
            rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

       
        return function () {
            var aFiles = document.getElementById("logo").files;
            if (aFiles.length === 0) { return; }
            if (!rFilter.test(aFiles[0].type)) { 
                alert("You must select a valid image file !");
                var $el = $('#logo');
                $el.wrap('<form>').closest('form').get(0).reset();      

                return; 
            }
            
        }
    

    }

})();

</script>
<section class="dashboard-page pb30">
    <div class="container">

        <div class="row">

            <div class="dash__left col-sm-3 mt100">

                   @include('front/serviceprovider/common/navigation')  

            </div>
            <div class="dash__right col-sm-9">

            <div class="hed"><h2>{{ $page_title }}</h2></div>

            <div class="row">

           <div class="fom fnc-fom accordion-arr fom-bottomline--focus">

            @include('front/common/errors')       
            {!! Form::model($model, ['files' => true,'class' => 'form','url' => ['vendor/profile/update', $model->id], 'method' => 'post']) !!}

            <?php $page = "front.serviceprovider.profile.form"; ?>
            @include($page)
            

            {!! Form::close() !!}
            </div>


            </div>

            </div>


    </div>
  </div>
</section>
<script>


function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#imagePreview').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#logo").change(function(){
        readURL(this);
    });
</script>
@endsection