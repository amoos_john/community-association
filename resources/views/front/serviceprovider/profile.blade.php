@extends('front')

@section('content')

<section class="easy-area pt30">
<div class="container">

 <div class="easy__steps shake-icon list-col-3 text-center">
    <ul>
    <li class="anime-flipInX active">
            <mark>Step 1</mark>
            <div class="icon"><img src="{{ asset('front/images/easy-icon1.png')}}" alt=""></div>
            <span>Register as Service Provider</span>
    </li>
    <li class="anime-flipInX delay1s active">
            <mark>Step 2</mark>
            <div class="icon"><img src="{{ asset('front/images/easy-icon2b.png')}}" alt=""></div>
            <span>Create Profile</span>
    </li>
     <li class="anime-flipInX delay2s">
            <mark>Step 3</mark>
            <div class="icon"><img src="{{ asset('front/images/easy-icon3.png')}}" alt=""></div>
            <span>Membership Package</span>
    </li>
    </ul>

</div>

</div>
</section>

<section class="package-area membership-packages association-profile pb50 fom-bottomline--focus fnc-fom accordion-arr">
<div class="container">
<div class="hed">
    <h2>{{ $page_title }}</h2>
</div>
@include('front/common/errors')      
{!! Form::open(array( 'class' => 'form','url' => 'vendor/profile/insert', 'files' => true)) !!}

<div class="form-group col-sm-12">
    <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-state.png')}}" alt="" /></i></span>
            <input type="text" name="company_name" value="{{ old('company_name') }}" id="name"  class="form-control"  placeholder="Company Name*" required="">
            <input type="hidden" name="id" value="{{ (isset($id))?$id:'' }}" />
      </div>
</div>

<div class="form-group col-sm-12">
  <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-map.png')}}" alt="" /></i></span>
        <input id="Address1" name="address" value="{{ old('address') }}" type="text" class="form-control"  placeholder="Your Address" >
  </div>
</div>

<div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-mobile.png')}}" alt="" /></i></span>
            <input id="Phone1" type="tel" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Phone Number*" required/>
      </div>
 </div>  

<div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-email.png')}}" alt="" /></i></span>
            <input id="EmailAddress" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address*"  required/>
      </div>
</div>


<div class="form-group col-sm-12">
  <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-website.png')}}" alt="" /></i></span>
        <input id="WebsiteURL" type="url" name="website_url" value="{{ old('website_url') }}" class="form-control"  placeholder="Website">
  </div>
</div>

  <div class="actions col-sm-12 text-right">
      <button type="submit" class="btn btn-primary">Next</button>
    </div>
		

{!! Form::close() !!}	

</div>
</section>
@endsection 