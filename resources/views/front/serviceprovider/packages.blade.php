@extends('front')

@section('content')
<?php
use App\Functions\Functions;
?>
<style>
.package__inr.active {
    background-color: #1a9fbc;
}    
    
</style>
<section class="package-area bg-cvr" style="background-image:url('{{ asset('front/images/etc-bg.jpg') }}');" >
<div class="container">
        <div class="hed">
                <h2>Services Provider Packages</h2>
        </div>

        @foreach($packages as $key=>$package)
        @if($key==0)
            <?php $animation = "anime-left delay1s";?>
        @elseif($key==1)
            <?php $animation = "anime-zoomIn";?>
        @else
            <?php $animation = "anime-right delay1s";?>
        @endif
        <div class="package-box col-sm-4 {{ $animation }}">
            <div class="package__inr <?php echo ($package_id==$package["id"])?'active':''; ?>">
            <div class="package__img">
                    <img src="{{ asset('/'.$package["image"])}}" alt="package" />
            </div>
            <div class="package__cont">
                    <h3><i class="icon-price"></i> {{ Functions::moneyFormat($package["price"]) }}</h3>
                    <h4>{{ $package["package_name"] }}</h4>
                    <h5>{{ $package["description"] }}</h5>
                    <div class="lnk-btn">
                            @if($package_id==$package["id"])
                               Selected Package
                            @else
                               <a href="{{ url('vendor/package/'.$package["id"])  }}" class="btn btn-default">Subscribe Now</a>

                            @endif
                    </div>
            </div>
           </div>
        </div>

	@endforeach
		
</div>
</section>

@if($package_id!="")
@include('front/common/errors')      
<form method="post" name="frm_cart" id="frm_cart"> 
<section class="services-area">
<div class="container">

<div class="box-holder">
     <div class="service__area col-sm-6 fnc-fom">

    <div class="hed"><h3>Service Area</h3></div>

  
    <div class="row ">
    <?php $i = 0; ?>
    @foreach($areas as $area)
    @if($i==0)
    <div class="col-sm-6">
    @endif
        <div class="form-group">
            <input type="checkbox" name="areas[]" class="service-area" value="{{ $area->id }}" id="{{ $area->name }}" />
            <mark></mark><label for="{{ $area->name }}">{{ $area->name }}</label>
        </div>
    <?php $i++; ?>
    @if($i==6)
    </div>
    <?php $i = 0; ?>
    @endif
    @endforeach
       </div>

        </div>
    </div>
           
             

<div class="service__loc col-sm-6  accordion-arr fnc-fom">

    <div class="hed"><h3>Service Location</h3></div>
    
    <div class="form-group col-sm-12  p0">

    <div class="panel-title style-select">
            <a type="button" data-toggle="collapse" data-target="#selectCounty" class="collapsed">
        <i class="icon"><img src="{{ asset('front/images/icon-state.png')}}" alt="" /></i> State </a>
    </div>

    <div id="selectCounty" class="style-select-option collapse ">

     @foreach($states as $row)

        <div class="form-group">
            <input type="radio" name="states[]" onChange="getCounties('{{ $row->state_code }}')" value="{{ $row->state_code }}" id="{{ $row->state }}" /><mark></mark>
             <label for="{{ $row->state }}">{{ $row->state }}</label>

        </div>

    @endforeach

    </div>

    </div>
    
    <div class="form-group  col-sm-12 p0">

    <div class="panel-title style-select">
            <a type="button" data-toggle="collapse" data-target="#selectState" class="collapsed">
         <i class="icon"><img src="{{ asset('front/images/icon-state.png')}}" alt="" /></i> County </a>
    </div>


    <div id="selectState" class="all style-select-option collapse ">
        
    </div>


    </div>
    
    <div class="form-group  col-sm-12 p0">
    <div class="panel-title style-select">
            <a type="button" data-toggle="collapse" data-target="#selectGallery" class="collapsed">
         <i class="icon"><img src="{{ asset('front/images/icon-photo.png')}}" alt="" /></i> Gallery </a>
    </div>
    <div id="selectGallery" class="all style-select-option collapse ">
        @foreach($gallery_packages as $row)
        <div class="form-group">
            <input type="radio" name="gallery"  value="{{ $row->id }}" id="{{ $row->package_name }}" /><mark></mark>
             <label for="{{ $row->package_name }}">{{ $row->package_name }}</label>

        </div>
    @endforeach
        <div class="form-group">
                <input type="radio" name="gallery"  value="0" id="wish" /><mark></mark>
                 <label for="wish">Do not wish to upgrade at this time.</label>

        </div>
    </div>
    </div>
    <div class="buttons text-right mb30">
        <button type="button" id="create" class="btn btn-primary">Create Package</button>
    </div>

</div>

</div>
<!--
<div class="accordion-arr fnc-fom">
    <div class="panel-title style-select">
            <a type="button" data-toggle="collapse" data-target="#selectPic" class="collapsed">
            Would you like to add gallery pics? </a>
    </div>


    <div id="selectPic" class="style-select-option collapse ">

            <div class="form-group">
                    <input type="radio" value="0" name="gallery" id="selectPicNo" name="selectPic" /><mark></mark>
                    <label for="selectPicNo">NO</label>
            </div>

            <div class="form-group">
                    <input type="radio" value="1" name="gallery" id="selectPicYes" name="selectPic" /><mark></mark>
                    <label for="selectPicYes">YES</label>
            </div>

    </div>

</div>
-->
</form>   
<div class="clearfix"></div>
	
    <div id="message"></div>
     
    <div class="clearfix"></div>
    <div id="cart">
        
     </div>
 
 </div> 
</section>

@include('front.common.javascript')
@endif
@endsection