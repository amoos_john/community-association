@extends('front')

@section('content')
<style>
td.disabled.day {
    opacity: 0.2;
}
</style>
<section class="dashboard-page accordion-arr fom-bottomline--focus pb50">
    <div class="container">

            <div class="row">

            <div class="dash__left col-sm-3 pt100">

                   @include('front/serviceprovider/common/navigation') 

            </div>
            <div class="dash__right col-sm-9">

            <div class="hed"><h2>{{ $page_title }}</h2></div>



          <div class="fom fnc-fom crtpromo-form">

            @include('front/common/errors')       
            {!! Form::open(array( 'class' => 'form','url' => 'vendor/promotional/insert', 'files' => true)) !!}

            <?php $page = "front.serviceprovider.promotional.form"; ?>
            @include($page)
            <div class="col-sm-12 text-right">
                <button type="submit" class="btn btn-primary ">Save & Continue</button>
                <button class="btn btn-info">Save & Exit</button>
            </div>

            {!! Form::close() !!}
            </div>


            </div>

            </div>

    </div>
</section>
<script>
    $(document).ready(function(){
                //var array = ["2017-07-14","2017-07-15","2017-07-16"]

                
    var start_date=$('input[name="start_date"]'); //our date input has the name "date"
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    start_date.datepicker({

            format: 'yyyy-mm-dd',
            startDate: new Date(),
            container: container,
            todayHighlight: true,
            autoclose: true,
            active:true


    });
   $("#start_date").keydown(function(event) { 
        return false;
    });   
    
    var start_date=$('input[name="end_date"]'); //our date input has the name "date"
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    start_date.datepicker({

            format: 'yyyy-mm-dd',
            startDate: new Date(),
            container: container,
            todayHighlight: true,
            autoclose: true,
            active:true


    });
   $("#end_date").keydown(function(event) { 
        return false;
    });
    
    


	});
</script>
<script>
//var len =  ;  
    
$(":checkbox[name='areas[]']").change(function(){
  if ($(":checkbox[name='areas[]']:checked").length == len)                                              
   $(":checkbox[name='areas[]']:not(:checked)").prop('disabled', true);  
  else                                                     
   $(":checkbox[name='areas[]']:not(:checked)").prop('disabled', false); 
});

$(":checkbox[name='category[]']").change(function(){
  if ($(":checkbox[name='category[]']:checked").length == len)                                              
   $(":checkbox[name='category[]']:not(:checked)").prop('disabled', true);  
  else                                                     
   $(":checkbox[name='category[]']:not(:checked)").prop('disabled', false); 
});

</script>
@endsection