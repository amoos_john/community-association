@extends('front')

@section('content')
<?php
    use App\Functions\Functions;
?>
<section class="dashboard-page pb30">
    <div class="container">
                   

        <div class="row">

            <div class="dash__left col-sm-3 mt100">

                   @include('front/serviceprovider/common/navigation')  

            </div>
            <div class="dash__rigt col-sm-9">
          <div class="hed"><h2>{{ $page_title }}</h2></div>
                @include('front/common/errors')
        <div class="table-responsive table-check-status fnc-fom clrlist">

        <table class="table table-bdr--black text-center">
                <thead>
                <tr>
                        <th>Title</th>
                        <th>Submission Date</th>
                        <th>Start Promotion Date</th>
                        <th>Expiration Date</th>
                        <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count($model)>0)
                    @foreach($model as $row)
                   
                        <tr>
                                <td>{{ $row->title }}</td>
                                <td>{{ Functions::dateFrontFormat($row->created_at) }}</td>
                                <td>{{  Functions::dateFrontFormat($row->start_date) }}</td>
                                <td>{{  Functions::dateFrontFormat($row->end_date) }}</td>
                                <td>
                                <ul>
                                    <li>
                                     <a data-target="#openJob" onClick="return open_job('{{ $row->id }}');" data-toggle="modal">View</a></li>

                                    <li><a href="{{ url("vendor/promotional/edit/".$row->id) }}">Edit</a></li>
                                    <li><a data-href="{{ url("vendor/promotional/delete/".$row->id) }}" data-target="#confirm-delete" data-toggle="modal" title="Void">
                                     Void</a></li>
                                </ul>
                        </td>
                        </tr>
                    @endforeach  
                @else
                    <tr>
                        <td colspan="5">Promotional's not found!</td>

                        </tr>

                @endif


                </tbody>

        </table>
                        
           <?php echo $model->render(); ?>        

        </div>
			
			
			
			</div>

            </div>

    </div>
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title text-center" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are you sure to delete this promotion?</p>
                </div>
                
                <div class="modal-footer">
                    <a  class="btn btn-info" data-dismiss="modal">Cancel</a>
                    <a class="btn btn-primary btn-ok" id="btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>  
   <div class="modal fade job-desc-modal" id="openJob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close " data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4></h4>
      </div>
      <div class="modal-body overload fnc-fom accordion-arr" id="details-modal">
        			
      </div>
    </div>
  </div>
</div>         
            
            
	</section>

<script>
       //$("a#rfp-sub-modal").click(function()  {
       
       
       function open_job(id)
       {
            //var id = $(this).attr('id');
            $.ajax({
                type: 'GET',
                url: '<?php echo  url("vendor/promotional/view"); ?>/'+id,
                success: function(data) 
                {
                    //$('#rfp-sub-modal').toggle();
                    $('#details-modal').html(data);
                }
            });
      }
   

</script> 
    


@endsection