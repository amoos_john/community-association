<?php
    $required = "required";
?>
<div class="row">
    <div class="form-group col-sm-12 " style="z-index:9;">
 <div class="input-group">
        <div class="panel-title style-select">
                <a type="button" data-toggle="collapse" data-target="#selectCategory" class="collapsed">
                <i class="icon"><img src="{{ asset('front/images/icon-category.png')}}" alt="" /></i> Select Category </a>
        </div>


        <div id="selectCategory" class="style-select-option collapse ride ">
            @foreach($categories as $row)
            @if(isset($sel_category))
                <?php
                    $checked = '';
                ?>
                @foreach($sel_category as $cat)
                <?php
                    if($cat==$row->id)
                    {
                        $checked = 'checked';
                    }  
                ?>
                @endforeach
            @endif
                    <div class="form-group">
                        <input type="checkbox"  name="category[]" value="{{ $row->id }}" id="{{ $row->id }}" <?php echo (isset($sel_category))?$checked:''; ?>/>
                        <mark></mark>
                        <label for="{{ $row->id }}">{{ $row->name }}</label>
                    </div>
             @endforeach
     
        </div>
  </div>
</div>
</div>
<div class="row">
<div class="form-group col-sm-12">
 <div class="input-group">
        <div class="panel-title style-select">
                <a type="button" data-toggle="collapse" data-target="#selectArea" class="collapsed">
                <i class="icon"><img src="{{ asset('front/images/icon-map.png')}}" alt="" /></i> Select Area </a>
        </div>


        <div id="selectArea" class="style-select-option collapse ride ">

            @foreach($states as $row)
            @if(isset($sel_areas))
                <?php
                    $checked = '';
                ?>
                @foreach($sel_areas as $area)
                <?php
                    if($area==$row->state_code)
                    {
                        $checked = 'checked';
                    }  
                ?>
                @endforeach
            @endif
                    <div class="form-group">
                        <input type="checkbox"  name="areas[]" value="{{ $row->state_code }}" id="{{ $row->state }}" <?php echo (isset($sel_areas))?$checked:''; ?>/><mark></mark>
                        <label for="{{ $row->state }}">{{ $row->state }}</label>
                    </div>

             @endforeach

        </div>
  </div>
  <a href="{{ url('vendor/upgrade') }}" class="extra-ques"><span>
        Would you like to upgrade?
  </span>
  </a>

</div>
</div>
<div class="row">
    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-title.png')}}" alt="" /></i></span>
            <input id="title" type="text" class="form-control" name="title" value="{{ (isset($model))?$model->title:old('title') }}" placeholder="Title*" <?php echo $required;?>/>
      </div>
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-12">
    <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-username.png')}}" alt="" /></i></span>
        <input id="contactName" type="text" class="form-control" name="contact_name" value="{{ (isset($model))?$model->contact_name:old('contact_name') }}" placeholder="Contact Name*" <?php echo $required;?>/>
    </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-12">
    <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-org.png')}}" alt="" /></i></span>
        <input id="companyName" type="text" class="form-control" name="company_name" value="{{ (isset($model))?$model->company_name:old('company_name') }}" placeholder="Company Name" <?php echo $required;?>/>
    </div>
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-12">
       <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-mobile.png')}}" alt="" /></i></span>
            <input id="Phone" type="text" class="form-control" name="phone" value="{{ (isset($model))?$model->phone:old('phone') }}" placeholder="Phone Number*" <?php echo $required;?>/>
      </div>
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6">
            <div class="input-group">
                    <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-calendar.png')}}" alt="" /></i></span>
                    <input type="text" class="form-control" name="start_date" value="{{ (isset($model))?$model->start_date:old('start_date') }}" id="start_date" placeholder="Promotion Start Date*" data-provide="datepicker" <?php echo $required;?>/>
            </div>
    </div>
    <div class="form-group col-sm-6">
            <div class="input-group">
                    <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-calendar.png')}}" alt="" /></i></span>
                    <input type="text" class="form-control" name="end_date" value="{{ (isset($model))?$model->end_date:old('end_date') }}" id="end_date" placeholder="Promotion End Date*" data-provide="datepicker" <?php echo $required;?>/>
            </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-12">
            <div class="input-group">
                    <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-price.png')}}" alt="" /></i></span>
                    <input id="pkgInclusion" type="text" class="form-control" name="pkg_inclusion" value="{{ (isset($model))?$model->pkg_inclusion:old('pkg_inclusion') }}" placeholder="Package Inclusion $12 per Year">
            </div>
    </div>
</div>

<div class="row">
    <div class="form-group textarea-box col-sm-12">
    <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-email.png')}}" alt="" /></i></span>
        <textarea id="yourComment" class="form-control" name="comments" rows="5" placeholder="Write your comment">{{ (isset($model))?$model->comments:old('comments') }}</textarea> 
         
    </div>
    </div>
</div>
