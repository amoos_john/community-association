<div class="dash__nav navbar-blinds--hover ">
<nav class="navbar navbar-default" role="navigation">
<div class="container-fluid p0">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#dashNav">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse p0" id="dashNav">

      <ul class="nav navbar-nav navbar-list navbar-mega">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Promotional Announcement</a>
            <ul class="dropdown-menu">
              <li><a href="{{ url('vendor/promotional/create') }}">Create Promotional Announcement</a></li>
              <li><a href="{{ url('vendor/promotional') }}">Promotional Announcement Status</a></li>
            </ul>
        </li>
        <li><a href="{{ url('vendor/gallery') }}">Gallery</a></li>
        <li><a href="#">View Community Address</a></li>
        <li><a href="{{ url('vendor/jobs') }}">Check Open Job Bids</a></li>
        <li><a href="{{ url('vendor/rfp') }}">RFPs Submission</a></li>
        <li><a href="{{ url('vendor/upgrade') }}">Upgrade Package</a></li>
        <li><a href="{{ url('vendor/profile/edit') }}">Service Provider Profile</a></li>
        <li><a href="{{ url('vendor/users/create') }}">Add User</a></li>
        <li><a href="{{ url('vendor/users') }}">Delete / Modify User</a></li>
      </ul>

    </div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>
</div>