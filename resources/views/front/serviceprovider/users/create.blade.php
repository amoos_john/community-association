@extends('front')

@section('content')

<section class="dashboard-page pb30">
    <div class="container">

        <div class="hed col-sm-offset-3 "><h2>Step 01 Add Vendor Role Membership</h2></div>

        <div class="row">

            <div class="dash__left col-sm-3">

                   @include('front/serviceprovider/common/navigation') 

            </div>
            <div class="dash__right col-sm-9">



           <div class="fom fnc-fom accordion-arr fom-bottomline--focus">

            @include('front/common/errors')  
            @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                 {!! session('error') !!}
            </div>
            @endif
            {!! Form::open(array( 'class' => 'form','url' => 'vendor/users/insert', 'files' => true)) !!}
            <?php $page = "front.serviceprovider.users.form"; ?>
            @include($page)
            <div class="form-group col-sm-12">
            <button type="submit" class="create-user-btn btn btn-primary w100">Create User</button>

            <div class="note mt10">Click “Create User.” The new user will be sent an email after the account has been created. You can resend the registration later from the user details page if necessary</div>
            </div>
            {!! Form::close() !!}
            
            </div>


            </div>

            </div>

    </div>
</section>
@endsection