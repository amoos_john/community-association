@if(count($data)>0 || count($data2)>0)
<table class="table table-cart">
    <thead>
            <tr>
                <th>Name</th>
                <th>Role Name</th>
                <th>Delete</th>    
            </tr>
    </thead>
    <tbody>
    
    @if(isset($categories) && (count($categories)>0 && count($positions)>0))    
    <tr>
        <td>{{ $categories->name }}</td>
        <td>{{ $positions->name }}</td>
        <td><a onClick="<?php echo ($id=="")?'deleteMember()':'deleteMember('.$id.')';?>">Delete</a></td>   
    </tr>
    @endif
    @if(isset($roles))    
    <tr>
        <td>Organization</td>
        <td>{{ $roles->name }}</td>
        <td><a onClick="<?php echo ($id=="")?'deleteRole()':'deleteRole('.$id.')';?>">Delete</a></td>   
    </tr>
    @endif

    </tbody>
</table>
@else
    <p>This user currently has no roles assigned.</p>
@endif
