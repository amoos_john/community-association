<?php
    $required = "required";
?>
<div class="form-group col-sm-6">

    <select name="category" id="category" class="form-control">
        <option value="">Select Category</option>
        @foreach($categories as $category)
        <?php  $selected = ($category->id==$category_id)?'selected':''; ?>
            <option value="{{ $category->id }}" <?php echo $selected; ?>>{{ $category->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group col-sm-6">
    <select name="position" id="position" class="form-control">
        <option value="">Select Position</option>
        @foreach($positions as $pos)
        <?php  $selected = ($pos->id==$position_id)?'selected':''; ?>
            <option value="{{ $pos->id }}" <?php echo $selected; ?>>{{ $pos->name }}</option>
        @endforeach
    </select> 
</div>

<div class="btns col-sm-12 text-right">
    <button type="button" id="btn-member" class="btn btn-primary">Add</button>
</div>


<div class="hed hed3 col-sm-12"><h2>Add An Vendor Based Role</h2></div>


<div class="form-group col-sm-12">
    <select name="roles" id="roles" class="form-control">
        <option value="">Select Role</option>
        @foreach($roles as $role)
        <?php  $selected = ($role->id==$roles_id)?'selected':''; ?>
            <option value="{{ $role->id }}" <?php echo $selected; ?>>{{ $role->name }}</option>
        @endforeach
    </select> 
</div>
        </div>

        <div class="btns col-sm-12 text-right mb30">
        <button type="button" id="btn-role" class="btn btn-primary">Add</button>
</div>
<div class="table table-responsive">
	<h4>Roles</h4>
	<div id="show">
            
        </div>
</div>

<div class="hed hed3 col-sm-12"><h2>Step 02 Set Username and Email Address</h2></div>
@if(isset($model))
<div class="form-group col-sm-6">
<div class="">
    <label>Active
       <input type="checkbox" name="status"  value="{{ $model->status }}" <?php echo ($model->status==1)?'checked':'' ?> />
       
    </label>   
</div>
</div>
@endif
<div class="clearfix"></div>
<div class="form-group col-sm-6">
<div class="input-group">
<span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-username.png')}}" alt="" /></i></span>
<input id="first_name" type="text" class="form-control" value="{{ (isset($model->first_name))?$model->first_name:old('first_name') }}" name="first_name" placeholder="First Name*" <?php echo $required;?>>
</div>
</div>


<div class="form-group col-sm-6">
<div class="input-group">
 <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-username.png')}}" alt="" /></i></span>
<input id="first_name" type="text" class="form-control" value="{{ (isset($model->last_name))?$model->last_name:old('last_name') }}" name="last_name" placeholder="Last Name*" <?php echo $required;?>>
</div>
</div>

<div class="form-group col-sm-12">
  <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-username.png')}}" alt="" /></i></span>
        <input id="Username" type="text" class="form-control" name="username" value="{{ (isset($model->username))?$model->username:old('username') }}" placeholder="User Name" <?php echo $required;?>>
  </div>
</div>


<div class="form-group col-sm-12">
  <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-email.png')}}" alt="" /></i></span>
        <input id="EmailAddress" type="email" value="{{ (isset($model->email))?$model->email:old('email') }}" class="form-control" name="email" placeholder="Email Address*" <?php echo $required;?>>
  </div>
</div>

<div class="form-group col-sm-12">
  <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-email.png')}}" alt="" /></i></span>
        <input id="EmailAddress" type="email" value="{{ (isset($model->email))?$model->email:old('email_confirmation') }}" class="form-control" name="email_confirmation" placeholder="Confirm Email Address*">
  </div>
</div>

<script>
    
    show();
    function show()
    {
       $.ajax({ 
         type: "GET",
         url: "{{ url('vendor/users/roles') }}",
         success: function(data){

                $('#show').html(data);

        },
         error: function(errormessage) {
               //you would not show the real error to the user - this is just to see if everything is working
               alert(errormessage);    

         }
        }); 
    }
  
$("body").on("click","#btn-member",function(){
    
    if($("#category").val()=="")
    {
        alert("Please select dropdown");
    }
    else if($("#position").val()=="")
    {
        alert("Please select dropdown");
    }
    else
    {
    var datasend = "category="+$("#category").val()
    +"&position="+$("#position").val()
    +"&type=1";
    var token = "&_token=<?php echo csrf_token() ?>";
    
     $.ajax({ 
     type: "POST",
     url: "{{ url('vendor/users/roles/create') }}",
     data: datasend+token,
     success: function(data){
        if(data=='1')
        {
            show();
        }
        else
        {
            var errorsHtml = '<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a><ul>';

            errorsHtml += data; //showing only the first error.

            errorsHtml += '</ul></div>';
            $('#message').html(errorsHtml).show();
            
            
        }
       
},
     error: function(errormessage) {
           //you would not show the real error to the user - this is just to see if everything is working
           alert(errormessage);    
               
     }
});
      
      }     
});
$("body").on("click","#btn-role",function(){
    
    if($("#roles").val()=="")
    {
        alert("Please select dropdown");
    }
    else
    {
    var datasend = "roles="+$("#roles").val()
    +"&type=2";
    var token = "&_token=<?php echo csrf_token() ?>";
    
     $.ajax({ 
     type: "POST",
     url: "{{ url('vendor/users/roles/create') }}",
     data: datasend+token,
     success: function(data){
        if(data=='1')
        {
            show();
        }
        else
        {
            var errorsHtml = '<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a><ul>';

            errorsHtml += data; //showing only the first error.

            errorsHtml += '</ul></div>';
            $('#message').html(errorsHtml).show();
            
            
        }
       
},
     error: function(errormessage) {
           //you would not show the real error to the user - this is just to see if everything is working
           alert(errormessage);    
               
     }
});
      
      }
});
function deleteMember()
{
    var r = confirm("Are you wish to delete this role memeber?");
    if (r == true) {


    window.location = '{{ url("vendor/users/member/delete") }}';
   

    } else {
    return false;
    }
}
function deleteRole()
{
    var r = confirm("Are you wish to delete this role memeber?");
    if (r == true) {

    window.location = '{{ url("vendor/users/roles/delete") }}';
    

    } else {
    return false;
    }
}
</script>