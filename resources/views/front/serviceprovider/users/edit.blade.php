@extends('front')

@section('content')

<section class="dashboard-page pb30">
    <div class="container">

        <div class="hed col-sm-offset-3 "><h2>Step 01 Add Vendor Role Membership</h2></div>

        <div class="row">

            <div class="dash__left col-sm-3">

                   @include('front/serviceprovider/common/navigation') 

            </div>
            <div class="dash__right col-sm-9">



           <div class="fom fnc-fom accordion-arr fom-bottomline--focus">

            @include('front/common/errors')  
            
            {!! Form::model($model, ['files' => true,'class' => 'form','url' => ['vendor/users/update', $model->id], 'method' => 'post']) !!}
            <?php $page = "front.serviceprovider.users.form"; ?>
            @include($page)
            <div class="form-group col-sm-12">
            <button type="submit" class="create-user-btn btn btn-primary w100">Update User</button>

            </div>
            {!! Form::close() !!}
            
            </div>


            </div>

            </div>

    </div>
</section>
<script>
    showRoles(<?php echo $id; ?>);
    function showRoles(id)
    {
       $.ajax({ 
         type: "GET",
         url: "{{ url('association/users/roles') }}",
         data: "id="+id,
         success: function(data){

                $('#show').html(data);

        },
         error: function(errormessage) {
               //you would not show the real error to the user - this is just to see if everything is working
               alert(errormessage);    

         }
        }); 
    }
    function deleteMember(id)
    {
        
        var r = confirm("Are you wish to delete this role memeber?");
        if (r == true) {

        $.ajax({ 
         type: "GET",
         url: "{{ url('association/users/member/delete') }}",
         data: "id="+id,
         success: function(data){

               showRoles(<?php echo $id; ?>); 

        },
         error: function(errormessage) {
               //you would not show the real error to the user - this is just to see if everything is working
               alert(errormessage);    

         }
        }); 
       


        } else {
        return false;
        }
    }
    function deleteRole(id)
    {
        var r = confirm("Are you wish to delete this role memeber?");
        if (r == true) {
        
        $.ajax({ 
         type: "GET",
         url: "{{ url('association/users/roles/delete') }}",
         data: "id="+id,
         success: function(data){

               showRoles(<?php echo $id; ?>); 

        },
         error: function(errormessage) {
               //you would not show the real error to the user - this is just to see if everything is working
               alert(errormessage);    

         }
        });


        } else {
        return false;
        }
    }
</script>
@endsection