@extends('front')

@section('content')
<?php
    use App\Functions\Functions;
?>
<section class="dashboard-page pb30">
    <div class="container">
                        <div class="hed"><h2>{{ $page_title }}</h2></div>
     

        <div class="row">

            <div class="dash__left col-sm-3 ">

                   @include('front/serviceprovider/common/navigation') 

            </div>
            <div class="dash__rigt col-sm-9">
           @include('front/common/errors')     
           <div class="fom fnc-fom usersearch-fom">
				
            <form action="{{ url('vendor/users/search') }}" method="get" class="navbar-form navbar-left" role="search">
              <div class="form-group">
                     <div class="input-group">
                            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-username.png')}}" alt="" /></i></span>
                            <input id="FirstName" type="text" class="form-control" name="first_name" value="{{ (isset($first_name))?$first_name:'' }}" placeholder="First Name">
                      </div>
              </div>
              <div class="form-group">
                     <div class="input-group">
                            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-username.png')}}" alt="" /></i></span>
                            <input id="LastName" type="text" class="form-control" name="last_name" value="{{ (isset($last_name))?$last_name:'' }}" placeholder="Last Name">
                      </div>
              </div>
              <div class="form-group ">
                    <span class="fnc__checkbox checker-area <?php echo (isset($status))?' is-active':'' ?>"><input type="checkbox" name="status" value="0" id="status" <?php echo (isset($status))?'checked':'' ?>/></span><label>Show Inactive Only</label>
              </div>

              <button type="submit" class="btn btn-primary">Search</button>
            </form>
            </div>
				     
                
        <div class="table-responsive table-adduser fnc-fom">
				
        <table class="table table-small">
                <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email Address</th>
                    <th>Active</th>
                    <th>Register</th>
                    <th>Pwd Reset</th>
                    <th>Login Date</th>
                    <th>Modified Date</th>
                    <th>Modified By</th>
                    <th>Details</th>
                </tr>
                </thead>
                <tbody>
                @if(count($model)>0)
                    @foreach($model as $row)
                    <?php $last_login = '';?>
                    @if(count($row->user)>0)
                        <?php 
                            if($row->user->last_login!='')
                            {
                                $last_login = $row->user->last_login;
                            }   
                        ?>   
                    @endif    
                    <tr>
                        <td>{{ $row->first_name }}</td>
                        <td>{{ $row->last_name }}</td>
                        <td>{{ $row->email }}</td>
                        <td><span class="fnc__checkbox checker-area <?php echo ($row->status==1)?' is-active ':'' ?>">
                          <input type="checkbox" name="status"  value="{{ $row->status }}" <?php echo ($row->status==1)?'checked':'' ?> disabled/></span></td>
                        <td><span class="fnc__checkbox checker-area <?php echo ($last_login!='')?' is-active':'' ?>">
                                <input type="checkbox" <?php echo ($last_login!='')?'checked':'' ?> disabled/>
                        </span></td>
                        <td><span class="fnc__checkbox checker-area"><input type="checkbox" /></span></td>
                        <td>{{ ($last_login!='')?Functions::dateTimeFormat($last_login):'' }}</td>
                        <td>{{ Functions::dateTimeFormat($row->updated_at) }}</td>
                        <td>{{ (count($row->modified)>0)?$row->modified->name:'' }}</td>
                        <td><a data-target="#detail-modal" onClick="return details('{{ $row->id }}');" data-toggle="modal">Details</a></td>
                    </tr>   
                    @endforeach  
                @else
                    <tr>
                        <td colspan="10" class="text-center">User's not found!</td>
                    </tr>
                @endif

                </tbody>

        </table>
                        
           <?php echo $model->render(); ?>        

        </div>
			
		</div>

            </div>

    </div>
 <div class="modal fade job-desc-modal" id="detail-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close " data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4></h4>
      </div>
      <div class="modal-body overload fnc-fom accordion-arr" id="details-modal">
        			
      </div>
    </div>
  </div>
</div>  
</section>
<script>
       function details(id)
       {
            $('#details-modal').html("");
            $.ajax({
                type: 'GET',
                url: '<?php echo  url("vendor/users/view"); ?>/'+id,
                success: function(data) 
                {
                    $('#details-modal').html(data);
                }
            });
      }
</script>
@endsection