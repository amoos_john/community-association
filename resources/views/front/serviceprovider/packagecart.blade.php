<?php
use App\Functions\Functions;
use App\ServiceArea;
?>
@if(count($model)>0)
<div class="package-combination">
<div class="hed"><h2>Package Combination</h2></div>

<div class="table table-responsive">

        <table class="table table-cart">
                <thead>
                    <tr>
                        <th>State</th>
                        <th>Service Location</th>
                        <th>Service Area</th>
                        <th>Package Combination Total</th>
                        <th>Gallery</th>
                    </tr>
                </thead>
            <tbody id="">
                @foreach($model as $row)
                <?php 
                $service_names = "";
                if($row->service_areas!="")
                {
                    $service_areas = explode(",",$row->service_areas);
                    $services = array();
                 ?>     
                    @foreach($service_areas as $area)
                        <?php 
                            $areas = ServiceArea::find($area);
                            $services[] = $areas->name; 
                            //dd($service_names);
                        ?>
                    @endforeach
               <?php
                    $service_names = implode(",",$services);
                 }
                ?>                
                <tr>
                    <td>{{ $row->getStates->state }}</td>
                    <td>{{ ($row->county!="")?$row->county:'All' }}</td>
                    <td>{{ $service_names }}</td>
                    <td>{{ $pack->total_rfp }}</td>
                    <td>{{ ($row->gallery!=0)?'Yes':'No' }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>


</div>




<div class=" table-responsive col-sm-5 pul-cntr p0">
    {!! Form::open(array( 'class' => 'form','url' => 'vendor/package/insert/'.$package_id, 'files' => true)) !!}
        <table class="table table-cost  text-center clrhm">
                <thead>
                        <tr>
                                <th colspan="3">Cost</th>
                        </tr>
                </thead>
                <tbody>
                        <tr>
                                <td> 
                                        <i class="icon"><img src="{{ asset('/'.$pack->image)}}" alt="" /></i>
                                </td>
                                <td>
                                        <span><!--Alabama-->{{ $pack->package_name }}</span>
                                </td>
                                <td>
                                        <h3><i class="icon-price color-invert"></i> {{ Functions::moneyFormat($pack->price) }}</h3>
                                </td>
                        </tr>
                      
                </tbody>
        </table>

                        <div class="buttons text-center mb30">
                                <button type="submit" class="btn btn-lg btn-primary">Add to Cart</button>
                        </div>
    {!! Form::close() !!}                    
                </div>

        </div>
@endif
<script>
var count =  <?php echo $area_count; ?>;  
    
$(":checkbox[name='areas[]']").change(function(){
  if ($(":checkbox[name='areas[]']:checked").length == count)                                              
   $(":checkbox[name='areas[]']:not(:checked)").prop('disabled', true);  
  else                                                     
   $(":checkbox[name='areas[]']:not(:checked)").prop('disabled', false); 
});

var len =  <?php echo $length; ?>;

$(":checkbox[name='states[]']").change(function(){
  if ($(":checkbox[name='states[]']:checked").length == len)                                              
   $(":checkbox[name='states[]']:not(:checked)").prop('disabled', true);  
  else                                                     
   $(":checkbox[name='states[]']:not(:checked)").prop('disabled', false); 
});

if(len==0)
{
    $("#create").attr("disabled",true);
}

</script>