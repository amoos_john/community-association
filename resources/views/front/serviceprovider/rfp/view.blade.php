<div class="job__cont clrlist col-sm-12">
    @if(count($model)>0)
    <div class="form-group col-sm-4 text-right pull-right" >
        <img class="img-responsive img-thumbnail" src="{{ asset('/'.$model->image)}}" alt="{{ $model->image_name }}" />
    </div>
    <h3>RFP Title</h3>
    <h4>{{ $model->title }}</h4> 
    <p class="job__location">{{ $model->location }}</p>
    
    <h3>Description</h3>
    <p class="m0"><?php echo $model->job_description; ?></p>
    <div class="job__sign col-sm-6">
            <h5>Signature:</h5>
            <img src="{{ ($model->signature!="")?asset('/'.$model->signature):asset('front/images/signature.png') }}" alt="signature" />
    </div>
    <div class="job__date text-right col-sm-6">
            <h5>Submission Date:</h5>
            <p>{{ $submission_date }}</p>
            <div class="job__attch">
                    <a href="{{ ($model->document!="")?asset(''.$model->document):'#' }}"   <?php echo ($model->document!="")?'download':'' ?>>
                    <img src="{{ asset('front/images/icon-attachment.png') }}" alt="attachment icon" />File Attached</a>
            </div>
    </div>
    @if($close_date <= $date || $result->status != 1) 
    {!! Form::open(array( 'class' => 'form','url' => 'vendor/rfp/insert/'.$id, 'files' => true)) !!}
    <div class="btns jobs__submit col-sm-12">
        <button type="submit" class="btn btn-primary">Submit RFP</button>
    </div>
    {!! Form::close() !!}
    @endif
    @else
    <h3>RFP not Found</h3>
    @endif
</div>