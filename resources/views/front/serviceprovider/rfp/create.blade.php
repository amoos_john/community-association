@extends('front')

@section('content')
<?php
    $required = "required";
?>
<style>
td.disabled.day {
    opacity: 0.2;
}
</style>
<script>

var loadImageFile = (function () 
{
    if (window.FileReader) {
        var   oPreviewImg = null, oFReader = new window.FileReader(),
            rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

       
        return function () {
            var aFiles = document.getElementById("image").files;
            if (aFiles.length === 0) { return; }
            if (!rFilter.test(aFiles[0].type)) { 
                alert("You must select a valid image file !");
                var $el = $('#image');
                $el.wrap('<form>').closest('form').get(0).reset();      

                return; 
            }
            oFReader.readAsDataURL(aFiles[0]);
        }

    }

})();
var loadImageFile2 = (function () 
{
    if (window.FileReader) {
        var   oPreviewImg = null, oFReader = new window.FileReader(),
            rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

       
        return function () {
            var aFiles = document.getElementById("image2").files;
            if (aFiles.length === 0) { return; }
            if (!rFilter.test(aFiles[0].type)) { 
                alert("You must select a valid image file !");
                var $el = $('#image2');
                $el.wrap('<form>').closest('form').get(0).reset();      

                return; 
            }
            oFReader.readAsDataURL(aFiles[0]);
        }

    }

})();
var loadDocument = (function () 
{
    //|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image
    if (window.FileReader) {
        var   oPreviewImg = null, oFReader = new window.FileReader(),
            rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png\/x\-xwindowdump)$/i;

       
        return function () {
            var aFiles = document.getElementById("document").files;
            if (aFiles.length === 0) { return; }
            if (!rFilter.test(aFiles[0].type)) { 
                alert("You must select a valid document file !");
                var $el = $('#document');
                $el.wrap('<form>').closest('form').get(0).reset();      

                return; 
            }
            oFReader.readAsDataURL(aFiles[0]);
        }

    }

})();
</script>
<section class="dashboard-page pb30">
    <div class="container">

        <div class="row">

            <div class="dash__left col-sm-3 mt100">

                   @include('front/association/common/navigation') 

            </div>


<div class="dash__right col-sm-9">

        <div class="hed"><h2>{{ $page_title }}</h2></div>



        <div class="fom fnc-fom accordion-arr fom-bottomline--focus">

        @include('front/common/errors')       
        {!! Form::open(array( 'class' => 'form','url' => 'association/rfp/insert', 'files' => true)) !!}

        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-groupuser.png')}}" alt="" /></i></span>
                <input id="name" type="text" class="form-control" name="title" value="{{ old('title') }}" placeholder="RFP Title" <?php echo $required;?>/>
          </div>
        </div>

        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-groupuser.png')}}" alt="" /></i></span>
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Association Name" <?php echo $required;?>/>
          </div>
        </div>

        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-map.png')}}" alt="" /></i></span>
                <input id="location" type="text" class="form-control" name="location" value="{{ old('location') }}" placeholder="Association Location">
          </div>
        </div>



        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-paper.png')}}" alt="" /></i></span>
                <input id="submission_date" type="text" class="form-control" name="submission_date" value="{{ old('submission_date') }}" placeholder="RFP Submission Date" <?php echo $required;?>/>
          </div>
        </div>


        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-pressbutton.png')}}" alt="" /></i></span>
                <input id="point_contact" type="text" class="form-control" name="point_contact" value="{{ old('point_contact') }}" placeholder="Point of Contact" <?php echo $required;?>/>
          </div>
        </div>



        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-contactbook.png')}}" alt="" /></i></span>
                <input id="point_contact_phone" type="tel" class="form-control" name="point_contact_phone" value="{{ old('point_contact_phone') }}" placeholder="Point of Contact Phone Number" <?php echo $required;?>/>
          </div>
        </div>




        <div class="form-group col-sm-12 icon-valign--top">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-paper.png')}}" alt="" /></i></span>
                <textarea id="job_description" type="text" class="form-control minh150" name="job_description"  placeholder="Full Job Description">{{ old('job_description') }}</textarea>
          </div>
        </div>




        <div class="form-group col-sm-12 icon-valign--top">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-worker.png')}}" alt="" /></i></span>
                <textarea id="requirement" class="form-control minh150" name="requirement"  placeholder="Contractor Requirements">{{ old('requirement') }}</textarea>
          </div>
        </div>



        <div class="form-group col-sm-12 icon-valign--top">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-manstand.png')}}" alt="" /></i></span>
                <textarea id="project_specification" class="form-control minh150" name="project_specification" placeholder="Project Specification">{{ old('project_specification') }}</textarea>
          </div>
        </div>




        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-photo.png')}}" alt="" /></i></span>
                <input id="Picture1" type="text" class="form-control" name="image_name" value="{{ old('image_name') }}" placeholder="Picture#1">
          </div>

                <div class="file-drop-area fnc-uplaod">
                          <span class="btn  btn-info">Choose Files</span>
                          <input class="file-input" type="file" name="image" id="image"  onchange="loadImageFile();"/>
                          <label for="Picture1">No file Chosen</label>
                  </div>


        </div>


        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-photo.png')}}" alt="" /></i></span>
                <input id="Picture2" type="text" class="form-control" name="image_name2" value="{{ old('image_name2') }}" placeholder="Picture#2">
          </div>

                <div class="file-drop-area fnc-uplaod">
                          <span class="btn btn-info">Choose Files</span>
                          <input class="file-input" type="file" name="image2" id="image2" onchange="loadImageFile2();">
                          <label for="Picture2">No file Chosen</label>
               </div>


        </div>


        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-paper2.png')}}" alt="" /></i></span>
                <input id="document_other" type="text" class="form-control" name="document_name" value="{{ old('document_name') }}" placeholder="Document or Other">
          </div>

                  <div class="file-drop-area fnc-uplaod">
                          <span class="btn btn-info">Choose Files</span>
                          <input class="file-input" type="file" name="document" id="document" >
                          <label for="DocumentorOther">No file Chosen</label>
                  </div>

        </div>





           <div class="actions col-sm-12 text-right">
               <button type="submit" class="btn btn-primary">Next</button>
	  </div>

            {!! Form::close() !!}
              </div>


            </div>

            </div>

    </div>
</section>
<script>
	$(document).ready(function(){
                //var array = ["2017-07-14","2017-07-15","2017-07-16"]

                
		var start_date=$('input[name="submission_date"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		start_date.datepicker({
                        
			format: 'yyyy-mm-dd',
                        startDate: new Date(),
			container: container,
			todayHighlight: true,
			autoclose: true,
			active:true
                      

		})
           $("#submission_date").keydown(function(event) { 
                return false;
            });     

	})
</script>
@endsection