@extends('front')

@section('content')
<?php
    $required = "required";
?>
<style type="text/css">

     /*This is the div within which the signature canvas is fitted*/
     #signature{
          border: 1px solid #ccc;
          background-color:#fff;
          margin-top:10px;
     }
     .datesignature{
          border: 1px solid #ccc;
          background-color:#fff;
          margin-top:10px;
     }
     .img-thumb img {
  
    width: 100%;
    }
    tbody.vendor-table {
    overflow-y: scroll;
    height: 400px;
    display: block;
}

     /* Drawing the 'gripper' for touch-enabled devices */ 
    
     

</style>
<section class="dashboard-page pb30">
    <div class="container">

        <div class="row">

            <div class="dash__left col-sm-3 mt100">

                   @include('front/association/common/navigation') 

            </div>

            
<div class="dash__right col-sm-9">

        <div class="hed"><h2>{{ $page_title }}</h2></div>



        <div class="fom fnc-fom accordion-arr fom-bottomline--focus fnc__checkbox--rounded">
        @include('front/common/errors')    

                    <div class="col-sm-12">
                        <a  id="allcheck" class="btn  btn-info">Select All</a>
                        <a  id="alluncheck" class="btn btn-primary">Clear All</a>
                    </div>

        {!! Form::open(array( 'class' => 'form','url' => 'association/rfp/vendor/insert/'.$id, 'files' => true)) !!}	

    <div class="desire-vendors-area table-responsive col-sm-8">

    <table class="table table-cost table-vendor table-bdr--row  text-center clrhm">

    <tbody class="vendor-table">
            @if(count($model)>0)
            @foreach($model as $row)
            <tr>
                <td><span class="fnc__checkbox checker-area">
                    <input type="checkbox" name="vendor_id[]" value="{{ $row->id }}" class="avail_chk" /></span>
                </td>
                <td><span class="img-thumb img-cntr img-thumb70x70"><img src="{{ asset('/'.$row->logo)}}" alt="" /></span></td>
                <td><h4>{{ $row->name }} <i class="fa fa-info"></i></h4></td>
                <td><a href="#">{{ $row->address }}</a></td>
            </tr>
            @endforeach
            @endif
           <!--<tr>
                    <td><span class="fnc__checkbox checker-area"><input type="checkbox" name="vendor_id[]" value="2" class="avail_chk" /></span></td>
                    <td><span class="img-thumb img-cntr img-thumb70x70"><img src="{{ asset('front/images/vendor-logo2.jpg')}}" alt="" /></span></td>
                    <td><h4>Group Green Crop <i class="fa fa-info"></i></h4></td>
                    <td><a href="#">Houston, TEXAS</a></td>
            </tr>
            <tr>
                    <td><span class="fnc__checkbox checker-area"><input type="checkbox" name="vendor_id[]" value="3" class="avail_chk" /></span></td>
                    <td><span class="img-thumb img-cntr img-thumb70x70"><img src="{{ asset('front/images/vendor-logo3.jpg')}}" alt="" /></span></td>
                    <td><h4>Star Field Services <i class="fa fa-info"></i></h4></td>
                    <td><a href="#">Houston, TEXAS</a></td>
            </tr>
            <tr>
                    <td><span class="fnc__checkbox checker-area"><input type="checkbox" name="vendor_id[]" value="4" class="avail_chk" /></span></td>
                    <td><span class="img-thumb img-cntr img-thumb70x70"><img src="{{ asset('front/images/vendor-logo4.jpg')}}" alt="" /></span></td>
                    <td><h4>Wave Construction  <i class="fa fa-info"></i></h4></td>
                    <td><a href="#">Houston, TEXAS</a></td>
            </tr>
           -->
    </tbody>
</table>

        <div class="col-sm-12 text-center data-print_box" id="btn_sig"  data-toggle="modal" data-target="#signatureModal"  >     
            <img id="signature_user_display" src="<?php echo ($rfp->signature=="")?asset('front/images/sign.png'):asset('/'.$rfp->signature);?>" alt="signature">  

        </div>
        <div class="col-sm-12 text-right p0">


                <button type="button" class="btn btn-primary "  data-toggle="modal" data-target="#submitRFPModal">Save & Submit</button>
                <button type="submit" class="btn btn-info">Save & Exit</button>
        </div>


        </div>
			
			
    <div class="vendor-total bg-cvr col-sm-4" style="background-image:url('{{ asset('front/images/steel-bg.png')}}');">
                    <h3>Remaining RFP’S</h3></td>
                    <h2><?php echo $total_rfps;?></h2>
                    <div class="arrow-arc"></div>
    </div>


</div>

</div>

</div>
			
 
<div class="modal fade modal-vcntr" id="submitRFPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        
      </div>
      <div class="modal-body text-center">
		
          <p class="lead">Please confirm your submission of '"RFP Title" <br/>to your <span id="chosen"></span> chosen vendors</p>

                <button type="submit" class="btn btn-primary w60 pul-cntr">Submit Now</button>
	  
      </div>
      
    </div>
  </div>
</div>
        
<!-- Modal signature -->
     <!-- Modal -->
     <div id="signatureModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

               <!-- Modal content-->
               <div class="modal-content">
                    <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                         
                    </div>
                    <div class="modal-body">

                         <div id="content">
                              <div class="hdr__cont__sign black">
                                   <h3>Add Signature</h3>
                                   <p>Your signature is required below.</p>
                              </div>
                              <div id="signatureparent">
                                   <div id="signature"></div>
                              </div>
                         </div>
                    </div>

                    <div class="modal-footer">
                         <div class="sign-buttons">
                              <div class="sign-btns-left">
                                   <button onclick="$('#signature').jSignature('clear');" class="btn btn-primary" type="button">CLEAR SIGN</button>
                                   <button data-toggle="modal" data-dismiss="modal"  class="btn btn-info"   type="button">Close</button>
                              </div>
                              <!--<div class="sign-btns-rgt">
                                   <button data-toggle="modal" data-dismiss="modal"  class="save-sign btn btn-md modal-btn btn-custom"   type="button">Save</button>
                              </div>-->
                         </div>
                    </div>
               </div>

          </div>
     </div>



     <!--signature ends-->        
        
        
{!! Form::close() !!}
			
		</div>
	</section>
<script>
function imgSelect(defaultimage, type)
{
    //var idimage = jQuery('#idimage').val();
    //var idimage = jQuery('#image_source').val();

    if (defaultimage == 0 || defaultimage == '')
    {
         alert('Please select Image ');
         return false;
    } 
    else
    {
jQuery.post("<?php echo url('association/rfp/store-signature'); ?>",
     {
          defaultimage: defaultimage,
          id: <?php echo $id ? $id: 0; ?>,
          _token: '<?php echo csrf_token(); ?>'
     },
     function (data, status) {
          if (data != '')
          {
            var obj = data;
         
            if (obj == '-1')
            {
                 alert('Please make signature');
                 return false;
            } else if (obj == '-2')
            {
                 alert('Please make signature');
                 return false;
            } else if (obj == '-3')
            {
                 alert('Please make signature');
                 return false;
            } else if (obj != '')
            {

                 $('#signature_user_display').attr('src', '<?php echo asset(''); ?>/' + obj);

            } else
            {
                 alert('Please make signature');
                 return false;
            }
        

          } else
          {

               $('#signatureModal').modal('hide');
          }
     });
    }

}

</script>

<script src="{{ asset('front/jsignature/libs/jSignature.min.noconflict.js') }}" type="text/javascript"></script>

<script>

 $(document).ready(function () {

    // This is the part where jSignature is initialized.
    var $sigdiv = $("#signature").jSignature({width: "500", height: "120", 'signatureLine': true})
    var signaturevariable = $("#signature .jSignature");
    (signaturevariable).attr("id", 'jSignatureid');


    var canvasid = document.getElementById("jSignatureid");

    var dataparsedimgvar = $("#dataparsedimg");
    var signaturedata = $("#finalsignaturedata");




    $("#signature").on('change', function () {

         var dataURL = canvasid.toDataURL("image/png");
         var dataURLclean = dataURL.replace("data:image/png;base64,", "");

         imgSelect(dataURLclean, 1);

    });


    });

 $(document).ready(function(){   
    $("#allcheck").click(function(){
  
         $('.in-view').addClass('is-active');
         $(".avail_chk").attr('checked',true);
         var n = $("input:checkbox:checked").length;
         $("#chosen").text(n);
        

    });
     $("#alluncheck").click(function(){

    $('.in-view').removeClass('is-active');
    $(".avail_chk").attr('checked',false);
    var n = $("input:checkbox:checked").length;
    $("#chosen").text(n);
    

    });
    
    $(".avail_chk").change(function(){
  
        var n = $("input:checkbox:checked").length;
        $("#chosen").text(n);
        

    });
    
    
  });
  
  
  
 </script>
@endsection