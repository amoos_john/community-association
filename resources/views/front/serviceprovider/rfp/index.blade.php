@extends('front')

@section('content')
<?php

use App\Functions\Functions;
?>
<section class="dashboard-page pb30">
    <div class="container">

        <div class="row">
            <div class="dash__left col-sm-3 pt100">
                @include('front/serviceprovider/common/navigation') 

            </div>
            <div class="dash__rigt col-sm-9">
                <div class="hed"><h2>{{ $page_title }}</h2></div>
                @include('front/common/errors')
                <div class="table-responsive open-job-bids-table fnc-fom">

                    <table class="table table-small0 clrlist">
                        <thead>
                            <tr>
                                <th>RFP Title</th>
                                <th>Requested By</th>
                                <th>Date Received</th>
                                <th>Proposal Submission Date</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($model)>0)
                            @foreach($model as $row)
                            <?php
                            $status = "";
                            $close_date = date("Y-m-d", strtotime($row->submission_date));
                            if (array_key_exists($row->status, $statuses)) {
                                $status = $statuses[$row->status];
                            }
                            if ($close_date <= $date && $row->status != 1) {
                                $status = 'Expired';
                            }
                            ?>
                            <tr>
                                <td>{{ $row->title }}</td>
                                <td>{{ $row->name }}</td>
                                <td>{{ Functions::dateFrontFormat($row->created_at) }}</td>
                                <td>{{ Functions::dateFrontFormat($row->submission_date) }}</td>
                                <td>
                                    <div class="status-actions">
                                        <i class="fa fa-floppy-o"></i>{{ $status }}
                                        <div class="status-actions-box">
                                            <ul>
                                                <li><a data-target="#rfp-sub-modal" onClick="return rfp_modal('{{ $row->id }}');" data-toggle="modal">View</a></li>
                                                @if($date<=$close_date)
                                                <li><a data-href="{{ url("vendor/rfp/delete/".$row->id) }}" data-target="#confirm-delete" data-toggle="modal" title="Void">
                                                        Void</a></li>
                                                @endif 
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach  
                            @else
                            <tr>
                                <td colspan="6">RFP's not found!</td>

                            </tr>

                            @endif


                        </tbody>

                    </table>

                    <?php echo $model->render(); ?>        

                </div>



            </div>



        </div>
        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title text-center" id="myModalLabel">Confirm Delete</h4>
                    </div>

                    <div class="modal-body">
                        <p>Are you sure to delete this rfp?</p>
                    </div>

                    <div class="modal-footer">
                        <a  class="btn btn-info" data-dismiss="modal">Cancel</a>
                        <a class="btn btn-primary btn-ok" id="btn-ok">Delete</a>
                    </div>
                </div>
            </div>
        </div>    
        <div class="modal fade job-desc-modal" id="rfp-sub-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close " data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4></h4>
                    </div>
                    <div class="modal-body overload fnc-fom accordion-arr" id="details-modal">

                    </div>
                </div>
            </div>
        </div>         

    </div> 

</section>

<script>
    function rfp_modal(id)
    {
    $('#details-modal').html("");
    $.ajax({
    type: 'GET',
            url: '<?php echo url("vendor/rfp/view"); ?>/' + id,
            success: function(data)
            {
            //$('#rfp-sub-modal').toggle();
            $('#details-modal').html(data);
            }
    });
    }


</script>
@endsection