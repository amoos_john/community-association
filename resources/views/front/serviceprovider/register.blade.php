@extends('front')

@section('content')

<section class="easy-area pt30">
<div class="container">

            <div class="easy__steps shake-icon list-col-3 text-center">
                <ul>
                <li class="anime-flipInX active">
                        <mark>Step 1</mark>
                        <div class="icon"><img src="{{ asset('front/images/easy-icon1.png')}}" alt=""></div>
                        <span>Register as Service Provider</span>
                </li>
                <li class="anime-flipInX delay1s">
                        <mark>Step 2</mark>
                        <div class="icon"><img src="{{ asset('front/images/easy-icon2b.png')}}" alt=""></div>
                        <span>Create Profile</span>
                </li>
                <li class="anime-flipInX delay2s">
                        <mark>Step 3</mark>
                        <div class="icon"><img src="{{ asset('front/images/easy-icon3b.png')}}" alt=""></div>
                        <span>Connect to the CAR Network</span>
                </li>
               
		</ul>
                 
            </div>

</div>
</section>



<section class="package-area membership-packages association-profile pb50 fom-bottomline--focus fnc-fom accordion-arr">
<div class="container">
<div class="hed">
    <h2>{{ $page_title }}</h2>
</div>
@include('front/common/errors')       
{!! Form::open(array( 'class' => 'form','url' => 'vendor/insert', 'files' => true)) !!}

    
    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-groupuser.png')}}" alt="" /></i></span>
            <input type="text" name="name" id="name"  class="form-control"  placeholder="Service Provider Name*" required="required" value="{{ old('name') }}"/>
      </div>
    </div>
    
    <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-map.png')}}" alt="" /></i></span>
                <input id="Address1" type="text" value="{{ old('address') }}" class="form-control" name="address" placeholder="Address 1" required="required" >
          </div>
        </div>  

        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-map.png')}}" alt="" /></i></span>
                <input id="Address2" type="text" value="{{ old('address2') }}" class="form-control" name="address2" placeholder="Address 2">
          </div>
        </div>
   <div class="form-group col-sm-6">
      <div class="input-group">

            <div class="panel-title style-select">
                    <a type="button" data-toggle="collapse" data-target="#selectState" class="collapsed">
                    <i class="icon"><img src="{{ asset('front/images/icon-state.png')}}" alt="" /></i> <span id="selState">Select State</span> </a>
            </div>


            <div id="selectState" class="style-select-option collapse ride ">
                
                    @foreach($states as $row)

                    <div class="form-group">
                        <input type="radio" name="states" onChange="getCities('{{ $row->state_code }}')" value="{{ $row->state_code }}" id="{{ $row->state }}" /><mark></mark>
                        <label for="{{ $row->state }}">{{ $row->state }}</label>
                    </div>

                    @endforeach
                    
            </div>

            </div>
    </div>     
    <div class="form-group col-sm-6">
      <div class="input-group">

            <div class="panel-title style-select">
                    <a type="button" data-toggle="collapse" data-target="#selectCity" class="collapsed">
                    <i class="icon"><img src="{{ asset('front/images/icon-state.png')}}" alt="" /></i> <span id="selCity">Select City</span> </a>
            </div>


            <div id="selectCity" class="style-select-option collapse ride "> 
            </div>
      </div>
    </div>


    



    <div class="form-group col-sm-6">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-zipcode.png')}}" alt="" /></i></span>
            <input id="Zipcode" type="text" name="zip_code" value="{{ old('zip_code') }}" class="form-control"  placeholder="Zipcode">
      </div>
    </div>


    <div class="form-group col-sm-6">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-contactperson.png')}}" alt="" /></i></span>
            <input id="OfficePhone" type="tel" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Office Phone" required="required" >
      </div>
    </div>

    <div class="form-group col-sm-6">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-contactperson.png')}}" alt="" /></i></span>
            <input id="EmergencyPhone" type="tel" name="emergency_phone" value="{{ old('emergency_phone') }}" class="form-control"  placeholder="Emergency Phone">
      </div>
    </div>


    <div class="form-group col-sm-6">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-fax.png')}}" alt="" /></i></span>
            <input id="Fax" type="text" class="form-control" name="fax" value="{{ old('fax') }}" placeholder="Fax">
      </div>
    </div>

    <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-website.png')}}" alt="" /></i></span>
                <input id="WebsiteURL" type="url" name="website_url" value="{{ old('website_url') }}" class="form-control"  placeholder="Website URL">
          </div>
    </div>

    <div class="form-group col-sm-12 icon-valign--top">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-paper.png')}}" alt=""></i></span>
                <textarea id="DescriptionofBusiness"  class="form-control minh100" name="description" placeholder="Description of Business!">{{ old('description') }}</textarea>
          </div>
    </div>
    <div class="form-group col-sm-12">

    <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-photo.png')}}" alt="" /></i> Add Logo</span>

    <div class="file-drop-area fnc-uplaod">
          <span class="btn  btn-info">Choose Files</span>
          <input class="file-input" type="file" name="logo" id="logo" required="required" accept="image/*"/>
          <label for="Picture1">No file Chosen</label>
    </div>

    </div>
    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-license.png')}}" alt="" /></i></span>
            <input id="License1" type="text" class="form-control" name="license_name" value="{{ old('license_name') }}" placeholder="License # 1">
      </div>

                    <div class="file-drop-area fnc-uplaod">
                              <span class="btn  btn-info">Choose Files</span>
                              <input class="file-input" type="file" name="license" value="{{ old('license') }}">
                              <label for="Picture1">No file Chosen</label>
                      </div>
    </div>


    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-paper3.png')}}" alt="" /></i></span>
            <input id="License1" type="text" class="form-control" name="certificate_name" value="{{ old('certificate_name') }}"  placeholder="Certificate of Insurance # 2">
      </div>

                    <div class="file-drop-area fnc-uplaod">
                              <span class="btn  btn-info">Choose Files</span>
                              <input class="file-input" type="file" name="certificate" value="{{ old('certificate') }}">
                              <label for="Picture1">No file Chosen</label>
                      </div>

    </div>


    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-paper3.png')}}" alt="" /></i></span>
            <input id="License1" type="text" class="form-control" name="reference_name" value="{{ old('reference_name') }}" placeholder="References PDF # 3">
      </div>

                    <div class="file-drop-area fnc-uplaod">
                              <span class="btn  btn-info">Choose Files</span>
                              <input class="file-input" type="file" name="reference" value="{{ old('reference') }}">
                              <label for="Picture1">No file Chosen</label>
                      </div>

    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-paper3.png')}}" alt="" /></i></span>
            <input id="License1" type="text" class="form-control" name="document_name" value="{{ old('document_name') }}" placeholder="Other documents # 4">
      </div>


                    <div class="file-drop-area fnc-uplaod">
                              <span class="btn  btn-info">Choose Files</span>
                              <input class="file-input" type="file" name="document" value="{{ old('document') }}">
                              <label for="Picture1">No file Chosen</label>
                      </div>

    </div>

    <div class="hed"><h2>Contact 1</h2></div>
			
    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-username.png')}}" alt="" /></i></span>
            <input id="Username" type="text" class="form-control" name="username[]" value="{{ old('username') }}" placeholder="Name">
      </div>
    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-contactperson.png')}}" alt="" /></i></span>
            <input id="Phone1" type="text" class="form-control" name="contact_phone[]" value="{{ old('contact_phone') }}" placeholder="Phone" />
      </div>
    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-mobile.png')}}" alt="" /></i></span>
            <input id="Phone1" type="text" class="form-control" name="cell_phone[]" value="{{ old('cell_phone') }}" placeholder="Cell Phone" />
      </div>
    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-email.png')}}" alt="" /></i></span>
            <input id="EmailAddress" type="email" class="form-control" name="email[]" value="{{ old('email') }}" placeholder="Email Address"  />
      </div>
    </div>
    <div class="hed"><h2>Contact 2</h2></div>
			
    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-username.png')}}" alt="" /></i></span>
            <input id="Username" type="text" class="form-control" name="username[]" value="{{ old('username') }}" placeholder="Name">
      </div>
    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-contactperson.png')}}" alt="" /></i></span>
            <input id="Phone1" type="text" class="form-control" name="contact_phone[]" value="{{ old('contact_phone') }}" placeholder="Phone" />
      </div>
    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-mobile.png')}}" alt="" /></i></span>
            <input id="Phone1" type="text" class="form-control" name="cell_phone[]" value="{{ old('cell_phone') }}" placeholder="Cell Phone" />
      </div>
    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-email.png')}}" alt="" /></i></span>
            <input id="EmailAddress" type="email" class="form-control" name="email[]" value="{{ old('email') }}" placeholder="Email Address"  />
      </div>
    </div>

      
    <div class="hed"><h2>Contact 3</h2></div>
			
    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-username.png')}}" alt="" /></i></span>
            <input id="Username" type="text" class="form-control" name="username[]" value="{{ old('username') }}" placeholder="Name">
      </div>
    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-contactperson.png')}}" alt="" /></i></span>
            <input id="Phone1" type="text" class="form-control" name="contact_phone[]" value="{{ old('contact_phone') }}" placeholder="Phone" />
      </div>
    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-mobile.png')}}" alt="" /></i></span>
            <input id="Phone1" type="text" class="form-control" name="cell_phone[]" value="{{ old('cell_phone') }}" placeholder="Cell Phone" />
      </div>
    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-email.png')}}" alt="" /></i></span>
            <input id="EmailAddress" type="email" class="form-control" name="email[]" value="{{ old('email') }}" placeholder="Email Address"  />
      </div>
    </div>

     
    
    <div class="hed"><h2>Contact 4</h2></div>
			
    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-username.png')}}" alt="" /></i></span>
            <input id="Username" type="text" class="form-control" name="username[]" value="{{ old('username') }}" placeholder="Name">
      </div>
    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-contactperson.png')}}" alt="" /></i></span>
            <input id="Phone1" type="text" class="form-control" name="contact_phone[]" value="{{ old('contact_phone') }}" placeholder="Phone" />
      </div>
    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-mobile.png')}}" alt="" /></i></span>
            <input id="Phone1" type="text" class="form-control" name="cell_phone[]" value="{{ old('cell_phone') }}" placeholder="Cell Phone" />
      </div>
    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-email.png')}}" alt="" /></i></span>
            <input id="EmailAddress" type="email" class="form-control" name="email[]" value="{{ old('email') }}" placeholder="Email Address"  />
      </div>
    </div>

    
  <div class="current-package-box col-sm-4 pul-cntr text-center">
      
        <div class="hed"><h2>Current Package</h2></div>
        <div class="package__inr0 ">
                <div class="package__img">
                        <img src="{{ asset('/'.$cart_package->package->image)}}" alt="package" />
                </div>
                <div class="package__cont">
                        <h3><i class="icon-price color-invert"></i> {{ $cart_package->grand_total }}</h3>
                        <h4>{{ $cart_package->package->package_name }}</h4>

                        <div class="actions">

                                <a href="{{ url('vendor/packages') }}" class="btn btn-primary">Upgrade Package</a>

                                <div class="clearfix mt20"></div>

                                <button type="submit" class="btn btn-primary">Save & Continue</button>

                                <button class="btn btn-info">Save & Exit</button>

                        </div>
                </div>
        </div>
</div>


{!! Form::close() !!}	


</div>
</section>
<script>

    function getCities(code)
    {
    var url = 'state_code=' + code;
    $.ajax({
    type: "GET",
            url: "{{ url('get/cities') }}",
            data: url,
            success: function(data){
            $('#selectCity').html(data);
            },
            error: function(errormessage) {
            //you would not show the real error to the user - this is just to see if everything is working
            alert(errormessage);
            }
    });
    }</script>
@endsection