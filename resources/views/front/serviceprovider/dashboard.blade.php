@extends('front')

@section('content')
<section class="dashboard-page pb30">
    <div class="container">

            <div class="row">

            <div class="dash__left col-sm-3 mt100">

                   @include('front/serviceprovider/common/navigation') 

            </div>

            <div class="dash__right col-sm-9">

                    <div class="hed"><h2>Dashboard</h2></div>

            </div>

            </div>

    </div>
</section>
	
@endsection