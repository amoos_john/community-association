<?php
    $required = "required";   
?>
<div class="gall-box  col-sm-8">
    <div class="gall__inr">
    <div class="gall__img" >
            <img id="imagePreview" src="{{ (isset($model))?asset('/'.$model->image):asset('front/images/not_available.jpg')}}" alt="gallery" />
    </div>
<div class="form-group col-sm-12">
<div class="file-drop-area fnc-uplaod">
        <span class="btn  btn-info">Add Photo*</span>
        <input class="file-input" type="file" name="image" id="image"  accept="image/*" <?php echo (isset($model))?'':$required;?>/>
      <label for="Picture1">No file Chosen</label>
</div>

</div>
<div class="form-group icon-valign--top">
    <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-title.png')}}" alt="" /></i></span>
        <input type="text" name="title" id="title"  class="form-control" placeholder="Title*" required="required" value="{{ old('title') }}" <?php echo $required;?>/>
    </div>
</div>
<div class="form-group icon-valign--top">
  <div class="input-group">
    <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-paper.png')}}" alt="" /></i></span>
    <textarea id="description" type="text" class="form-control minh150" name="description" placeholder="Description">{{ (isset($model))?$model->description:old('description') }}</textarea> 
    </div>
</div>
    </div>       
</div>

