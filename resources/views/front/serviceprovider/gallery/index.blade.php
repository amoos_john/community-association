@extends('front')

@section('content')
<?php
    use App\Functions\Functions;
?>
<section class="dashboard-page accordion-arr fom-bottomline--focus pb50">
<div class="container">

        <div class="row">

            <div class="dash__left col-sm-3 pt100">

                   @include('front/serviceprovider/common/navigation')  

            </div>
           <div class="dash__right vendor-gallery col-sm-9">
          <div class="hed"><h2>{{ $page_title }}</h2></div>
                @include('front/common/errors')
        <div class="table-responsive table-check-status fnc-fom clrlist">
   @foreach($packages as $package)         
    <div class="gall-box text-center col-sm-6">
        <div class="gall__inr">
    <div class="gall__img">
            <img src="{{ asset('/'.$package->image) }}" alt="gallery" />
    </div>
    @if(count($provider)==0)
         <div class="gall__edit__upgrade gall__upgrade">
            <a href="{{ url('vendor/billing/gallery/'.$package->id) }}"><i class="fa fa-dollar"></i>Pay Now</a>
        </div>
    @else
        @if($provider->gallerypackage_id==$package->id)
        <div class="gall__edit__upgrade gall__edit">
                <a href="{{ url('vendor/gallery/create') }}"><i class="fa fa-pencil"></i>Edit Profile</a>
        </div>
        @else
            <div class="gall__edit__upgrade gall__upgrade">
                 <a href="{{ url('vendor/billing/gallery/'.$package->id) }}"><i class="fa fa-check-square-o"></i>Upgrade</a>
            </div>
        @endif 
    @endif       
    <div class="gall__cont">
            <p><a href="#"><i class="fa fa-floppy-o"></i>{{ $package->package_name }}</a></p>
            <p class="gall__price">(Additional <span>{{ $symbol.Functions::moneyFormat($package->price) }}</span> {{ $package->duration }})</p>
    </div>
        </div>
    </div>
   @endforeach
   
<!--div class="gall-box text-center col-sm-6">
        <div class="gall__inr">
                <div class="gall__img">
                        <img src="{{ asset('front/images/gallery2.jpg') }}" alt="gallery" />
                </div>
                <div class="gall__edit__upgrade gall__upgrade">
                        <a href="#"><i class="fa fa-check-square-o"></i>Upgrade</a>
                </div>
                <div class="gall__cont">
                        <p>Lorem Ipsum is simply dummy text printing
                         and typesetting industry. Lorem  has been 
                        the industry's standard.</p>
                        <p><a href="#"><i class="fa fa-floppy-o"></i>2 Photos Logo or Recent Work</a></p>
                        <p class="gall__price">(Additional <span>$50</span> per year)</p>
                </div>
        </div>
</div>
<div class="gall-box text-center col-sm-6">
        <div class="gall__inr">
                <div class="gall__img">
                        <img src="{{ asset('front/images/gallery3.jpg') }}" alt="gallery" />
                </div>
                <div class="gall__edit__upgrade gall__upgrade">
                        <a href="#"><i class="fa fa-check-square-o"></i>Upgrade</a>
                </div>
                <div class="gall__cont">
                        <p>Lorem Ipsum is simply dummy text printing
                         and typesetting industry. Lorem  has been 
                        the industry's standard.</p>
                        <p><a href="#"><i class="fa fa-floppy-o"></i>2 Photos Logo or Recent Work</a></p>
                        <p class="gall__price">(Additional <span>$75</span> per year)</p>
                </div>
        </div>
</div>
<div class="gall-box text-center col-sm-6">
        <div class="gall__inr">
                <div class="gall__img">
                        <img src="{{ asset('front/images/gallery1.jpg') }}" alt="gallery" />
                </div>
                <div class="gall__edit__upgrade gall__upgrade">
                        <a href="#"><i class="fa fa-check-square-o"></i>Upgrade</a>
                </div>
                <div class="gall__cont">
                        <p>Lorem Ipsum is simply dummy text printing
                         and typesetting industry. Lorem  has been 
                        the industry's standard.</p>
                        <p><a href="#"><i class="fa fa-floppy-o"></i>2 Photos Logo or Recent Work</a></p>
                        <p class="gall__price">(Additional <span>$100</span> per year)</p>
                </div>
        </div>
</div>-->


</div>

</div>

</div>
</div>
</section>
@endsection