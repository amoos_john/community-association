@extends('front')

@section('content')
<style>
    .file-drop-area.fnc-uplaod {
    float: left;
}
img.img-responsive.img-thumbnail {
    width: 230px;
    height: 180px;
}
.gall__img  {
border: 1px solid #b7b7b7;

}
.gall__img img {
    opacity: 1;
    object-fit: inherit;
    width: 100%;
}
.gall__inr {
    border: none;
}
</style>
<section class="dashboard-page pb30">
    <div class="container">

        <div class="row">

            <div class="dash__left col-sm-3 mt100">

                   @include('front/serviceprovider/common/navigation') 

            </div>
            <div class="dash__right col-sm-9">

            <div class="hed"><h2>{{ $page_title }}</h2></div>



           <div class="table-responsive table-check-status fnc-fom clrlist">

            @include('front/common/errors')       
            {!! Form::open(array( 'class' => 'form','url' => 'vendor/gallery/insert', 'files' => true)) !!}

            <?php $page = "front.serviceprovider.gallery.form"; ?>
            @include($page)
            <div class="vendor-total bg-cvr col-sm-4" style="background-image:url('{{ asset('front/images/steel-bg.png')}}');">
                    <h3>Remaining Photos</h3></td>
                    <h2><?php echo $photos;?></h2>
                    <div class="arrow-arc"></div>
            </div>
            <div class="col-sm-8 text-right">
                <button type="submit" class="btn btn-primary ">Save & Exit</button>
                
            </div>
            
            

            {!! Form::close() !!}
             </div>


            </div>

            </div>

    </div>
</section>
<script>


function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#imagePreview').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#image").change(function(){
        readURL(this);
    });
</script>
@endsection