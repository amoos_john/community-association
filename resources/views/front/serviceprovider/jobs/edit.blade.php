@extends('front')

@section('content')

<section class="dashboard-page pb30">
    <div class="container">

        <div class="row">

            <div class="dash__left col-sm-3 mt100">

                   @include('front/association/common/navigation') 

            </div>
            <div class="dash__right col-sm-9">

            <div class="hed"><h2>{{ $page_title }}</h2></div>



           <div class="fom fnc-fom accordion-arr fom-bottomline--focus">

            @include('front/common/errors')       
            {!! Form::model($model, ['files' => true,'class' => 'form','url' => ['association/jobs/update', $model->id], 'method' => 'post']) !!}

            <?php $page = "front.association.jobs.form"; ?>
            @include($page)
            <div class="col-sm-12 text-right">
            <button type="submit" class="btn btn-primary ">Update</button>
            <a href="{{ url('association/jobs') }}" class="btn btn-info">Cancel</a>
            </div>

            {!! Form::close() !!}
              </div>


            </div>

            </div>

    </div>
      
</section>
@endsection