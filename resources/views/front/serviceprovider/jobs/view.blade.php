<div class="job__cont clrlist col-sm-12">
    @if(count($model)>0)
    <h3>Job Title</h3>
    <h4>{{ $model->title }}</h4>
    <p class="job__location">Hiring in {{ $model->location }}</p>
    <p>Email: {{ $model->email }}</p>
    <p>Phone: {{ $model->phone }}</p>
    <h3>Description</h3>
    <p class="m0"><?php echo $model->job_description; ?></p>
    @else
    <h3>Job not Found</h3>
    @endif
    @if(count($job_bid)==0)
<div id="message"></div>
<form method="post" action="" name="frm_reply" id="frm_reply" enctype="multipart/form-data"> 
    <input type="hidden" name="id" value="<?php echo $id ?>" /> 
    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>" />    
    <div class="job__attch">
        <input type="file" name="file" id="file"/>
        <button type="button"><img src="{{ asset('front/images/icon-attachment.png') }}" alt="attachment icon" />Attach Resume or CV*</button>
        <span role="alert" id="nameError" class="red"></span>
    </div>
    <div class="form-group col-sm-12"> 
    <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-email.png') }}" alt="" /></i></span>
            <input id="email" type="email" class="form-control" name="email" placeholder="Email Address*" required>
    </div>
   <span role="alert" id="emailError" class="red"></span>
</div>


<div class="form-group textarea-box col-sm-12">
    <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-email.png') }}" alt="" /></i></span>
            <textarea id="msg"  class="form-control" name="message" rows="5" placeholder="Message*" required></textarea>

    </div>
   <span role="alert" id="msgError" class="red"></span>

</div>

<div class="btns jobs__submit text-right col-sm-12">
    <button type="button" id="reply" class="btn btn-primary">Submit</button>
</div>
</form>
@else
 <h3>Your Response</h3>
 <h4>Email Address</h4>
 <p class="m0">{{ $job_bid->email }}</p>
 <h4>Message</h4>
 <p class="m0"><?php echo $job_bid->message; ?></p>
 
    <div class="job__attch">
        <a href="{{ url('/'.$job_bid->document) }}" download><img src="{{ asset('front/images/icon-attachment.png') }}" alt="attachment icon" />Download file</a>
    </div>
@endif
</div>

