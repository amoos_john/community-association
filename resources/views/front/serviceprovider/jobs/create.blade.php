@extends('front')

@section('content')

<section class="dashboard-page pb30">
    <div class="container">

        <div class="row">

            <div class="dash__left col-sm-3 mt100">

                   @include('front/association/common/navigation') 

            </div>
            <div class="dash__right col-sm-9">

            <div class="hed"><h2>{{ $page_title }}</h2></div>



           <div class="fom fnc-fom accordion-arr fom-bottomline--focus">

            @include('front/common/errors')       
            {!! Form::open(array( 'class' => 'form','url' => 'association/jobs/insert', 'files' => true)) !!}

            <?php $page = "front.association.jobs.form"; ?>
            @include($page)
            <div class="col-sm-12 text-right">
                <button type="submit" class="btn btn-primary ">Save & Continue</button>
                <button class="btn btn-info">Save & Exit</button>
            </div>

            {!! Form::close() !!}
              </div>


            </div>

            </div>

    </div>
</section>
<script>
	$(document).ready(function(){
                //var array = ["2017-07-14","2017-07-15","2017-07-16"]

                
		var start_date=$('input[name="submission_date"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		start_date.datepicker({
                        
			format: 'yyyy-mm-dd',
                        startDate: new Date(),
			container: container,
			todayHighlight: true,
			autoclose: true,
			active:true
                      

		})
           $("#submission_date").keydown(function(event) { 
                return false;
            });     

	})
</script>
@endsection