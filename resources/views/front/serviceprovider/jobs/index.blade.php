@extends('front')

@section('content')
<?php
    use App\Functions\Functions;
    use App\JobBids;
?>
<section class="dashboard-page pb30">
    <div class="container">
                   

        <div class="row">

            <div class="dash__left col-sm-3 mt100">

                   @include('front/serviceprovider/common/navigation')

            </div>
            <div class="dash__rigt col-sm-9">
          <div class="hed"><h2>{{ $page_title }}</h2></div>
                @include('front/common/errors')
	<div class="table-responsive open-job-bids-table fnc-fom">
				
	<table class="table table-small0 clrlist">
        <thead>
        <tr>
                <th>Title</th>
                <th>Association Submitted</th>
                <th>Date Submitted</th>
                <th>Status</th>
        </tr>
        </thead>
        <tbody>
        @if(count($model)>0)
            @foreach($model as $row)
            <?php
                $bid = JobBids::where("user_id","=",$user_id)
                            ->where("job_id","=",$row->id)
                            ->where("deleted","!=",1)
                            ->where("status","=",1)->first();
                $status = 0;
                $submitted = '';
                if(count($bid)>0)
                {
                    $status = $bid->status;
                    $submitted = Functions::dateFrontFormat($bid->created_at);
                }
                if (array_key_exists($status, $statuses)) {
                    $status = $statuses[$status];
                }
            ?>
                <tr>
                <td>{{ $row->title }}</td>
                <td>{{ $row->association->name }}</td>
                <td>{{ $submitted }}</td>
                <td>
                    <div class="status-actions">
                    <i class="fa fa-floppy-o"></i>{{ $status }}
                    <div class="status-actions-box">
                            <ul>
                                    <li><a data-target="#openJob" onClick="return open_job('{{ $row->id }}');" data-toggle="modal">View</a></li>
                                    <!--<li><a data-toggle="modal" data-target="#openJob">Edit</a></li>-->
                                    @if(count($bid)>0)
                                    <li><a data-href="{{ url("vendor/jobs/delete/".$bid ->id) }}" data-target="#confirm-delete" data-toggle="modal" title="Void">
                                     Void</a></li>
                                     @endif
                            </ul>
                    </div>
                </div>
                </td>
                </tr>
            @endforeach  
        @else
            <tr>
                <td colspan="5">Bid's not found!</td>

            </tr>

        @endif
        

        </tbody>

        </table>
            <div class="text-right"><?php echo $model->render(); ?></div>             
               

        </div>
			
			
			
			</div>

            </div>

    </div>
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title text-center" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are you sure to delete this job?</p>
                </div>
                
                <div class="modal-footer">
                    <a  class="btn btn-info" data-dismiss="modal">Cancel</a>
                    <a class="btn btn-primary btn-ok" id="btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>  
 <div class="modal fade job-desc-modal" id="openJob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close " data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4></h4>
      </div>
      <div class="modal-body overload fnc-fom accordion-arr" id="details-modal">
        			
      </div>
    </div>
  </div>
</div>  

            
            
	</section>

<script>
       //$("a#rfp-sub-modal").click(function()  {
       
       
       function open_job(id)
       {
           $('#details-modal').html("");
            //var id = $(this).attr('id');
            $.ajax({
                type: 'GET',
                url: '<?php echo  url("vendor/jobs/view"); ?>/'+id,
                success: function(data) 
                {
                    $('#details-modal').html(data);
                }
            });
      }
   

</script> 
<script src="{{ asset('front/js/jquery.form.js')}}"></script>
<script>  
function refreshParent() {

  window.location = '{{ url("vendor/jobs") }}';
}

$("body").on("keyup","#email",function(){
     $('#emailError').html('');

});
$("body").on("keyup","#msg",function(){

     $('#msgError').html('');

});

var errorMsg = 'This field is required.';
$("body").on("click","#reply",function(){
if ($("#file").val().length < '1') {
    jQuery('#nameError').html(errorMsg).show();
}
if ($("#email").val().length < '1') {
    jQuery('#emailError').html(errorMsg).show();
}
if ($("#msg").val().length < '1') {
    jQuery('#msgError').html(errorMsg).show();
}
else
{
$('#frm_reply').ajaxSubmit({

     type: "POST",
     url: "{{ url('vendor/jobs/reply') }}",
     data: $("#frm_reply").serialize,
     cache: false,
     success: function(data){
        if(data=='1')
        {
            $('#frm_reply')[0].reset();
            var successHtml = '<div class="alert alert-success alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a>';
            successHtml += "Thank you, your request has been submitted."; //showing only the first error.

            successHtml += '</div>';
            $('#message').html(successHtml).show();
            setTimeout(function(){ refreshParent(); }, 1000)
            
        }
        else
        {
            var errorsHtml = '<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a><ul>';

            errorsHtml += data; //showing only the first error.

            errorsHtml += '</ul></div>';
            $('#message').html(errorsHtml).show();
             
        }
       
},
     error: function(errormessage) {
           //you would not show the real error to the user - this is just to see if everything is working
            //$('#message').html(errormessage).show();
               
     }
   
});  
}

});
</script>    
@endsection