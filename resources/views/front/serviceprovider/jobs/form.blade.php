<?php
    $required = "required";
?>
<div class="form-group col-sm-12">
  <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-worker.png')}}" alt="" /></i></span>
        <input id="title" type="text" class="form-control" name="title" value="{{ (isset($model))?$model->title:old('title') }}" placeholder="Job Title*" <?php echo $required;?>/>
  </div>
</div>

<div class="form-group col-sm-12">
  <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-map.png')}}" alt="" /></i></span>
        <input id="location" type="text" class="form-control" name="location" value="{{ (isset($model))?$model->location:old('location') }}" placeholder="Location Description*" <?php echo $required;?>/>
  </div>
</div>

<div class="form-group col-sm-12 icon-valign--top">
  <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-paper.png')}}" alt="" /></i></span>
        <textarea id="job_description" type="text" class="form-control minh150" name="job_description" placeholder="Job Description">
        {{ (isset($model))?$model->job_description:old('job_description') }}    
        </textarea>
  </div>
</div>

<div class="form-group col-sm-12">
  <div class="input-group">
        <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-people.png')}}" alt="" /></i></span>
        <input id="apply_job" type="text" class="form-control" name="apply_job" value="{{ (isset($model))?$model->apply_job:old('apply_job') }}" placeholder="How do people apply for this job?">
  </div>
</div>
<div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-username.png')}}" alt="" /></i></span>
            <input type="text" name="name" id="name"  class="form-control"  placeholder="Name*"  value="{{ (isset($model))?$model->name:old('name') }}" <?php echo $required;?>/>
      </div>
</div>

 <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-email.png')}}" alt="" /></i></span>
            <input id="EmailAddress" type="email" class="form-control" name="email" value="{{ (isset($model))?$model->email:old('email') }}" placeholder="Email Address*" <?php echo $required;?> />
      </div>
</div>
<div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-mobile.png')}}" alt="" /></i></span>
            <input id="Phone" type="tel" class="form-control" name="phone" value="{{ (isset($model))?$model->phone:old('phone') }}" placeholder="Phone*" <?php echo $required;?>/>
      </div>
</div>
