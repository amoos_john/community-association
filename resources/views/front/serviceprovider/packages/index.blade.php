@extends('front')

@section('content')
<?php
    use App\Functions\Functions;
?>
<section class="dashboard-page pb30">
    <div class="container">
                   

    <div class="row">

        <div class="dash__left col-sm-3 mt100">

             @include('front/serviceprovider/common/navigation') 

        </div>
            <div class="dash__rigt col-sm-9">
          <div class="hed"><h2>{{ $page_title }} ({{ $package_name }})<span>State of Alaska</span></h2>  </div>
 @if(count($model->packages)>0)
 <div class="package-box vendor-package-box col-sm-4 anime-zoomIn col-sm-offset-4">
    <div class="package__inr">
            <div class="package__img">
                    <img src="{{ asset('/'.$model->packages->image)}}" alt="package" />
            </div>
            <div class="package__cont">
                    <h3><i class="icon-price"></i> {{ Functions::moneyFormat($model->packages->price) }}</h3>
                    <h4>{{ $package_name }}</h4>
                    <h5 class="expiry-date">Exp date: {{ $exp_date }}</h5>
                    <h6>Current</h6>
            </div>
    </div>
</div>
@endif

    <div class="clearfix"></div>
    
    <div class="hed h1 "><h2>Upgrade Package</h2></div>
 <?php $i = 0; ?>   
@foreach($packages as $package)		
<div class="package-box vendor-package-box col-sm-4 <?php echo  ($i==0)?'col-sm-offset-2':''; ?> anime-zoomIn ">
<div class="package__inr">
    <div class="package__img">
        <img src="{{ asset('/'.$package->image)}}" alt="package" />
    </div>
    <div class="package__cont">
        <h3><i class="icon-price"></i> {{ Functions::moneyFormat($package->price) }}</h3>
        <h4>{{ $package->package_name }}</h4>
        <h5>{{ $package->description }}</h5>
        <div class="lnk-btn">
                <a href="{{ url('vendor/package/'.$package->id) }}" class="btn btn-default">Upgrade Now</a>
        </div>
    </div>
</div>
</div>
 <?php $i++; ?>   
@endforeach			
	<div class="clearfix"></div>		
	</div>

            </div>

    </div>
</section>

@endsection