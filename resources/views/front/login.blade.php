@extends('front')

@section('content')

<section class="login-area clrlist">
	<div class="container">
		<div class="hed">
			<h2>Login</h2>
		</div>

		<div class="login-fom fom-bottomline--focus fnc-fom col-sm-6 col-sm-offset-3 mb50">
                @include('front/common/errors') 
                {!! Form::open(array( 'class' => 'form','url' => 'postLogin', 'files' => true)) !!}

                  <div class="input-group col-sm-12">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                        <input id="email" type="email" class="form-control" value="{{ old("email") }}" name="email" placeholder="Email Address" required />
                  </div>
                  <div class="input-group col-sm-12">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="password" type="password" class="form-control" placeholder="Password" name="password" required />
                  </div>
                  <div class="check-area pul-lft">
                        <input type="checkbox" name="remember" id="check1"><mark></mark> <label for="remember">Remember Me</label>
                  </div>
                  <div class="login__lost pul-rgt">
                        <a href="{{ url('forgot') }}"><i class="fa fa-support"></i> Lost your Password?</a>
                  </div>
                  <div class="clearfix"></div>
                  <div class="login__submit mt20">
                        <button type="submit" class="btn btn-primary w100">Login</button>
                  </div>
		{!! Form::close() !!}	
		</div>
	</div>
</section>
@endsection