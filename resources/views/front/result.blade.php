@if($model->count()>0)
@foreach($model as $row)
<div class="progal-box col-sm-4 anime-left delay1s">
    <div class="progal__inr">
        <div class="progal__img "><img src="{{ asset('/'.$row->image) }}" alt="{{ $row->title }}"></div>
        <div class="progal__cont">
            <h3>{{ $row->name }}</h3>
            <div class="cont">{{ $row->description }}</div>
            <div class="lnk-btn view-btn"><a data-target="#viewProfile" onClick="return viewProfile('{{ $row->serviceprovider_id }}');" data-toggle="modal"">View Profile</a></div>
        </div>
    </div>
</div>
@endforeach
@else
<h3>Result not found!</h3>
@endif
<div class="clearfix"></div>
<div class="text-center"><?php echo $model->render();?></div>

