<script type="text/javascript">
        
        function removeParam(key, sourceURL,value) {
         var rtn = sourceURL.split("?")[0],
             param,
             pvalue,
             params_arr = [],
             queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
         if (queryString !== "") {
             params_arr = queryString.split("&");
             for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                 param = params_arr[i].split("=")[0];
                 pvalue = params_arr[i].split("=")[1];
                 //console.log(pvalue);
                 if (param === key) {
                     params_arr.splice(i, 1);
                 }
             }
             
             rtn = rtn + "?" + params_arr.join("&");
         }
         return rtn;
     }
    function getCounties(code)
    {
      var url = 'state_code='+code;
       $.ajax({ 
         type: "GET",
         url: "{{ url('vendor/get-counties') }}",
         data: url,
         success: function(data){
            $('#selectState').html(data);

        },
         error: function(errormessage) {
               //you would not show the real error to the user - this is just to see if everything is working
               alert(errormessage);    

         }
        }); 
    }
    getCart('<?php echo $package_id; ?>');
    function getCart(id)
    {
       $.ajax({ 
         type: "GET",
         url: "{{ url('vendor/cart') }}/"+id,
         success: function(data){

                $('#cart').html(data);

        },
         error: function(errormessage) {
               //you would not show the real error to the user - this is just to see if everything is working
               alert(errormessage);    

         }
        }); 
    }
$("body").on("click","#create",function(){
      
     var id='&package_id='+<?php echo $package_id; ?>; 
     var token = "&_token=<?php echo csrf_token() ?>"; 
      
     $.ajax({ 
     type: "POST",
     url: "{{ url('vendor/create/package') }}",
     data: $("#frm_cart").serialize()+token+id,
     success: function(data){
        if(data=='1')
        {
            $('#frm_cart')[0].reset();
            $('#selectState').html("");
            getCart('<?php echo $package_id; ?>');
        }
        else
        {
            var errorsHtml = '<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a><ul>';

            errorsHtml += data; //showing only the first error.

            errorsHtml += '</ul></div>';
            $('#message').html(errorsHtml).show();
            getCart('<?php echo $package_id; ?>');
            
        }
       
},
     error: function(errormessage) {
           //you would not show the real error to the user - this is just to see if everything is working
           alert(errormessage);    
               
     }
});
      
     
});   
</script>