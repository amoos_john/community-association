    <section class="bottom-area bg-gray white text-center clrlist p10">
        <div class="container">
            <span>&copy <?php date('Y');?> New Century Labs All rights reserved.
</span>
            <div class="bottom__links clrlist listdvr">
                <ul>
                    <li><a href="{{url('privacy')}}">Privacy</a></li>
                    <li><a href="{{url('terms')}}">Terms</a></li>
                </ul>
            </div>
        </div>
    </section>