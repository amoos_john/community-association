<!-- Modal -->
<!--<link rel="stylesheet" href="{{ asset('front/css/demo.css')}}">-->
<style>
.card
{
    display:none;
}
</style>
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close " data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4> </h4>
      </div>
      <div class="modal-body overload fnc-fom accordion-arr">
        <div class="card"></div>      
   
    <div class="form-inline col-sm-12 fnc-fom--no">
        <div class="title"><h5 class="red">Required fields*</h5></div>
        <div class="title"><h5>Select Payment Method*</h5></div>
        <div class="clearfix"></div>
        <div class="form-group"><input type="radio" id="payment_type" name="payment_type" value="1" required="" id="Credit Card"> <label for="Credit Card">Credit Card</label></div>
        <div class="form-group"><input type="radio" id="payment_type" name="payment_type" value="2" id="paypal"> <label for="paypal"><img src="{{ asset('front/images/payment-paypal.png')}}" alt=""/></label></div>
        <div class="form-group"><input type="radio" id="payment_type" name="payment_type" value="3" id="echeck"> <label for="echeck"><img src="{{ asset('front/images/payment-echeck.png')}}" alt=""/></label></div>
    </div>


    <div class="form-group col-sm-12">  
        <input id="Name" type="text" class="form-control" name="card_name" id="card_name" value="{{ old('card_name') }}" placeholder="Name*" required=""/>
        
    </div>


    <div class="form-group col-sm-12">  
        <input type="text" class="form-control"  name="card_no" id="card_no" placeholder="Enter Your Credit Card Number*" value="{{ old('card_no') }}" required=""/>
        
    </div>

<div class="form-group col-sm-12">  
        <input id="cvc_no" type="text" class="form-control"  name="cvc_no" id="cvc_no" value="{{ old('cvc_no') }}" placeholder="Cvc Number*" required=""/>
</div>
		
		
<div class="title col-sm-12" ><h5>Expiration Date*</h5></div>
		
<div class="form-group col-sm-6">

  <div class="input-group">

<div class="panel-title style-select">
        <a type="button" data-toggle="collapse" data-target="#selectMonth" class="collapsed">
            <span id="selMonth">Month*</span></a>
</div>


<div id="selectMonth" class="style-select-option collapse ">
    @foreach($months as $key=>$mon)
    <?php $checked = ($key ==  old('month'))?'checked':''; ?>
    <div class="form-group">
        <input type="radio" value="{{ $key }}" id="{{ $mon }}" name="month" id="month" <?php echo $checked;?>/><mark></mark>
        <label for="{{ $mon }}">{{ $mon }}</label>
    </div>
     @endforeach

</div>
  </div>
</div>
			
			
      <div class="form-group col-sm-6">

      <div class="input-group">

        <div class="panel-title style-select">
                <a type="button" data-toggle="collapse" data-target="#selectYear" class="collapsed">
                    <span id="selYear">Year*</span> </a>
        </div>


        <div id="selectYear" class="style-select-option collapse ">
        @for($i=$year;$i<=2020;$i++)
        <?php $checked = ($i ==  old('year'))?'checked':''; ?>
            <div class="form-group">
                <input type="radio" value="{{ $i }}" id="{{ $i }}" name="year" id="year" <?php echo $checked;?>/><mark></mark>
                <label for="{{ $i }}">{{ $i }}</label>
            </div>
       @endfor 
        
        </div>
      </div>
    </div>
			
			
    <div class="btns col-sm-12">
        <button type="submit" id="btn-order" class="btn btn-primary w100" disabled="">Save and Submit</button>
    </div>		
			
      </div>
    </div>
  </div>
</div>
<script>
$('#selectMonth .form-group input').click(function(){
 
 
 $('#selMonth').text($(this).attr('id'));
 $('[data-target="#selectMonth"]').trigger( "click" );

});

$("body").on("click","#selectYear .form-group input",function(){

 $('[data-target="#selectYear"]').trigger( "click" );
 $('#selYear').text($(this).attr('id'));

});    

</script>

