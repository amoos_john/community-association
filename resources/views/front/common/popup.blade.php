<div class="job__cont clrlist col-sm-12">
    @if(count($model)>0)
    <div class="form-group col-sm-4 text-right pull-right" >
        <img class="img-responsive img-thumbnail" src="{{ asset('/'.$model->logo)}}" alt="{{ $model->logo }}" />
    </div>
    <h3>Vendor Name</h3>
    <p class="job__location">{{ $model->name }}</p>
    
    <h3>Description</h3>
    <p class="m0"><?php echo $model->description; ?></p>
    
    
   
    @else
    <h3>Vendor not Found</h3>
    @endif
</div>