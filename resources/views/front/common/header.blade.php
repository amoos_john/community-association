<header>
<section class="hdr-area hdr-nav hdr--sticky cross-toggle bg-white navbar-blinds--hover">
<div class="container">
<nav class="navbar navbar-default" role="navigation" id="slide-nav">
<div class="container-fluid">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
  </button>
        <a class="navbar-brand ani-shiny" href="{{ url('') }}"><img src="{{ url('front/images/logo.png')}}" alt="logo" /></a>
</div>

<!-- Collect the nav links, forms, and other content for toggling -->
<div id="slidemenu">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          @if(!isset(Auth::user()->id))
          <ul class="nav navbar-nav navbar-main">
                <li><a href="{{ url('') }}">HOME</a></li>
                <li><a href="#">ABOUT US</a></li>
                <li><a href="#">HOW IT WORKS</a></li>
                <li><a href="{{ url('gallery') }}">GALLERY</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">PRICING</a>
                  <ul class="dropdown-menu">
                        <li><a href="#">Association</a></li>
                        <li><a href="#">Service Provider</a></li>
                  </ul>
                </li>

          </ul>
          @endif
        <ul class="nav navbar-nav pul-rgt">

            <li class="<?php echo (isset(Auth::user()->id))?'dropdown user user-menu pul-rgt':'';?>">


              @if(isset(Auth::user()->id))
               <a href="#usr-name" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="usr-name" onClick="dashboard()">Dashboard</span>
                <!--<span class="dp"><img src="{{ url('front/images/icon-user.png') }}" alt="user" ></span>-->
                </a>
              <script>
              function dashboard()
              {     
                  <?php
                  $profile = '';
                  if(Auth::user()->role_id==2)
                  {
                      $profile = url("association/profile/edit");
                  ?>
                  window.location = '{{ url("association/dashboard") }}';
                  <?php
                  }
                  elseif(Auth::user()->role_id==3)
                  {
                  $profile = url("vendor/profile/edit");
                  ?>
                  window.location = '{{ url("vendor/dashboard") }}';
                 <?php } 
                 elseif(Auth::user()->role_id==4)
                 {
                     
                 ?>
                 window.location = '{{ url("user/dashboard") }}';
                 <?php } ?>
              }
              </script>
              @else
              <span class="addon-label z100"><a  href="{{ url('login') }}">Already a Member?</a></span>
              <a href="{{ url('register') }}">
                <span class="usr-name">REGISTER</span></a>
              @endif


            @if(isset(Auth::user()->id))    
            <ul class="dropdown-menu p20">

              <li class="user-header">

                    <p>{{ Auth::user()->name }}<small>- {{ Auth::user()->role->role }}</small></p>
                    <small>Member since {{ date("d/m/Y",strtotime(Auth::user()->created_at)) }}</small>
              </li>
              <li class="user-footer">
                    <div class="pull-left">
                       @if(Auth::user()->role_id!=4)
                      <a href="{{ $profile  }}" class="  btn-flat0"><i class="fa fa-user"></i> Profile</a>
                      @endif    
                    </div>
                    <div class="pull-right ">
                      <a href="{{ url('auth/logout') }}" class=" btn-flat0  "><i class="fa fa-sign-out"></i> Sign out</a>
                    </div>
              </li>
            </ul>
            @endif
            </li>
    </ul>


        </div><!-- /.navbar-collapse -->
</div>
</div><!-- /.container-fluid -->
</nav>
</div>
</section>
</header>