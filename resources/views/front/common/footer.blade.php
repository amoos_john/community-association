<footer>
		<section class="ftr-area bg-cvr" style="background-image:url('{{ url('front/images/footer-bg.jpg')}}')">
			<div class="container">
				<div class="ftr__box col-sm-3 ftr__logo bdr-next">
					
					<div class="ftr-logo"><img src="{{ url('front/images/footer-logo.png')}}" class="drp-shad" alt="" /></div>
					
				</div>
				<div class="ftr__box ftr__cont col-sm-4">
					<h4>Service Provider Directory</h4>
					<div class="cont">Find and post jobs related to property preservation, property management and other field services.</div>
				</div>
				
				<div class="ftr__box ftr__cont col-sm-3">
					<h4>Service Provider Directory</h4>
					<div class="cont">Find property preservation vendors in your area. Add your company to receive job notifications.</div>
				</div>
				
				<div class="ftr__box ftr__nav col-sm-2 clrlist listview">
					<h4>Useful Links</h4>
					<ul>
						<li><a href="#">Terms of Service</a></li>
						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Contact Us</a></li>
						<li><a href="#">Forgot Password</a></li>
					</ul>
				</div>

				
			</div>
		</section>
		
		
		<section class="bottom-area bottom-logo--slide p20 bg-blue white">
			<div class="container">

				<div class="pul-lft">Copyright <?php echo date("Y"); ?> Market Place. All Rights Reserved.</div>
				
				<div class="pul-rgt">Designed & Developed by <a href="http://golpik.com" target="_blank">Golpik</a></div>
				
			</div>
		</section>
		<a href="" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
	</footer>