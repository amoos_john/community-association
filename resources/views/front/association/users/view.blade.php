<style>
    .job__sign {
    padding: 10px 0 0;
}
</style>
<div class="job__cont clrlist col-sm-12">
    @if(count($model)>0)
    <h3>User Details</h3>
    <div class="job__sign col-sm-6">
       <h4>First Name: {{ $model->first_name }}</h4>
    </div>

    <div class="job__sign col-sm-6">
        <h4>Last Name: {{ $model->last_name }}</h4>
    </div>
    
    <div class="job__sign col-sm-6">
        <h4>Username: {{ $model->username }}</h4>
    
    </div>

    <div class="job__sign col-sm-6">
         <h4>Email: {{ $model->email }}</h4>
    </div>
   
    <div class="job__date  col-sm-8">
        <h5>Roles</h5> 
        @if(isset($categories) || isset($positions) || isset($roles)) 
        <table class="table table-cart">
            <thead>
                    <tr>
                        <th>Name</th>
                        <th>Role Name</th>
                    </tr>
            </thead>
            <tbody>

            @if(isset($categories) && (count($categories)>0 && count($positions)>0))    
            <tr>
                <td>{{ $categories->name }}</td>
                <td>{{ $positions->name }}</td>
            </tr>
            @endif
            @if(isset($roles))    
            <tr>
                <td>Organization</td>
                <td>{{ $roles->name }}</td>
            </tr>
            @endif

            </tbody>
        </table>
        @else
            <p>This user currently has no roles assigned.</p>
        @endif
        
    </div>
    <div class="clearfix"></div>
    <div class="btns jobs__submit pull-left">
        <form role="form" method="POST" action="{{ url('forgot/password') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="email" value="{{ $model->email }}">
            <button type="submit" class="btn btn-primary">Reset Password</button>   
        </form>
    </div>
    <div class="btns jobs__submit pull-right">
        <a href="{{ url('association/users/edit/'.$id) }}" class="btn btn-primary">Edit User</a>
        <a href="{{ url('association/users/email/'.$id) }}" class="btn btn-primary">Send Email</a>
    </div>
    @else
    <h3>User not Found</h3>
    @endif
</div>