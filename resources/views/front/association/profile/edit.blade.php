@extends('front')

@section('content')

<section class="dashboard-page pb30">
    <div class="container">

        <div class="row">

            <div class="dash__left col-sm-3 mt100">

                   @include('front/association/common/navigation') 

            </div>
            <div class="dash__right col-sm-9">

            <div class="hed"><h2>{{ $page_title }}</h2></div>



           <div class="fom fnc-fom accordion-arr fom-bottomline--focus">

            @include('front/common/errors')       
            {!! Form::model($model, ['files' => true,'class' => 'form','url' => ['association/profile/update', $model->id], 'method' => 'post']) !!}

            <?php $page = "front.association.profile.form"; ?>
            @include($page)
            

            {!! Form::close() !!}
            </div>


            </div>

            </div>
        
<div class="map-area col-sm-12 mt50">
     <iframe src="<?php echo ($model->google_map!='')?$model->google_map:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11340.735412285894!2d-93.03872321070934!3d44.71585916558592!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87f7cbbf2a97a96f%3A0xa92be8970396117e!2sCoates%2C+MN+55068!5e0!3m2!1sen!2s!4v1499195669139' ;?>" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

    </div>
</section>
<script>
    getCities('<?php echo ($model->state!='')?$model->state:'';?>','<?php echo ($model->city!='')?$model->city:'';?>');
    function getCities(code,city)
    {
    var url = 'state_code=' + code +'&city=' + city;
    $.ajax({
    type: "GET",
            url: "{{ url('get/cities') }}",
            data: url,
            success: function(data){
            $('#selectCity').html(data);
            },
            error: function(errormessage) {
            //you would not show the real error to the user - this is just to see if everything is working
            //alert(errormessage);
            }
    });
    }</script>
@endsection