<?php
    $required = "required";
?>
<div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-groupuser.png')}}" alt="" /></i></span>
            <input type="text" name="name" value="{{ $model->name }}" id="name"  class="form-control"  placeholder="Association Name*" <?php echo $required;?>/>
      </div>
</div>
<div class="form-group col-sm-4">
    
    <div class="input-group">

    <div class="panel-title style-select">
        <a type="button" data-toggle="collapse" data-target="#selectState" class="collapsed">
            <i class="icon"><img src="{{ asset('front/images/icon-state.png')}}" alt="" /></i> <span id="selState">Select State</span> </a>
    </div>

    <div id="selectState" class="style-select-option collapse ride ">

        @foreach($states as $row)
         <?php
        $checked = '';
        if($model->state==$row->state_code)
        {
            $checked = 'checked';
        }  
        ?>

        <div class="form-group">
            <input type="radio" name="states" onChange="getCities('{{ $row->state_code }}')" value="{{ $row->state_code }}" id="{{ $row->state }}" <?php echo $checked; ?>/><mark></mark>
            <label for="{{ $row->state }}">{{ $row->state }}</label>
        </div>

        @endforeach

    </div>

</div>
   
</div>
<div class="form-group col-sm-4">
   <div class="input-group">

    <div class="panel-title style-select">
        <a type="button" data-toggle="collapse" data-target="#selectCity" class="collapsed">
            <i class="icon"><img src="{{ asset('front/images/icon-state.png')}}" alt="" /></i> <span id="selCity">Select City</span> </a>
    </div>
    <div id="selectCity" class="style-select-option collapse ride "></div>

</div> 
</div>
<div class="form-group col-sm-4">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-zipcode.png')}}" alt="" /></i></span>
            <input id="Zipcode" type="text" name="zip_code" value="{{ $model->zip_code }}" class="form-control"  placeholder="Zipcode">
      </div>
</div>

<div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-contactperson.png')}}" alt="" /></i></span>
            <input id="ContactPerson" type="text" class="form-control" name="contact_person" value="{{ $model->contact_person }}" placeholder="Contact Person">
      </div>
</div>

<div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-org.png')}}" alt="" /></i></span>
            <input id="OrganizationPosition" type="text" class="form-control" name="position" value="{{ $model->position }}" placeholder="Organization Position">
      </div>
    </div>


  <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-mobile.png')}}" alt="" /></i></span>
            <input id="Phone1" type="tel" class="form-control" name="phone" value="{{ $model->phone }}" placeholder="Phone*" required/>
      </div>
    </div>


    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-mobile.png')}}" alt="" /></i></span>
            <input id="Phone2" type="tel" class="form-control" name="phone2" value="{{ $model->phone2 }}" placeholder="Phone 02" />
      </div>
    </div>


    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-email.png')}}" alt="" /></i></span>
            <input id="EmailAddress" type="email" class="form-control" name="email" value="{{ $model->email }}" placeholder="Email Address*" required />
      </div>
    </div>
    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-key.png')}}" alt="" /></i></span>
            <input id="password" type="password" class="form-control" name="password" placeholder="Password"/>
      </div>
    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-key.png')}}" alt="" /></i></span>
            <input id="confirmPassword" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password"/>
      </div>
    </div>

    <div class="terms0 form-group col-sm-12">

            <label>Do you wish to receive Service Provider Advertisement</label>

            <span class="fnc__checkbox checker-area <?php echo ($model->advertisement==1)?'is-active':'' ;?>"><input type="radio" value="1" name="advertisement" <?php echo ($model->advertisement==1)?'checked':'' ;?> /></span> YES

            <span class="fnc__checkbox checker-area <?php echo ($model->advertisement==0)?'is-active':'' ;?>"><input type="radio" value="0" name="advertisement" <?php echo ($model->advertisement==0)?'checked':'' ;?> /></span> NO


    </div>


<div class="btns col-sm-12 text-right">
    <button type="submit" class="btn btn-primary">Update Account</button>
</div>

