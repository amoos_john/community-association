<div class="dash__nav navbar-blinds--hover ">
<nav class="navbar navbar-default" role="navigation">
<div class="container-fluid p0">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#dashNav">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse p0" id="dashNav">

      <ul class="nav navbar-nav navbar-list navbar-mega">
        <li><a href="{{ url("association/rfp/create") }}">Sends or Create RFPs</a></li>
        <li><a href="{{ url("association/rfp") }}">Check Status</a></li>
        @if(Auth::user()->role_id==2)
        <li><a href="{{ url("association/jobs") }}">Open Jobs</a></li>
        <li><a href="{{ url("association/jobs/create") }}">Post Open Jobs</a></li>
        <li><a href="{{ url("association/users/create") }}">Add User</a></li>
        <li><a href="{{ url("association/users") }}">Delete / Modify User</a></li>
        <li><a href="{{ url("association/profile/edit") }}">View Profile Account</a></li>
        <li><a href="{{ url("association/upgrade") }}" class="php2">Upgrade Package</a></li>
        @endif
      </ul>

    </div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>
</div>