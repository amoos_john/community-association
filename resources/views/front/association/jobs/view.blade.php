<div class="job__cont clrlist col-sm-12">
    @if(count($model)>0)
    <h3>Job Title</h3>
    <h4>{{ $model->title }}</h4>
    <p class="job__location">Hiring in {{ $model->location }}</p>
    <h3>Description</h3>
    <p class="m0"><?php echo $model->job_description; ?></p>
    @else
    <h3>Job not Found</h3>
    @endif
</div>