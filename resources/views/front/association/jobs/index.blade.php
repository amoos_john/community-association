@extends('front')

@section('content')
<?php
    use App\Functions\Functions;
?>
<section class="dashboard-page pb30">
    <div class="container">
                   

        <div class="row">

            <div class="dash__left col-sm-3 mt100">

                   @include('front/association/common/navigation') 

            </div>
            <div class="dash__rigt col-sm-9">
          <div class="hed"><h2>{{ $page_title }}</h2></div>
        @include('front/common/errors')
        @if (Session::has('message'))
        <div class="alert alert-warning alert-dismissible">
             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-info"></i> &nbsp  {!! session('message') !!}</h4>
        </div>
        @endif
        <div class="table-responsive table-check-status fnc-fom clrlist">

        <table class="table table-bdr--black text-center">
                <thead>
                <tr>
                        <th>Job Title</th>
                        <th>Created Date</th>
                        <th>Name</th>
                        <th>Email</th>
                        <!--<th>Status</th>-->
                        <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count($model)>0)
                    @foreach($model as $row)
                    <?php
                        //$status = "";
                        if (array_key_exists($row->status, $statuses)) {
                        $status = $statuses[$row->status];
                        }
                    ?>
                        <tr>
                                <td>{{ $row->title }}</td>
                                <td>{{ Functions::dateFrontFormat($row->created_at) }}</td>
                                <td>{{  $row->name }}</td>
                                <td>{{  $row->email }}</td>
                                <!--<td></td>-->
                                <td>
                                <ul>
                                    <li>
                                     <a data-target="#openJob" onClick="return open_job('{{ $row->id }}');" data-toggle="modal">View</a></li>

                                    <li><a href="{{ url("association/jobs/edit/".$row->id) }}">Edit</a></li>
                                    <li><a data-href="{{ url("association/jobs/delete/".$row->id) }}" data-target="#confirm-delete" data-toggle="modal" title="Void">
                                     Void</a></li>
                                </ul>
                        </td>
                        </tr>
                    @endforeach  
                @else
                    <tr>
                        <td colspan="5">Job's not found!</td>

                        </tr>

                @endif


                </tbody>

        </table>
                        
           <?php echo $model->render(); ?>        

        </div>
			
			
			
			</div>

            </div>

    </div>
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title text-center" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are you sure to delete this job?</p>
                </div>
                
                <div class="modal-footer">
                    <a  class="btn btn-info" data-dismiss="modal">Cancel</a>
                    <a class="btn btn-primary btn-ok" id="btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>  
   <div class="modal fade job-desc-modal" id="openJob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close " data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4></h4>
      </div>
      <div class="modal-body overload fnc-fom accordion-arr" id="details-modal">
        			
      </div>
    </div>
  </div>
</div>         
            
            
	</section>

<script>
       //$("a#rfp-sub-modal").click(function()  {
       
       
       function open_job(id)
       {
            //var id = $(this).attr('id');
            $.ajax({
                type: 'GET',
                url: '<?php echo  url("association/jobs/view"); ?>/'+id,
                success: function(data) 
                {
                    //$('#rfp-sub-modal').toggle();
                    $('#details-modal').html(data);
                }
            });
      }
   

</script> 
    


@endsection