@extends('front')

@section('content')
<?php
use App\Functions\Functions;
?>
<section class="easy-area pt30">
<div class="container">

            <div class="easy__steps shake-icon list-col-3 text-center">
                    <ul>
                            <li class="anime-flipInX active">
                                    <mark>Step 1</mark>
                                    <div class="icon"><img src="{{ asset('front/images/easy-icon1.png')}}" alt=""></div>
                                    <span>Create your Profile</span>
                            </li>
                            <li class="anime-flipInX delay1s active">
                                    <mark>Step 2</mark>
                                    <div class="icon"><img src="{{ asset('front/images/easy-icon2b.png')}}" alt=""></div>
                                    <span>Add Additional User</span>
                            </li>
                            <li class="anime-flipInX delay2s active">
                                    <mark>Step 3</mark>
                                    <div class="icon"><img src="{{ asset('front/images/easy-icon3.png')}}" alt=""></div>
                                    <span>Membership Package</span>
                            </li>
                    </ul>
            </div>

</div>
</section>

<section class="package-area membership-packages ">
<div class="container">
    <div class="hed">
            <h2>Membership Packages</h2>
    </div>

    @foreach($packages as $key=>$package)
    @if($key==0)
        <?php $animation = "anime-left delay1s";?>
    @elseif($key==1)
        <?php $animation = "anime-zoomIn";?>
    @else
        <?php $animation = "anime-right delay1s";?>
    @endif
    <div class="package-box col-sm-4 {{ $animation }}">
            <div class="package__inr">
                    <div class="package__img">
                            <img src="{{ url('front/images/'.$package["image"])}}" alt="package" />
                    </div>
                    <div class="package__cont">
                            <h3><i class="icon-price"></i> {{ Functions::moneyFormat($package["price"]) }}</h3>
                            <h4>{{ $package["package_name"] }}</h4>
                            <h5>Send up to {{ $package["total_rfp"] }} RFPs per {{ $package["duration"] }}</h5>
                            <h5>{{ $package["description"] }}</h5>
                            <h5><strong>Exp date: {{ $date }}</strong></h5>
                            <div class="lnk-btn">
                                    <a href="{{ url('association/billing/'.$package["id"])  }}" class="btn btn-default">Subscribe Now</a>
                            </div>
                    </div>
            </div>
    </div>

        @endforeach


</div>
</section>
@endsection