@extends('front')

@section('content')
<style>
    .center
    {
        margin:0 auto;
       
    }
</style>
<section class="dashboard-page pb30">
    <div class="container">

        <div class="row">

            <div class="dash__left col-sm-3 mt100">

                @include('front/association/common/navigation') 

            </div>

            <div class="dash__right col-sm-9">

                <div class="hed"><h2>Dashboard</h2></div>
                <div class="center">
                    <div class="vendor-total bg-cvr col-sm-4 col-sm-offset-2" style="background-image:url('{{ asset('front/images/steel-bg.png')}}');">
                        <h3>Remaining RFP’S</h3></td>
                        <h2><?php echo $total_rfps; ?></h2>
                        <div class="arrow-arc"></div>
                    </div>

                    <div class="vendor-total bg-cvr col-sm-4" style="background-image:url('{{ asset('front/images/steel-bg.png')}}');">
                        <h3>Created RFP’S</h3></td>
                        <h2><?php echo $created_rfps; ?></h2>
                        <div class="arrow-arc"></div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>

@endsection