@extends('front')

@section('content')

<section class="easy-area pt30">
<div class="container">

            <div class="easy__steps shake-icon list-col-3 text-center">
                    <ul>
                            <li class="anime-flipInX active">
                                    <mark>Step 1</mark>
                                    <div class="icon"><img src="{{ url('front/images/easy-icon1.png')}}" alt=""></div>
                                    <span>Create your Profile</span>
                            </li>
                            <li class="anime-flipInX delay1s active">
                                    <mark>Step 2</mark>
                                    <div class="icon"><img src="{{ url('front/images/easy-icon2b.png')}}" alt=""></div>
                                    <span>Add Additional User</span>
                            </li>
                            <li class="anime-flipInX delay2s">
                                    <mark>Step 3</mark>
                                    <div class="icon"><img src="{{ url('front/images/easy-icon3.png')}}" alt=""></div>
                                    <span>Membership Package</span>
                            </li>
                    </ul>
            </div>

</div>
</section>

<section class="package-area membership-packages association-profile pb50 fom-bottomline--focus fnc-fom accordion-arr">
<div class="container">
<div class="hed">
    <h2>Contact Information for Additional User</h2>
</div>
@include('front/common/errors')       
{!! Form::open(array( 'class' => 'form','url' => 'association/additional/insert', 'files' => true)) !!}

    
    <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-username.png')}}" alt="" /></i></span>
                <input id="Username" type="text" class="form-control" name="username" value="{{ (isset($username))?$username:old('username') }}" placeholder="User Name">
                <input type="hidden" name="id" value="{{ (isset($id))?$id:'' }}" />
          </div>
        </div>

        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-groupuser.png')}}" alt="" /></i></span>
                <input id="AssociationName" type="text" value="{{ (isset($name))?$name:old('name') }}"  class="form-control" name="association_name" placeholder="Association Name">
          </div>
        </div>


        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-org.png')}}" alt="" /></i></span>
                <input id="OrganizationPosition" type="text" class="form-control" name="position" placeholder="Organization Position">
          </div>
        </div>


        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-contactperson.png')}}" alt="" /></i></span>
                <input id="PhoneNumber" type="tel" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Phone Number">
          </div>
        </div>


        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-email.png')}}" alt="" /></i></span>
                <input id="EmailAddress" type="email" value="{{ (isset($email))?$email:old('email') }}" class="form-control" name="email" placeholder="Email Address*">
          </div>
        </div>



        <div class="terms0  form-group col-sm-12">

                <label>Add me as additional user</label>

                <span class="fnc__checkbox checker-area"><input type="radio" name="add_user" value="1" <?php echo (old("add_user")==1)?'checked':'' ;?>></span> YES
                <span class="fnc__checkbox checker-area"><input type="radio"  name="add_user" value="0" <?php echo (old("add_user")==0)?'checked':'' ;?>></span> NO

        </div>


          <div class="btns col-sm-12 text-right">
                        <!--<button class="btn btn-primary">Create Account</button>-->
                        <button type="submit" class="btn btn-primary">Save & Continue</button>
          </div>
		

{!! Form::close() !!}	


</div>
</section>
@endsection