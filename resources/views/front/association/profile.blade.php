@extends('front')

@section('content')

<section class="easy-area pt30">
<div class="container">

            <div class="easy__steps shake-icon list-col-3 text-center">
                    <ul>
                            <li class="anime-flipInX active">
                                    <mark>Step 1</mark>
                                    <div class="icon"><img src="{{ url('front/images/easy-icon1.png')}}" alt=""></div>
                                    <span>Create your Profile</span>
                            </li>
                            <li class="anime-flipInX delay1s ">
                                    <mark>Step 2</mark>
                                    <div class="icon"><img src="{{ url('front/images/easy-icon2b.png')}}" alt=""></div>
                                    <span>Add Additional User</span>
                            </li>
                            <li class="anime-flipInX delay2s">
                                    <mark>Step 3</mark>
                                    <div class="icon"><img src="{{ url('front/images/easy-icon3.png')}}" alt=""></div>
                                    <span>Membership Package</span>
                            </li>
                    </ul>
            </div>

</div>
</section>



<section class="package-area membership-packages association-profile pb50 fom-bottomline--focus fnc-fom accordion-arr">
<div class="container">
<div class="hed">
    <h2>Association Profile</h2>
</div>
@include('front/common/errors')      
{!! Form::open(array( 'class' => 'form','url' => 'association/profile/insert', 'files' => true)) !!}

   <div class="form-group col-sm-12">
        <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-groupuser.png')}}" alt="" /></i></span>
                <input type="text" name="name" value="{{ (isset($name))?$name:'' }}" id="name"  class="form-control"  placeholder="Association Name*" required="">
                <input type="hidden" name="id" value="{{ (isset($id))?$id:'' }}" />
          </div>
        </div>


        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-website.png')}}" alt="" /></i></span>
                <input id="WebsiteURL" type="url" name="website_url" value="{{ old('website_url') }}" class="form-control"  placeholder="Website URL">
          </div>
        </div>


        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-map.png')}}" alt="" /></i></span>
                <input id="PhysicalAddress" name="address" value="{{ old('address') }}" type="text" class="form-control"  placeholder="Physical Address">
          </div>
        </div>


        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-map.png')}}" alt="" /></i></span>
                <input id="GoogleMapLink" type="text" name="google_map" value="{{ old('google_map') }}" class="form-control"  placeholder="Google Map Link">
          </div>
        </div>


        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-state.png')}}" alt="" /></i></span>
                <input id="NumberofBuildings" type="text" name="no_of_bulidings" value="{{ old('no_of_bulidings') }}" class="form-control"  placeholder="Number of Buildings">
          </div>
        </div>


        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-unit.png')}}" alt="" /></i></span>
                <input id="NumberofUnits" type="text" class="form-control" name="no_of_units" value="{{ old('no_of_units') }}" placeholder="Number of Units">
          </div>
        </div>


        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-grouptype.png')}}" alt="" /></i></span>
                <input id="TypesofAssociation" type="text" class="form-control" name="association_type" value="{{ old('association_type') }}" placeholder="Types of Association">
          </div>
        </div>

        <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close rotating360" data-dismiss="modal" aria-hidden="true">&times;</button>
              </div>
              <div class="modal-body overload">

                        <div class="hed text-uppercase"><h2>Add Additional User</h2></div>

                        <div class="form-group col-sm-12">
                                <div class="input-group">
                                        <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-username.png')}}" alt="" /></i></span>
                                        <input id="Name" type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Name">
                                </div>
                        </div>

                        <div class="form-group col-sm-12">
                                <div class="input-group">
                                        <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-email.png')}}" alt="" /></i></span>
                                        <input id="EmailAddress" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address">
                                </div>
                        </div>

              </div>
            </div>
          </div>
        </div>

          <div class="btns col-sm-12 text-right">
                        <button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#addUserModal">Add Additional User</button>
                        <button type="submit" class="btn btn-primary">Save & Continue</button>
          </div>
		

{!! Form::close() !!}	



</div>
</section>
@endsection