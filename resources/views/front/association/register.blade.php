@extends('front')

@section('content')

<section class="easy-area pt30">
    <div class="container">

        <div class="easy__steps shake-icon list-col-3 text-center">
            <ul>
                <li class="anime-flipInX active">
                    <mark>Step 1</mark>
                    <div class="icon"><img src="{{ asset('front/images/easy-icon1.png')}}" alt=""></div>
                    <span>Create your Profile</span>
                </li>
                <li class="anime-flipInX delay1s ">
                    <mark>Step 2</mark>
                    <div class="icon"><img src="{{ asset('front/images/easy-icon2b.png')}}" alt=""></div>
                    <span>Add Additional User</span>
                </li>
                <li class="anime-flipInX delay2s">
                    <mark>Step 3</mark>
                    <div class="icon"><img src="{{ asset('front/images/easy-icon3.png')}}" alt=""></div>
                    <span>Membership Package</span>
                </li>
            </ul>
        </div>

    </div>
</section>



<section class="package-area membership-packages association-profile pb50 fom-bottomline--focus fnc-fom accordion-arr">
    <div class="container">
        <div class="hed">
            <h2>Create your Profile</h2>
        </div>
        @include('front/common/errors')       
        {!! Form::open(array( 'class' => 'form','url' => 'association/insert', 'files' => true)) !!}


        <div class="form-group col-sm-12">
            <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-groupuser.png')}}" alt="" /></i></span>
                <input type="text" name="name" id="name"  class="form-control"  placeholder="Association Name*" required="required" value="{{ old('name') }}"/>
            </div>
        </div>

        <div class="form-group col-sm-4">
            <div class="input-group">

                <div class="panel-title style-select">
                    <a type="button" data-toggle="collapse" data-target="#selectState" class="collapsed">
                        <i class="icon"><img src="{{ asset('front/images/icon-state.png')}}" alt="" /></i> <span id="selState">Select State</span> </a>
                </div>


                <div id="selectState" class="style-select-option collapse ride ">

                    @foreach($states as $row)

                    <div class="form-group">
                        <input type="radio" name="states" onChange="getCities('{{ $row->state_code }}')" value="{{ $row->state_code }}" id="{{ $row->state }}" /><mark></mark>
                        <label for="{{ $row->state }}">{{ $row->state }}</label>
                    </div>

                    @endforeach

                </div>

            </div>
        </div>

        <div class="form-group col-sm-4">
            <div class="input-group">

                <div class="panel-title style-select">
                    <a type="button" data-toggle="collapse" data-target="#selectCity" class="collapsed">
                        <i class="icon"><img src="{{ asset('front/images/icon-state.png')}}" alt="" /></i> <span id="selCity">Select City</span> </a>
                </div>
                <div id="selectCity" class="style-select-option collapse ride "></div>
                 
            </div>
        </div>






        <div class="form-group col-sm-4">
            <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-zipcode.png')}}" alt="" /></i></span>
                <input id="Zipcode" type="text" name="zip_code" value="{{ old('zip_code') }}" class="form-control"  placeholder="Zipcode">
            </div>
        </div>


        <div class="form-group col-sm-12">
            <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-contactperson.png')}}" alt="" /></i></span>
                <input id="ContactPerson" type="text" class="form-control" name="contact_person" value="{{ old('contact_person') }}" placeholder="Contact Person">
            </div>
        </div>


        <div class="form-group col-sm-12">
            <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-org.png')}}" alt="" /></i></span>
                <input id="OrganizationPosition" type="text" class="form-control" name="position" value="{{ old('position') }}" placeholder="Organization Position">
            </div>
        </div>


        <div class="form-group col-sm-12">
            <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-mobile.png')}}" alt="" /></i></span>
                <input id="Phone1" type="tel" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Phone*" required/>
            </div>
        </div>


        <div class="form-group col-sm-12">
            <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-mobile.png')}}" alt="" /></i></span>
                <input id="Phone2" type="tel" class="form-control" name="phone2" value="{{ old('phone2') }}" placeholder="Phone 02" />
            </div>
        </div>


        <div class="form-group col-sm-12">
            <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ asset('front/images/icon-email.png')}}" alt="" /></i></span>
                <input id="EmailAddress" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address*" required />
            </div>
        </div>

        <div class="terms0 form-group col-sm-12">

            <label>Do you wish to receive Service Provider Advertisement</label>

            <span class="fnc__checkbox checker-area"><input type="radio" value="1" name="advertisement" <?php echo (old("advertisement") == 1) ? 'checked' : ''; ?> /></span> YES

            <span class="fnc__checkbox checker-area"><input type="radio" value="0" name="advertisement" <?php echo (old('advertisement') == 0) ? 'checked' : ''; ?> /></span> NO


        </div>


        <div class="btns col-sm-12 text-right">
            <button class="btn btn-primary" name="account">Create Account</button>
            <button type="submit" class="btn btn-primary" name="account">Save & Continue</button>
        </div>

        {!! Form::close() !!}	


    </div>
</section>
<script>

    function getCities(code)
    {
    var url = 'state_code=' + code;
    $.ajax({
    type: "GET",
            url: "{{ url('get/cities') }}",
            data: url,
            success: function(data){
            $('#selectCity').html(data);
            },
            error: function(errormessage) {
            //you would not show the real error to the user - this is just to see if everything is working
            alert(errormessage);
            }
    });
    }</script>
@endsection