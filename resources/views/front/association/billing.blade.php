@extends('front')

@section('content')
<?php
$required="required";
?>

<section class="login-area clrlist is-active0">
	<div class="container">
		<div class="hed">
			<h2>{{ $page_title }}</h2>
		</div>
		
		
		<div class="billing-info-fom fom-bottomline--focus fnc-fom col-sm-8 mb50 accordion-arr">
@include('front/common/errors')       
@if(isset($user_id))
 {!! Form::model($model, ['files' => true,'class' => 'form_chk','url' => ['association/billing/insert/'.$package_id], 'method' => 'post']) !!}
@else
{!! Form::open(array( 'class' => 'form_chk','url' => 'association/billing/insert/'.$package_id, 'files' => true)) !!}
@endif

         <div class="row">

         <div class="form-group col-sm-12">
                  <div class="input-group">
                        <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-groupuser.png')}}" alt="" /></i></span>
                        <input id="AssociationName" type="text" class="form-control" name="name" value="{{ (isset($name))?$name:'' }}" placeholder="Association Name*" required="required"/>
                        <input type="hidden" name="id" value="{{ (isset($id))?$id:'' }}" />
                  </div>
          </div>

        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-username.png')}}" alt="" /></i></span>
                {!! Form::text('username', null , array('class' => 'form-control','id'=>'UserName','placeholder'=>'User Name*',$required) ) !!}

          </div>
        </div>
        <!--     
        <div class="form-group col-sm-6">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-lock.png')}}" alt="" /></i></span>
                <input id="password" type="password" class="form-control" name="password" placeholder="Password*" />
          </div>
        </div>

        <div class="form-group col-sm-6">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-lock.png')}}" alt="" /></i></span>
                <input id="ConfirmPassword" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password*" />
          </div>
        </div>


        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-email.png')}}" alt="" /></i></span>
                {!! Form::email('email', null , array('class' => 'form-control','id'=>'EmailAddress','placeholder'=>'Email*',$required) ) !!}

          </div>
        </div> 
        -->
        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-map.png')}}" alt="" /></i></span>
                {!! Form::text('address', null , array('class' => 'form-control','id'=>'Address1','placeholder'=>'Address 1') ) !!}

          </div>
        </div>  

        <div class="form-group col-sm-12">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-map.png')}}" alt="" /></i></span>
                {!! Form::text('address2', null , array('class' => 'form-control','id'=>'Address2','placeholder'=>'Address 2') ) !!}

          </div>
        </div>

        <div class="form-group col-sm-6">			  
          <div class="input-group">
                <span class="input-group-addon"><i class=" icon"><img src="{{ url('front/images/icon-state.png')}}" alt="" /></i></span>
                {!! Form::text('city', null , array('class' => 'form-control','id'=>'City','placeholder'=>'City') ) !!}

          </div>
        </div>

        <div class="form-group col-sm-6">			  
          <div class="input-group">
                <span class="input-group-addon"><i class=" icon"><img src="{{ url('front/images/icon-state.png')}}" alt="" /></i></span>
                {!! Form::text('state', null , array('class' => 'form-control','id'=>'State','placeholder'=>'State') ) !!}

          </div>
        </div>


        <div class="form-group col-sm-6">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-zipcode.png')}}" alt="" /></i></span>
                {!! Form::text('zip_code', null , array('class' => 'form-control','id'=>'Zipcode','placeholder'=>'Zipcode') ) !!}

          </div>
        </div>


        <div class="form-group col-sm-6">
          <div class="input-group">
                <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-mobile.png')}}" alt="" /></i></span>
               {!! Form::tel('phone', null , array('class' => 'form-control','id'=>'PhoneNumber','placeholder'=>'Phone Number') ) !!}

          </div>
        </div>


        <div class="form-group col-sm-12">
          <div class="input-group">
                <label>Help us Fight Spam</label>
                {!! app('captcha')->display()!!}
                {!! $errors->first('g-recaptcha-response','<p class="alert alert-danger">:message</p>')!!}
          </div>
        </div>


        <div class="form-group col-sm-12">  
          <div class="terms checker-area">
                <span class="fnc__checkbox"><input type="checkbox" name="terms" id="terms" required/></span> <label>Yes, I understand and agree to the  <a href="#">Community Association Resources Terms of Service</a>, including the  <a href="#">User Agreement</a> and <a href="#">Privacy Policy.</a></label>
          </div>
        </div>

          <div class="clearfix"></div>
          <div class="login__submit col-sm-12 mt20 text-right">
                <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#paymentModal">Submit & Continue</button>
                <!--<button type="button" class="btn btn-primary ">Submit & Exit</button>-->
          </div>

          </div>
	@include('front.common.payment')
        {!! Form::close() !!}		
	</div>
		
		<div class="billing-logos col-sm-4">
			<img src="{{ asset('front/images/payment-logos.jpg')}}" alt="" />
		</div>
		
		
	</div>
</section>
<script src="{{ asset('front/js/card.min.js')}}"></script>

<script>
function myform()
{
    var empty = false;

    if ($("#payment_type").val() == '') {
        empty = true;
    }
    else if($("#card_name").val() == '')
    {
        empty = true;
    }
    else if($("#card_no").val() == '')
    {
        empty = true;
    }
    else if($("#cvc_no").val() == '')
    {
        empty = true;
    }

    else if($("#month").val() == '')
    {
        empty = true;
    }
    else if($("#year").val() == '')
    {
        empty = true;
    }



if (empty) {
  $("#btn-order").attr('disabled', true);

} else {
     $('#btn-order').removeAttr('disabled');

}
}
$( "body" ).load(function() {
    myform();
});

$('.form_chk :input').keyup(function() {
   myform(); 
});
new Card({
  form: '.form_chk',
  container: '.card',
  formSelectors: {
    numberInput: 'input[name=card_no]',
    expiryInput: 'input[name=month]',
    cvcInput: 'input[name=cvc_no]',
    nameInput: 'input[name=card_name]'
  },

  width: 390, // optional — default 350px
  formatting: true,

  placeholders: {
    card_no: '•••• •••• •••• ••••',
    card_name: 'Full Name',
    ccExpiry: '••/••',
    cvvNumber: '•••'
  }
})
  </script>
@endsection