@extends('front')

@section('content')
<?php
    $required = "required";
?>
<style type="text/css">

     /*This is the div within which the signature canvas is fitted*/
     #signature{
          border: 1px solid #ccc;
          background-color:#fff;
          margin-top:10px;
     }
     .datesignature{
          border: 1px solid #ccc;
          background-color:#fff;
          margin-top:10px;
     }
     .img-thumb img {
  
    width: 100%;
    }
    tbody.vendor-table {
    overflow-y: scroll;
    height: 400px;
    display: block;
}
.col-sm-8.margin {
    margin-top: 10px;
    padding: 0 !important;
}
.form-group.col-sm-9 {
    margin: 0 !important;
}
.form-group.col-sm-3 {
    margin: 0 !important;
}
 .fnc-fom input[type="checkbox"] {
    opacity: 1;
}
.table-vendor h4 {
  
    font-size: 14px;
}
     /* Drawing the 'gripper' for touch-enabled devices */ 
    
     

</style>
<section class="dashboard-page pb30">
    <div class="container">

        <div class="row">

            <div class="dash__left col-sm-3 mt100">

                   @include('front/association/common/navigation') 

            </div>

            
<div class="dash__right col-sm-9">

        <div class="hed"><h2>{{ $page_title }}</h2></div>



        <div class="fom fnc-fom accordion-arr fom-bottomline--focus fnc__checkbox--rounded">
        @include('front/common/errors')    

    <div class="col-sm-12">
        <a  id="allcheck" class="btn  btn-info">Select All</a>
        <a  id="alluncheck" class="btn btn-primary">Clear All</a>
    </div>
        
    <div class="col-sm-8 margin">
    <div class="form-group col-sm-9">    
    <label>Search by name</label>    
    <input type="text" name="search" id="search" class="form-control" placeholder="Search"/>
    </div>
   <div class="form-group col-sm-3">
    <label>Per page</label>
    
    <select class="form-control" name="per_page" id="per_page"> 
        <option value="10">10</option>
        <option value="20">20</option>
        <option value="30">30</option>        
   </select>
   
    </div>
    </div>

    {!! Form::open(array( 'class' => 'form','url' => 'association/rfp/vendor/insert/'.$id, 'files' => true)) !!}	

    <div class="desire-vendors-area table-responsive col-sm-8" >
    <div id="list_vendors">  
       @include('front.association.rfp.search')     
    </div>

        <div class="col-sm-12 text-center data-print_box" id="btn_sig"  data-toggle="modal" data-target="#signatureModal"  >     
            <img id="signature_user_display" src="<?php echo ($rfp->signature=="")?asset('front/images/sign.png'):asset('/'.$rfp->signature);?>" alt="signature">  

        </div>
        <div class="col-sm-12 text-right p0">


                <button type="button" class="btn btn-primary "  data-toggle="modal" data-target="#submitRFPModal">Save & Submit</button>
                <button type="submit" class="btn btn-info">Save & Exit</button>
        </div>


        </div>
			
			
    <div class="vendor-total bg-cvr col-sm-4" style="background-image:url('{{ asset('front/images/steel-bg.png')}}');">
                    <h3>Remaining RFP’S</h3></td>
                    <h2><?php echo $total_rfps;?></h2>
                    <div class="arrow-arc"></div>
    </div>


</div>

</div>

</div>
			
 
<div class="modal fade modal-vcntr" id="submitRFPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        
      </div>
      <div class="modal-body text-center">
		
          <p class="lead">Please confirm your submission of '"RFP Title" <br/>to your <span id="chosen"></span> chosen vendors</p>

          <button type="submit" class="btn btn-primary w60 pul-cntr" id="submit-now" disabled="">Submit Now</button>
	  
      </div>
      
    </div>
  </div>
</div>
        
<!-- Modal signature -->
     <!-- Modal -->
     <div id="signatureModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

               <!-- Modal content-->
               <div class="modal-content">
                    <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                         
                    </div>
                    <div class="modal-body">

                         <div id="content">
                              <div class="hdr__cont__sign black">
                                   <h3>Add Signature</h3>
                                   <p>Your signature is required below.</p>
                              </div>
                              <div id="signatureparent">
                                   <div id="signature"></div>
                              </div>
                         </div>
                    </div>

                    <div class="modal-footer">
                         <div class="sign-buttons">
                              <div class="sign-btns-left">
                                   <button onclick="$('#signature').jSignature('clear');" class="btn btn-primary" type="button">CLEAR SIGN</button>
                                   <button data-toggle="modal" data-dismiss="modal"  class="btn btn-info"   type="button">Close</button>
                              </div>
                              <!--<div class="sign-btns-rgt">
                                   <button data-toggle="modal" data-dismiss="modal"  class="save-sign btn btn-md modal-btn btn-custom"   type="button">Save</button>
                              </div>-->
                         </div>
                    </div>
               </div>

          </div>
     </div>



     <!--signature ends-->        
        
        
{!! Form::close() !!}
			
		</div>
	</section>
<script>

    $("#search").on('keyup',function(){
        
        searchResult('');
    });
   $("#per_page").on('change',function(){
        
        searchResult('');
    });
    $("#btnclear").on('click',function(){
        
        $('#search').val(''); 
    });
    $(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();
        var url = $(this).attr('href'); 
        var getval = url.split('=');
        var value=getval[1];
        
        //$("html, body").animate({ scrollTop: 0 }, "slow");
        searchPagination(value);
        return false;

        });

    });
    function searchPagination(value)
    {
        var originalURL="<?php echo (Request::getQueryString() ? ( Request::getQueryString()) : ''); ?>";

        var url = removeParam('page', originalURL,value);
        var search =  '&page='+ value;
        
        searchResult(search);


    }
    
    function removeParam(key, sourceURL,value) {
         var rtn = sourceURL.split("?")[0],
             param,
             pvalue,
             params_arr = [],
             queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
         if (queryString !== "") {
             params_arr = queryString.split("&");
             for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                 param = params_arr[i].split("=")[0];
                 pvalue = params_arr[i].split("=")[1];
                 //console.log(pvalue);
                 if (param === key) {
                     params_arr.splice(i, 1);
                 }
             }
             
             rtn = rtn + "?" + params_arr.join("&");
         }
         return rtn;
     }
    function searchResult(value)
    {
        var search = $('#search').val();
        var per_page = $('#per_page').val();
       
        var datasend = 'search='+search+'&per_page='+per_page+value;
        
        $.ajax({
            type: "GET",
            url: "<?php echo url('association/rfp/vendors/search'); ?>",
            data: datasend,
            cache:false,
            success: function(data){
    
                $('#list_vendors').html(data);
                
            }
            
        });
    }
</script>

@include('front.association.common.signature')
@endsection