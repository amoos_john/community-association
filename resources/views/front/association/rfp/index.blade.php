@extends('front')

@section('content')
<?php
    use App\Functions\Functions;
    $required = "required";
?>
<section class="dashboard-page pb30">
    <div class="container">

        <div class="row">

            <div class="dash__left col-sm-3 mt100">

                   @include('front/association/common/navigation') 

            </div>

            
 
        <div class="dash__right col-sm-9">
            <div class="hed"><h2>{{ $page_title }}</h2></div>
                @include('front/common/errors')
     @if (Session::has('message'))
    <div class="alert alert-warning alert-dismissible">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	<h4><i class="icon fa fa-info"></i> &nbsp  {!! session('message') !!}</h4>
    </div>
    @endif
        <div class="table-responsive table-adduser table-check-status fnc-fom clrlist">

        <table class="table table-bdr--black text-center">
                <thead>
                <tr>
                        <th>RFP Title</th>
                        <th>Created Date</th>
                        <th>Closed Date</th>
                        <th>Status</th>	
                        <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count($model)>0)
                    @foreach($model as $row)
                    <?php
                        $status = "";
                        if (array_key_exists($row->status, $statuses)) {
                        $status = $statuses[$row->status];
                        }
                        if($row->submission_date<$date)
                        {
                            $status = 'Expire';
                        }
                        
                    ?>
                    <tr>
                            <td>{{ $row->title }}</td>
                            <td>{{ Functions::dateFrontFormat($row->created_at) }}</td>
                            <td>{{ Functions::dateFrontFormat($row->submission_date) }}</td>
                            <td>{{ $status }}</td>
                            <td>
                            <ul>
                                <li>
                                <a data-target="#rfp-sub-modal" onClick="return rfp_modal('{{ $row->id }}');" data-toggle="modal">View</a></li>
                                @if($date<=$row->submission_date)
                                <li><a href="{{ url("association/rfp/edit/".$row->id) }}">Edit</a></li>
                                <li><a data-href="{{ url("association/rfp/delete/".$row->id) }}" data-target="#confirm-delete" data-toggle="modal" title="Void">
                                 Void</a></li>
                                @endif
                            </ul>
                    </td>
                    </tr>
                    @endforeach  
                @else
                    <tr>
                        <td colspan="6">RFP's not found!</td>

                    </tr>

                @endif


                </tbody>

        </table>
                        
           <?php echo $model->render(); ?>        

        </div>
			
			
			
			</div>
			
			
			
		</div>
       <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title text-center" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are you sure to delete this rfp?</p>
                </div>
                
                <div class="modal-footer">
                    <a  class="btn btn-info" data-dismiss="modal">Cancel</a>
                    <a class="btn btn-primary btn-ok" id="btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>    
  <div class="modal fade job-desc-modal" id="rfp-sub-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close " data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4></h4>
      </div>
      <div class="modal-body overload fnc-fom accordion-arr" id="details-modal">
        			
      </div>
    </div>
  </div>
</div>         
            
            
	</section>

<script>
       //$("a#rfp-sub-modal").click(function()  {
       
       
       function rfp_modal(id)
       {
            $('#details-modal').html("");
            $.ajax({
                type: 'GET',
                url: '<?php echo  url("association/rfp/view"); ?>/'+id,
                success: function(data) 
                {
                    //$('#rfp-sub-modal').toggle();
                    $('#details-modal').html(data);
                }
            });
      }
      localStorage.clear();
   

</script>
@endsection