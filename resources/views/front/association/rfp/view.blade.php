<div class="job__cont clrlist col-sm-12">
    @if(count($model)>0)
    <div class="form-group col-sm-4 text-right pull-right" >
        <img class="img-responsive img-thumbnail" src="{{ asset('/'.$model->image)}}" alt="{{ $model->image_name }}" />
    </div>
    <h4>RFP Title</h4>
    <h4>{{ $model->title }}</h4>
    <h4>Location</h4>
    <p class="job__location">{{ $model->location }}</p>
    <h4>Description</h4>
    <p class="m0"><?php echo $model->job_description; ?></p>
    <div class="job__sign col-sm-6">
            <h5>Signature:</h5>
            <img src="{{ ($model->signature!="")?asset('/'.$model->signature):asset('front/images/signature.png') }}" alt="signature" />
    </div>
    <div class="job__date text-right col-sm-6">
            <h5>Submission Date:</h5>
            <p>{{ $submission_date }}</p>
            @if($model->document!="")
            <div class="job__attch">
                    <a href="{{ asset(''.$model->document) }}"   <?php echo ($model->document!="")?'download':'' ?>>
                    <img src="{{ asset('front/images/icon-attachment.png') }}" alt="attachment icon" />Download File</a>
            </div>
            @endif
    </div>
    @else
    <h3>RFP not Found</h3>
    @endif
</div>