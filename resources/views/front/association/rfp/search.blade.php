 <table class="table table-cost table-vendor table-bdr--row  text-center clrhm">
    <tbody id="checkbox-container">
        @if(count($model)>0)
        @foreach($model as $row)
        <tr>
            <td><input type="checkbox" name="vendor_id[]" value="{{ $row->id }}" class="avail_chk" id="checkbox-{{ $row->id }}"/>
            </td>
            <td><span class="img-thumb img-cntr img-thumb70x70"><img src="{{ asset('/'.$row->logo)}}" alt="" /></span></td>
            <td><h4>{{ $row->name }} <i class="fa fa-info"></i></h4></td>
            <td><a href="#">{{ (count($row->cities)>0)?$row->cities->city:'' }}</a></td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="4" class="text-center">Result not found!</td>
        </tr>
        @endif
    </tbody>
</table>
<div class="text-right">
    <?php echo $model->render(); ?>
</div> 
<script>
var checkboxValues = JSON.parse(localStorage.getItem('checkboxValues')) || {},
    $checkboxes = $("#checkbox-container :checkbox");

$checkboxes.on("change", function(){
  $checkboxes.each(function(){
    checkboxValues[this.id] = this.checked;
    //console.log(checkboxValues[this.id]);
  });
  
  localStorage.setItem("checkboxValues", JSON.stringify(checkboxValues));
});

// On page load
$.each(checkboxValues, function(key, value) {
 
  $("#" + key).prop('checked', value);
  $("#" + key).parent('.in-view').addClass('is-active');
  
});
$(document).ready(function(){   
    $("#allcheck").click(function(){
  
         //$('.in-view').addClass('is-active');
         $(".avail_chk").prop('checked',true);
         var n = $("input:checkbox:checked").length;
         $("#chosen").text(n);
         $("#submit-now").removeAttr('disabled');
        

    });
     $("#alluncheck").click(function(){

    $('.in-view').removeClass('is-active');
    $(".avail_chk").attr('checked',false);
    var n = $("input:checkbox:checked").length;
    $("#chosen").text(n);
    $("#submit-now").attr('disabled',true);

    });
    
    $(".avail_chk").change(function(){
  
        var n = $("input:checkbox:checked").length;
        $("#chosen").text(n);
        $("#submit-now").removeAttr('disabled');
        

    });
    
    
  });
</script>