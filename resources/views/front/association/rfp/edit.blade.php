@extends('front')

@section('content')
<?php
    $required = "required";
?>
<style>
td.disabled.day {
    opacity: 0.2;
}
#signature{
    border: 1px solid #ccc;
    background-color:#fff;
    margin-top:10px;
}
.datesignature{
    border: 1px solid #ccc;
    background-color:#fff;
    margin-top:10px;
}
.img-thumb img {

    width: 100%;
}
</style>
<section class="dashboard-page pb30">
    <div class="container">

    <div class="row">

    <div class="dash__left col-sm-3 mt100">

           @include('front/association/common/navigation') 

    </div>
    <div class="dash__right col-sm-9">

    <div class="hed"><h2>{{ $page_title }}</h2></div>



   <div class="fom fnc-fom accordion-arr fom-bottomline--focus">

    @include('front/common/errors')       
    {!! Form::model($model, ['files' => true,'class' => 'form','url' => ['association/rfp/update', $model->id], 'method' => 'post']) !!}

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-groupuser.png')}}" alt="" /></i></span>
            <input id="name" type="text" class="form-control" name="title" value="{{ $model->title }}" placeholder="RFP Title*" <?php echo $required;?>/>
      </div>
    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-groupuser.png')}}" alt="" /></i></span>
            <input id="name" type="text" class="form-control" name="name" value="{{ $model->name }}" placeholder="Association Name*" <?php echo $required;?>/>
      </div>
    </div>

    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-map.png')}}" alt="" /></i></span>
            <input id="location" type="text" class="form-control" name="location" value="{{ $model->location }}" placeholder="Association Location">
      </div>
    </div>



    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-paper.png')}}" alt="" /></i></span>
            <input id="submission_date" type="text" class="form-control" name="submission_date" value="{{ $model->submission_date }}" placeholder="RFP Submission Date*" <?php echo $required;?>/>
      </div>
    </div>


    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-pressbutton.png')}}" alt="" /></i></span>
            <input id="point_contact" type="text" class="form-control" name="point_contact" value="{{ $model->point_contact }}" placeholder="Point of Contact" <?php echo $required;?>/>
      </div>
    </div>



    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-contactbook.png')}}" alt="" /></i></span>
            <input id="point_contact_phone" type="tel" class="form-control" name="point_contact_phone" value="{{ $model->point_contact_phone }}" placeholder="Point of Contact Phone Number" <?php echo $required;?>/>
      </div>
    </div>




    <div class="form-group col-sm-12 icon-valign--top">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-paper.png')}}" alt="" /></i></span>
            <textarea id="job_description" class="form-control minh150" name="job_description" placeholder="Full Job Description*" <?php echo $required;?>>{{ $model->job_description }}</textarea>
      </div>
    </div>




    <div class="form-group col-sm-12 icon-valign--top">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-worker.png')}}" alt="" /></i></span>
            <textarea id="requirement" class="form-control minh150" name="requirement"  placeholder="Contractor Requirements*" <?php echo $required;?>>{{ $model->requirement }}</textarea>
      </div>
    </div>



    <div class="form-group col-sm-12 icon-valign--top">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-manstand.png')}}" alt="" /></i></span>
            <textarea id="project_specification" class="form-control minh150" name="project_specification" placeholder="Project Specification*" <?php echo $required;?>>{{ $model->project_specification }}</textarea>
      </div>
    </div>




    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-photo.png')}}" alt="" /></i></span>
            <input id="Picture1" type="text" class="form-control" name="image_name" value="{{ $model->image_name }}" placeholder="Picture#1">
      </div>

            <div class="file-drop-area fnc-uplaod">
                      <span class="btn  btn-info">Choose Files*</span>
                      <input class="file-input" type="file" name="image" id="image"  onchange="loadImageFile();" accept="image/*"/>
                      <label for="Picture1">No file Chosen</label>
              </div>


    </div>


    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-photo.png')}}" alt="" /></i></span>
            <input id="Picture2" type="text" class="form-control" name="image_name2" value="{{ $model->image_name2 }}" placeholder="Picture#2">
      </div>

            <div class="file-drop-area fnc-uplaod">
                      <span class="btn btn-info">Choose Files</span>
                      <input class="file-input" type="file" name="image2" id="image2" onchange="loadImageFile2();" accept="image/*"/>
                      <label for="Picture2">No file Chosen</label>
           </div>


    </div>


    <div class="form-group col-sm-12">
      <div class="input-group">
            <span class="input-group-addon"><i class="icon"><img src="{{ url('front/images/icon-paper2.png')}}" alt="" /></i></span>
            <input id="document_name" type="text" class="form-control" name="document_name" value="{{ $model->document_other }}" placeholder="Document or Other">
      </div>

              <div class="file-drop-area fnc-uplaod">
                      <span class="btn btn-info">Choose Files</span>
                      <input class="file-input" type="file" name="document" id="document" >
                      <label for="DocumentorOther">No file Chosen</label>
              </div>

    </div>
    <div class="col-sm-12 text-center data-print_box" >     
        <h4>Draw your signature*</h4>
        
        <div class='clearfix'></div>
        <br/>
        <div id="btn_sig" data-toggle="modal" data-target="#signatureModal" >
        <img id="signature_user_display" src="<?php echo ($model->signature=="")?asset('front/images/sign.png'):asset('/'.$model->signature);?>" alt="signature">  
        </div>
    </div>
    <!-- Modal signature -->
     <!-- Modal -->
     <div id="signatureModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

               <!-- Modal content-->
               <div class="modal-content">
                    <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                         
                    </div>
                    <div class="modal-body">

                         <div id="content">
                              <div class="hdr__cont__sign black">
                                   <h3>Add Signature</h3>
                                   <p>Your signature is required below.</p>
                              </div>
                              <div id="signatureparent">
                                   <div id="signature"></div>
                              </div>
                         </div>
                    </div>

                    <div class="modal-footer">
                         <div class="sign-buttons">
                              <div class="sign-btns-left">
                                   <button onclick="$('#signature').jSignature('clear');" class="btn btn-primary" type="button">CLEAR SIGN</button>
                                   <button data-toggle="modal" data-dismiss="modal"  class="btn btn-info"   type="button">Close</button>
                              </div>
                         </div>
                    </div>
               </div>

          </div>
     </div>

     <!--signature ends-->    

    <div class="col-sm-12 text-right">
        <button type="submit" class="btn btn-primary ">Update</button>
        <a href="{{ url('association/rfp') }}" class="btn btn-info">Cancel</a>
    </div>
        

    {!! Form::close() !!}
    </div>


    </div>

    </div>

    </div>
</section>
<script>
	$(document).ready(function(){
                //var array = ["2017-07-14","2017-07-15","2017-07-16"]

                
		var start_date=$('input[name="submission_date"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		start_date.datepicker({
                        
			format: 'yyyy-mm-dd',
                        startDate: new Date(),
			container: container,
			todayHighlight: true,
			autoclose: true,
			active:true
                      

		})
           $("#submission_date").keydown(function(event) { 
                return false;
            });     

	})
</script>
@include('front.association.common.signature')
@endsection