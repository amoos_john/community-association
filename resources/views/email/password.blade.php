<!DOCTYPE html>
<html >
<head>
<meta charset="UTF-8">
<title>Password Reset</title>
  
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"> </script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"> </script>
<style>
.logo_template{
  margin: 0 auto;
  display: block;
  width: 120px;
  margin-top: 10px;
}
.logo_template img{
width: 100%;}
p{
color:#222;
    font-family: arial,sans-serif;
    font-size: 14px;
}
*{   font-family: arial,sans-serif;}
.img_box
{
  width: 200px;
  margin:  0 auto;
  display: block;
}
.img_box img{
  width: 100%;
}
h1{
      font-size: 1.5em;
    -webkit-margin-before: 0.83em;
    -webkit-margin-after: 0.83em;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    font-weight: bold;
}
h1 a{
  color:#248ccc;
}
</style>
</head>

<body>
<div class="container">
    <h1>Hi There,</h1>
    <p>You have selected to reset your password. You may set your password by clicking on the link below. The link is valid for 15 minutes from the moment this email is sent. After the 15 minutes expire the link will become inactive and you will have to start the process again. Once you have clicked on the link you will be redirected to our reset password screen where you can successfully create your new password.</p>
    <p>Click here to reset your password: <a href="{{ url('password/reset/'.$token) }}">Click here</a>
        If the link does not work properly, please copy and paste the URL below on your browser: <br/>
        {{ url('password/reset/'.$token) }}    
    </p>

<br><br>


Best Regards,
<br><br>
<strong>Community Association Resources</strong>
</p>
  <div>
</body>


</html>

