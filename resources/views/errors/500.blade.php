@extends('front')

@section('content')

<section class="slider-area slider-home hover-ctrl  text-center" >
    <h1>Sorry</h1>
    <h4>We Couldn't find that page</h4>
    <h4>Go back to <a href="{{ url('/') }}">home page</a></h4>
</section>
	
@endsection