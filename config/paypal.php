<?php
return [
    'express'=>[
        'client_id'=>'AUkn6x9RrNchvUxhkAke0fFlx-AFNYgOYEQG-SViry1lNnqYVgMfU6nq9z7HXwpxvB_NGgbxRnesx48q',
        'secret'=>'EEgVqwWfQeq0GlGVFMVtGh3sm4bJ1vL2zkJB-hbMIzuieeLV47MFv2axLZ4j9uAr5pVdR52sq_Afe90E',
        'success'=>'Payments\PaypalController@success',
        'cancel'=>'Payments\PaypalController@cancel',
        'config'=>
        [
            'mode' => 'sandbox',
            'service.EndPoint' => 'https://api.sandbox.paypal.com',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
        ]
    ]
];