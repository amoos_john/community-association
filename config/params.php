<?php
return [
    'site_name' => 'Community Association Resources',
    'image_path' => 'uploads/images/',
     'symbol'=>'$',
	'currency'=>[
            'USD'=>['symbol'=>'$','name'=>'Dollar'],
    ],
    'currency_default'=>'USB',
    'languages'=>[
            'en_uk'=>'English (UK)',
            'en_us'=>'English (Us)',
    ],
    'language_default'=>'en_uk',
    'contentTypes'=>[
            'page'=>'Page',
            'email'=>'Email',
            'block'=>'Block',
    ],
    'packages'=>[
            '1'=>['name'=>'GOLD PACKAGE','price'=>'400','image'=>'package-icon1.png'],
            '2'=>['name'=>'SILVER PACKAGE','price'=>'300','image'=>'package-icon2.png'],
            '3'=>['name'=>'BRONZE PACKAGE','price'=>'200','image'=>'package-icon3.png'],
    ],
    'admin_email'=>'amoos@golpik.com',
    'server_email'=>'amoos@golpik.com'
    
];