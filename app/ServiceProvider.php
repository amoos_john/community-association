<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class ServiceProvider extends Model {
    
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	
    protected $table='service_provider';
    
    public function packages()
    {
        return $this->hasOne('App\Packages', 'id', 'package_id');
    }
    public function cities()
    {
        return $this->hasOne('App\Cities', 'id', 'city');
    }
    public function package_combination()
    {
        return $this->hasMany('App\PackageCombination', 'user_id', 'user_id');
    }
    public function users()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function gallery_package()
    {
        return $this->hasOne('App\Packages', 'id', 'gallerypackage_id');
    }
    public static function getServiceProviderId($user_id)
    {
        $result = ServiceProvider::where("user_id","=",$user_id)->first();

        $serviceprovider_id = $result->id;
        
        return $serviceprovider_id;
                
    }
    public static function getServiceProvider()
    {
        //>where("deleted","!=",1)
        $result = ServiceProvider::where("status","=",1)->orderby('name','asc')->get();

        return $result;
                
    }
    public static function search($search='')
    {
        $result =  ServiceProvider::join('users','users.id','=','service_provider.user_id')
                   ->select('service_provider.*','users.email','users.created_at');
                
        if(isset($search['name']) && $search['name']!="") 
        {
            $result = $result->where('service_provider.name','LIKE',"%".$search['name']."%");
        }
        if(isset($search['email']) && $search['email']!="") 
        {
            $result = $result->where('users.email','=',$search['email']);
        }
        if(isset($search['status']) && $search['status']!="") 
        {
            $result = $result->where('service_provider.status','=',$search['status']);
        }
        
        $result = $result->orderBy("id","desc");
        $result = $result->paginate(10);
        return $result;
    }
    public static function allServiceProvider()
    {
        $result=ServiceProvider::orderby("name","ASC")->where("status","=",1)->lists("name", "id")->prepend('Select', '');

        return $result;
                
    }
    public static function searchVendor($search='')
    {
        $result =  ServiceProvider::with("cities")->where("status","=",1);
                
        if(isset($search['search']) && $search['search']!="") 
        {
            $result = $result->where('name','LIKE',"%".$search['search']."%");
        }
        if(isset($search['per_page']) && $search['per_page']!="") 
        {
             $per_page = $search['per_page'];
        }
        else
        {
            $per_page = 10;
        }
        
        $result = $result->where("status","=",1)->orderBy("name","asc");
        $result = $result->paginate($per_page);
        return $result;
    }
     
}
