<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageCombination extends Model {
	
    protected $table='package_combination';
    
    public function billing()
    {
        return $this->hasOne('App\Billing', 'id', 'billing_id');
    }
   
}