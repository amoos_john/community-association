<?php

namespace App\Functions;

use Intervention\Image\Facades\Image as Image;

class Functions {

    public static function prettyJson($inputArray, $statusCode) {
        return response()->json($inputArray, $statusCode, array('Content-Type' => 'application/json'), JSON_PRETTY_PRINT);
    }

    public static function saveImage($file, $destinationPath, $destinationPathThumb = '') {
        $extension = $file->getClientOriginalExtension();
        //dd($extension);
        $fileName = rand(111, 999) . time() . '.' . $extension;
        $image = $destinationPath . '/' . $fileName;
        $upload_success = $file->move($destinationPath, $fileName);
        //Functions::saveThumbImage($image,'fit',$destinationPath.$fileName);
        return $fileName;
    }

    public static function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public static function moneyFormat($price)
    {
        $price=number_format($price,2);
        return $price;
    }
    public static function dateFormat($date)
    {
        $dates=date("d/m/Y",strtotime($date));
        return $dates;
    }
    public static function setEmailTemplate($contentModel, $replaces) {
        $data['body'] = $contentModel[0]->body;
        $data['subject'] = $contentModel[0]->subject;
        $data['title'] = $contentModel[0]->title;
        foreach ($replaces as $key => $replace) {
            $data['body'] = str_replace("%%" . $key . "%%", $replace, $data['body']);
        }

        return $data;
    }
    public static function sendEmail($email,$subject,$body,$header='',$from,$cc="",$bcc="")
    {
        // To send HTML mail, the Content-type header must be set
        if($header=='')
        {
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: <'.$from.'>' . "\r\n";
            //$headers .= 'To: '.$email.'' . "\r\n";
           
            if($cc!=""){
                $headers .= 'Cc: '.$cc. "\r\n";
            }
            if($bcc!=""){
                $headers .= 'Bcc: '.$bcc. "\r\n";
            }
        }                  
        //$body = "<title></title><style></style></head><body>".$body."</body></html>";
        return mail($email,$subject,$body,$headers);     
    }
    public static function dateFrontFormat($date)
    {
        $dates=date("d-m-Y",strtotime($date));
        return $dates;
    }
    public static function dateTimeFormat($date)
    {
        $dates=date("d/m/Y H:i A",strtotime($date));
        //$dates=date_format($date,"d/m/Y H:i A");
        return $dates;
    }
    public static function getMonths()
    {
        $months = array('01' => 'January', '02' => 'Febuary', '03' => 'March', 
            '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', 
            '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
        
        return $months;
    }

}
