<?php
namespace App\Functions;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use DateTime;
//date_default_timezone_set('America/Los_Angeles');
  
define("AUTHORIZENET_LOG_FILE", "phplog");
class AuthorizeNet {

public static function createSubscription($data)
{
    /* Create a merchantAuthenticationType object with authentication details
       retrieved from the constants file */
    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
    $merchantAuthentication->setName(env('MERCHANT_LOGIN_ID'));
    $merchantAuthentication->setTransactionKey(env('MERCHANT_TRANSACTION_KEY'));
    
    // Set the transaction's refId
    $refId = 'ref' . time();
    $date = date("Y-m-d");

    // Subscription Type Info
    $subscription = new AnetAPI\ARBSubscriptionType();
    $subscription->setName($data["package_name"]);

    $interval = new AnetAPI\PaymentScheduleType\IntervalAType();
    $interval->setLength("1");
    $interval->setUnit("months");

    $paymentSchedule = new AnetAPI\PaymentScheduleType();
    $paymentSchedule->setInterval($interval);
    $paymentSchedule->setStartDate(new DateTime($date));
    $paymentSchedule->setTotalOccurrences("12");
    $paymentSchedule->setTrialOccurrences("1");

    $subscription->setPaymentSchedule($paymentSchedule);
    $subscription->setAmount($data["total"]);
    $subscription->setTrialAmount("0.00");
    
    $creditCard = new AnetAPI\CreditCardType();
    $creditCard->setCardNumber($data["card_no"]);
    $creditCard->setExpirationDate($data["year"]."-".$data["month"]);

    $payment = new AnetAPI\PaymentType();
    $payment->setCreditCard($creditCard);
    $subscription->setPayment($payment);

    $order = new AnetAPI\OrderType();
    $order->setInvoiceNumber("1234354");        
    $order->setDescription($data["description"]); 
    $subscription->setOrder($order);
    
    $billTo = new AnetAPI\NameAndAddressType();
    $billTo->setFirstName($data["card_name"]);
    $billTo->setLastName("Smith");

    $subscription->setBillTo($billTo);

    $request = new AnetAPI\ARBCreateSubscriptionRequest();
    $request->setmerchantAuthentication($merchantAuthentication);
    $request->setRefId($refId);
    $request->setSubscription($subscription);
    $controller = new AnetController\ARBCreateSubscriptionController($request);

    $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
    
    
    return $response;
  }

} 

?>