<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageCart extends Model {
	
    protected $table='package_cart';
    
    public function package()
    {
        return $this->hasOne('App\Packages', 'id', 'package_id');
    }
   
}
