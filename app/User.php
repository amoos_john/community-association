<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public static $status=[""=>"Select Status","1"=>"Active","0"=>"Inactive"];

     public function role()
    {
        return $this->hasOne('App\Role', 'id', 'role_id');
    }
    
    
    
    public static function search($search)
    {
        $result =  User::where("role_id","=",3);
                
        if(isset($search['account_name']) && $search['account_name']!="") 
        {
            $result = $result->where('name','LIKE',"%".$search['account_name']."%");
        }
        if(isset($search['status']) && $search['status']!="") 
        {
            $result = $result->where('status','=',$search['status']);
        }
        
        $result = $result->orderBy("name","asc");
        $result = $result->paginate(10);
        return $result;
    }
    					
   

}
