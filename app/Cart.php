<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model {
	//
    protected $table='cart';
    
    public  function getStates()
    {
        return $this->hasOne('App\States', 'state_code', 'states');
    }
}
