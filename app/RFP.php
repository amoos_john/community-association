<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class RFP extends Model {
	
    protected $table='rfp';
    
    public static $status=["1"=>"Success","0"=>"Pending","2"=>"Expired"];
    
    public function association()
    {
        return $this->hasOne('App\Association', 'id', 'association_id');
    }
    
    public function rfp_vendors()
    {
        return $this->hasMany('App\RFPVendors', 'rfp_id', 'id');
    }
    public static function updateSignature($params = array())
    {
        $update = RFP::where("id","=",$params["rfp_id"])
                ->update(array("signature"=>$params["sign"],
                    "updated_at"=>$params["updated_at"]));
        
        
        
        return $update;
    }
    
}
