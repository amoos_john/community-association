<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Cities extends Model {
	
    protected $table='cities';
    
    public static function getStates()
    {
        $result = DB::table("states")
                ->orderby("state","ASC")->get();
        
        return $result;
        /*$result = DB::table("states as st")
                ->join("cities as cit","st.state_code","=","cit.state_code")
                ->select("st.*","cit.county")
                ->groupby("st.state_code")
                ->orderby("st.state","ASC")->get();*/
        
        
    }
    
    public static function getCities()
    {
        $result = DB::table("cities")
                ->groupby("city")->orderby("city","ASC")
                ->limit(300)->get();
        
        return $result;
    }
    public static function getCounties($state_code)
    {
      
        
      $result = DB::table("cities")
        ->where("county","!=","") 
        ->where("state_code","=",$state_code)
         ->groupby("county")
        ->orderby("county","ASC")
        ->get();
      //
        
        return $result;
    }
    public static function getAllCities()
    {
        $result = Cities::orderby("city","ASC")
                ->groupby("city")->orderby("city","ASC")
                ->limit(300)->lists("city", "id")->prepend('Select', '');
        
        return $result;
    }
    public static function getStateCities($state_code)
    {
        $result = Cities::where("state_code","=",$state_code)
                ->groupby("city")->orderby("city","ASC")
                 ->get();
        
        return $result;
    }
    
}
