<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RFPVendors extends Model {
	
    protected $table='rfp_vendors';
    
    public static $status=["1"=>"Submitted","0"=>"Pending","2"=>"Expired"];
    
    public function serviceprovider()
    {
        return $this->hasOne('App\ServiceProvider', 'id', 'serviceprovider_id');
    }
    public static function getRfpVendor($id)
    {
        $result = RFPVendors::join("service_provider as pro","pro.id","=","rfp_vendors.serviceprovider_id")
                  ->join("rfp","rfp.id","=","rfp_vendors.rfp_id")
                  ->join("association as assoc","assoc.id","=","rfp.association_id")
                  ->select("rfp_vendors.*","rfp.title","rfp.submission_date","assoc.name")
                  ->where("rfp.status","=",1)  
                  ->where("rfp_vendors.serviceprovider_id","=",$id)
                  ->orderby("rfp_vendors.id","DESC")  
                  ->paginate(10);
        return $result;
    }
    
}
