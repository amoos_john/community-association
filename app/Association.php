<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AssociationSubscriptions;

class Association extends Model {
	
    protected $table='association';
    
    public function users()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public  function packages()
    {
        return $this->hasOne('App\Packages', 'id', 'package_id');
    }
    
    public static function totalRfps($user_id)
    {
        //$result = Association::where("user_id","=",$user_id)->first();
        $result = AssociationSubscriptions::where("subscription_status", "=", 1)
                ->where("user_id", "=", $user_id)
                ->where("subscription_date", ">=", date('Y-m-d'))
                ->where("deleted", "!=", 1)
                ->where("total_rfp", "!=", 0)
                ->orderby("id", "desc")
                ->first();
        
        return $result;
                
    }
    
    public static function getAssociationId($user_id)
    {
        $accociation = Association::where("user_id","=",$user_id)->first();

        $association_id = $accociation->id;
        
        return $association_id;
                
    }
    public static function search($search='')
    {
        $result =  Association::join('users','users.id','=','association.user_id')
                   ->select('association.*','users.email','users.created_at');
                
        if(isset($search['name']) && $search['name']!="") 
        {
            $result = $result->where('association.name','LIKE',"%".$search['name']."%");
        }
        if(isset($search['email']) && $search['email']!="") 
        {
            $result = $result->where('users.email','=',$search['email']);
        }
        if(isset($search['status']) && $search['status']!="") 
        {
            $result = $result->where('association.status','=',$search['status']);
        }
        
        $result = $result->orderBy("id","desc");
        $result = $result->paginate(10);
        return $result;
    }
    public static function allAssociation()
    {
        $result=Association::orderby("name","ASC")->where("status","=",1)->lists("name", "id")->prepend('Select', '');

        return $result;
                
    }
    
}
