<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class AdditionalUser extends Model {
	
    protected $table='additional_user';
    
    public function association()
    {
        return $this->hasOne('App\Association', 'id', 'association_id');
    }
    
    public function service_provider()
    {
        return $this->hasOne('App\ServiceProvider', 'id', 'serviceprovider_id');
    }
    
    public function modified()
    {
        return $this->hasOne('App\User', 'id', 'modified_by');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    
    public static function search($search)
    {
        $result = AdditionalUser::with("modified")->with("user");
                
        if(isset($search['first_name']) && $search['first_name']!="") 
        {
            $result = $result->where('first_name','LIKE',"%".$search['first_name']);
        }
        if(isset($search['last_name']) && $search['last_name']!="") 
        {
            $result = $result->where('last_name','LIKE',"%".$search['last_name']);
        }
        if(isset($search['status']) && $search['status']!="") 
        {
            $result = $result->where('status','=',$search['status']);
        }
        
        if(isset($search['association_id']))
        {
            $result = $result->where('association_id','=',$search['association_id']);
        }
        elseif(isset($search['serviceprovider_id']))
        {
            $result = $result->where('serviceprovider_id','=',$search['serviceprovider_id']);
        }
       
        $result = $result->paginate(20);
        return $result;
    }
    public static function searchUsers($search)
    {
        $result = AdditionalUser::with('user')->with('modified');
                
        if(isset($search['association_id']))
        {
            $result = $result->with('association');
        }
        elseif(isset($search['serviceprovider_id']))
        {
            $result = $result->with('service_provider');
        }        
        if(isset($search['first_name']) && $search['first_name']!="") 
        {
            $result = $result->where('first_name','LIKE',"%".$search['first_name']."%");
        }
        if(isset($search['last_name']) && $search['last_name']!="") 
        {
            $result = $result->where('last_name','LIKE',"%".$search['last_name']."%");
        }
        if(isset($search['status']) && $search['status']!="") 
        {
            $result = $result->where('status','=',$search['status']);
        }
        if(isset($search['association_id']) && $search['association_id']!="")
        {
            $result = $result->where('association_id','=',$search['association_id'])
                    ->where('association_id','!=',0);
        }
        elseif(isset($search['serviceprovider_id']) && $search['serviceprovider_id']!="")
        {
            $result = $result->where('serviceprovider_id','=',$search['serviceprovider_id']);
        }
        
        if(isset($search['association_id']))
        {
            $result = $result->where('association_id','!=',0);
        }
        elseif(isset($search['serviceprovider_id']))
        {
            $result = $result->where('serviceprovider_id','!=',0);
        }
       
        $result = $result->where('user_id','!=',0)->orderby('id','desc')->paginate(20);
        return $result;
    }
}
