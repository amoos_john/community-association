<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorSubscriptions extends Model {

    protected $table = 'vendor_subscriptions';

    public function users() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function packages() {
        return $this->hasOne('App\Packages', 'id', 'package_id');
    }

    public static function getSubcription($user_id) {
        $assoc = VendorSubscriptions::where("subscription_status", "=", 1)
                ->where("user_id", "=", $user_id)
                ->where("subscription_date", ">=", date('Y-m-d'))
                ->where("deleted", "!=", 1)
                ->orderby("id", "desc")
                ->first();
        return $assoc;
    }
    public static function getGallerySubcription($user_id) {
        $assoc = VendorSubscriptions::where("subscription_status", "=", 1)
                ->where("type", "=", 2)
                ->where("user_id", "=", $user_id)
                ->where("subscription_date", ">=", date('Y-m-d'))
                ->where("deleted", "!=", 1)
                ->orderby("id", "desc")
                ->first();
        return $assoc;
    }

}
