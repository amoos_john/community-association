<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Packages extends Model {
	
    protected $table='packages';
    
    public static function getAssociatePackage()
    {
        $result = Packages::where("type","=",1)->get();
        
        return $result;
    }
    public static function getServiceProviderPackage()
    {
        $result = Packages::where("type","=",2)->get();
        
        return $result;
    }
    
    
}
