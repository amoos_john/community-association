<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model {
	
    protected $table='gallery';
    
    public static function  searchGallery($search)
    {
        $result =  Gallery::join('package_combination','package_combination.id','=','gallery.packagecombine_id')
                   ->join('service_provider','service_provider.user_id','=','gallery.created_by') 
                   ->select('gallery.*','service_provider.name','service_provider.id as serviceprovider_id');
        
        if(isset($search['areas']) && $search['areas']!="") 
        {
            $get_areas = implode(",", $search['areas']);
            $get_area = explode(",", $get_areas);
            
            foreach($get_area as $areas)
            {
               $result = $result->whereRaw("FIND_IN_SET('".$areas."',package_combination.service_areas)");

            }
            //$result = $result->whereIn('package_combination.service_areas',$areas);
        }
        if(isset($search['states']) && $search['states']!="") 
        {
            /*$get_states = implode(",", $search['states']);
            $get_state = explode(",", $get_states);
            
            $states = array_map('intval', $get_state);*/
            $result = $result->where('package_combination.states','=',$search['states']);
        }
        if(isset($search['county']) && $search['county']!="") 
        {
            $get_county = implode(",", $search['county']);
            $counties = explode(",", $get_county);
           
            foreach($counties as $county)
            {
               $result = $result->whereRaw("FIND_IN_SET('".$county."',package_combination.county)");

            }
        }
        
        $result = $result->where('gallery.status','=',1)->orderBy("gallery.id","desc");
        $result = $result->paginate(10);
        return $result;
    }
}
