<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssociationSubscriptions extends Model {

    protected $table = 'association_subscriptions';

    public function users() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function packages() {
        return $this->hasOne('App\Packages', 'id', 'package_id');
    }

    public static function getSubcription($user_id) {
        $assoc = AssociationSubscriptions::where("subscription_status", "=", 1)
                ->where("user_id", "=", $user_id)
                ->where("subscription_date", ">=", date('Y-m-d'))
                ->where("deleted", "!=", 1)
                ->where("total_rfp", "!=", 0)
                ->orderby("id", "desc")
                ->first();
        return $assoc;
    }

}
