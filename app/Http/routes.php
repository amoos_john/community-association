<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'HomeController@index');
Route::get('thank-you', 'HomeController@thankYou');
Route::get('payment/thank-you', 'Payments\PaymentController@index');
Route::get('payment/cancel', 'Payments\PaymentController@cancel');

Route::get('register', 'HomeController@register');
Route::get('login', 'RegisterController@index');
Route::post('postLogin', 'RegisterController@postLogin');
Route::get('logout', 'RegisterController@logout');
Route::get('forgot','HomeController@forgotPassword');
Route::post('forgot/password','HomeController@emailPassword');
Route::get('user/dashboard','DashboardController@indexUser');
Route::get('get/cities','HomeController@getCities');
Route::get('gallery','HomeController@gallery');
Route::get('gallery/result','HomeController@result');
Route::get('gallery/profile/{id}','HomeController@viewProfile');

Route::get('404', function()
{
    return view('errors.404', ['page_title' => 'Page not found!']);
});
Route::get('500', function()
{
    return view('errors.500', ['page_title' => 'Page not found!']);
});
Route::resource('items', 'ItemController');
/* Association */
Route::group(
    array('prefix' => 'association'), 
    function() {
        $assoc = "Association";
        
    Route::get('register', $assoc.'RegisterController@register');
    Route::post('insert', $assoc.'RegisterController@insert');
    Route::get('profile', $assoc.'RegisterController@profile');
    Route::post('profile/insert', $assoc.'RegisterController@profileInsert');
    Route::get('additional-user', $assoc.'RegisterController@additionalUser');
    Route::post('additional/insert', $assoc.'RegisterController@additionalInsert');
    Route::get('packages', $assoc.'RegisterController@packages');
    Route::get('billing/{package_id}', 'BillingController@index');
    Route::post('billing/insert/{package_id}', 'BillingController@insert');
    Route::post('billing/checkout', 'BillingController@order');
    
    Route::get('dashboard','DashboardController@index');
    Route::get('rfp','RFPController@index');
    Route::get('rfp/create','RFPController@create');
    Route::post('rfp/insert','RFPController@insert');
    Route::get('rfp/vendor/{id}','RFPController@rfpVendors');
    Route::post('rfp/vendor/insert/{id}','RFPController@rfpVendorsInsert');
    Route::get('rfp/edit/{id}','RFPController@edit');
    Route::post('rfp/update/{id}','RFPController@update');
    Route::get('rfp/delete/{id}','RFPController@delete');
    Route::post('rfp/store-signature', 'RFPController@storeSignature');
    Route::get('rfp/view/{id}','RFPController@view');
    Route::get('rfp/vendors/search','RFPController@searchVendor');
    
    Route::get('jobs', 'JobsController@index');
    Route::get('jobs/create', 'JobsController@create');
    Route::post('jobs/insert', 'JobsController@insert');
    Route::get('jobs/edit/{id}', 'JobsController@edit');
    Route::post('jobs/update/{id}','JobsController@update');
    Route::get('jobs/delete/{id}','JobsController@delete');
    Route::get('jobs/view/{id}','JobsController@view');
    
    Route::get('users', $assoc.'UserController@index');
    Route::get('users/view/{id}', $assoc.'UserController@view');
    Route::get('users/search', $assoc.'UserController@search');
    Route::get('users/create', $assoc.'UserController@create');
    Route::post('users/insert', $assoc.'UserController@insert');
    Route::get('users/edit/{id}', $assoc.'UserController@edit');
    Route::post('users/update/{id}',$assoc.'UserController@update');
    Route::delete('users/delete/{id}',$assoc.'UserController@delete');
    Route::get('users/roles', $assoc.'UserController@roles');
    Route::post('users/roles/create', $assoc.'UserController@createRoles');
    Route::get('users/{code}/delete', $assoc.'UserController@deleteRoles');
    Route::get('users/email/{id}', $assoc.'UserController@registerEmail');
    
    Route::get('profile/edit', 'ProfileController@edit');
    Route::post('profile/update/{id}','ProfileController@update');
    
    Route::get('upgrade', 'PackagesController@index');
    
      
    }
);
/* Vendor */
Route::group(
    array('prefix' => 'vendor'), 
    function() {
   $service = "ServiceProvider";
        
    Route::get('register', $service.'Controller@index');
    Route::post('insert', $service.'Controller@insert');
    Route::get('profile', $service.'Controller@profile');
    Route::post('profile/insert', $service.'Controller@profileInsert');
  
    Route::get('packages', $service.'Controller@packages');
    Route::get('package/{package_id}', $service.'Controller@packages');
    Route::get('get-counties', $service.'Controller@getCounties');
    Route::post('create/package', $service.'Controller@createPackage');
    Route::get('cart/{package_id}', $service.'Controller@getCart');
    Route::post('package/insert/{package_id}', $service.'Controller@insertPackage');
   
    Route::get('billing', 'BillingController@indexPackage');
    Route::get('billing/{code}/{package_id}', 'BillingController@indexGallery');
    Route::post('billing/insert/{package_id}', 'BillingController@insertPackage');
        
    Route::get('dashboard','DashboardController@indexService');
    Route::get('promotional', 'PromotionalController@index');
    Route::get('promotional/create', 'PromotionalController@create');
    Route::post('promotional/insert', 'PromotionalController@insert');
    Route::get('promotional/edit/{id}', 'PromotionalController@edit');
    Route::post('promotional/update/{id}','PromotionalController@update');
    Route::get('promotional/delete/{id}','PromotionalController@delete');
    Route::get('promotional/view/{id}','PromotionalController@view');

    Route::get('jobs', 'JobBidsController@index');
    Route::get('jobs/view/{id}','JobBidsController@view');
    Route::post('jobs/reply', 'JobBidsController@reply');

    Route::get('jobs/edit/{id}', 'JobBidsController@edit');
    Route::post('jobs/update/{id}','JobBidsController@update');
    Route::get('jobs/delete/{id}','JobBidsController@delete');

    Route::get('gallery','GalleryController@index');
    Route::get('gallery/create','GalleryController@create');
    Route::post('gallery/insert','GalleryController@insert');

    Route::get('rfp', 'RFPController@indexRfp');
    Route::get('rfp/view/{id}','RFPController@viewRfp');
    Route::post('rfp/insert/{id}', 'RFPController@insertRfp');
    Route::get('rfp/delete/{id}','RFPController@deleteRfp');

    Route::get('profile/edit', 'ProfileController@editProvider');
    Route::post('profile/update/{id}','ProfileController@updateProvider');
    
    Route::get('users', 'VendorUserController@index');
    Route::get('users/view/{id}', 'VendorUserController@view');
    Route::get('users/search', 'VendorUserController@search');
    Route::get('users/create', 'VendorUserController@create');
    Route::post('users/insert', 'VendorUserController@insert');
    Route::get('users/edit/{id}', 'VendorUserController@edit');
    Route::post('users/update/{id}','VendorUserController@update');
    Route::get('users/roles', 'VendorUserController@roles');
    Route::post('users/roles/create', 'VendorUserController@createRoles');
    Route::get('users/{code}/delete', 'VendorUserController@deleteRoles');
    Route::get('users/email/{id}','VendorUserController@registerEmail');

    Route::get('upgrade', 'PackagesController@indexVendor');
       
      
    }
);
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::controllers(['auth' => 'Auth\AuthController', 'password' => 'Auth\PasswordController']);



Route::group(
    array('prefix' => 'admin'), 
    function() {
        $admin= "Admin\\";
       
    Route::get('/', $admin . 'HomeController@index');

    Route::get('/login', $admin . 'UserController@getLogin');

    Route::post('/login', $admin . 'UserController@postLogin');

    Route::get('get/cities', $admin . 'HomeController@getCities');
    Route::get('users', $admin . 'UsersController@index');
    Route::get('users/create', $admin . 'UsersController@create');
    Route::post('users/insert', $admin . 'UsersController@insert');
    Route::get('users/edit/{id}', $admin . 'UsersController@edit');
    Route::post('users/update/{id}', $admin . 'UsersController@update');
    Route::get('users/delete/{id}', $admin . 'UsersController@delete');

    Route::get('vendors', $admin . 'VendorsController@index');
    Route::get('vendors/search', $admin . 'VendorsController@search');
    Route::get('vendors/edit/{id}', $admin . 'VendorsController@edit');
    Route::post('vendors/update/{id}', $admin . 'VendorsController@update');
    Route::get('vendors/delete/{id}', $admin . 'VendorsController@delete');
    Route::get('vendors/view/{id}', $admin . 'VendorsController@view');
    Route::get('vendors/users', $admin . 'VendorsController@indexUsers');
    Route::get('vendors/users/search', $admin . 'VendorsController@searchUsers');
    Route::get('vendors/users/delete/{id}', $admin . 'VendorsController@deleteUser');
    Route::get('vendors/users/status/{id}', $admin . 'VendorsController@statusUser');

    
    Route::get('association', $admin . 'AssociationController@index');
    Route::get('association/search', $admin . 'AssociationController@search');
    Route::get('association/edit/{id}', $admin . 'AssociationController@edit');
    Route::post('association/update/{id}', $admin . 'AssociationController@update');
    Route::get('association/delete/{id}', $admin . 'AssociationController@delete');
    Route::get('association/view/{id}', $admin . 'AssociationController@view');
    Route::get('association/users', $admin . 'AssociationController@indexUsers');
    Route::get('association/users/search', $admin . 'AssociationController@searchUsers');
    Route::get('association/users/delete/{id}', $admin . 'AssociationController@deleteUser');
    Route::get('association/users/status/{id}', $admin . 'AssociationController@statusUser');
       
    }
);


