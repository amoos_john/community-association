<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator,
    Input,
    Redirect,
    Session,
    Config;
use App\Functions\Functions;
use App\Association;
use App\AdditionalUser;
use App\AssociationSubscriptions;
use App\Packages;
 
class DashboardController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | AssociationRegister Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   
    public function index() 
    {
        if(Auth::user()->role_id==2)
        {
            $page_title = "Association Dashboard";
            $user_id = Auth::user()->id;
            $model = Association::totalRfps($user_id);
            $total_rfps = 0;
            $created_rfps = 0;
            if(count($model)>0)
            {
                $package = Packages::find($model->package_id);
                $total_rfps = $model->total_rfp;
                $created_rfps = $model->total_rfp - $total_rfps;
            }
            
            return view('front.association.dashboard',compact("page_title","user_id","total_rfps","created_rfps"));
        }
        else 
        {
            
          return redirect('user/dashboard');

        }
        
    }
    public function indexService() 
    {
        if(Auth::user()->role_id==3)
        {
            $page_title = "Service Provider Dashboard";
        
            return view('front.serviceprovider.dashboard',compact("page_title"));
        }
        else 
        {
            
          return redirect('user/dashboard');

        }
       
    }
    public function indexUser() 
    {
        if(Auth::user()->role_id==4)
        {
            $page_title = "User Dashboard";
            $user_id = Auth::user()->id;
            $result = AdditionalUser::where("user_id","=",$user_id)->first();
          
            if($result->association_id!=0)
            {
                $user_id = $result->modified_by;
                $model = Association::totalRfps($user_id);
                $total_rfps = 0;
                $created_rfps = 0;
                if(count($model)>0)
                {
                    $package = Packages::find($model->package_id);
                    $total_rfps = $model->total_rfp;
                    $created_rfps = $package->total_rfp - $total_rfps;
                }
                
                return view('front.association.dashboard',compact("page_title","total_rfps","created_rfps"));
            }
            elseif($result->serviceprovider_id!=0)
            {
                return view('front.serviceprovider.dashboard',compact("page_title"));
            }
            
        }
        else 
        {
            
          return redirect('/');

        }
       
    }
    
    

    
}
