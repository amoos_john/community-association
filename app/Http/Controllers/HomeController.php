<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator,
    Input,
    Redirect;
use App\Functions\Functions;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use App\Cities;
use App\ServiceArea;
use App\Gallery;
use App\ServiceProvider;

class HomeController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    public function index() {

        return view('front.index');
    }
    public function register() {
        
        return view('front.register');
    }
    public function thankYou() {
        
        return view('front.thankyou');
    }
    public function forgotPassword()
    {
       return view('auth.password');
    }
    public function emailPassword(Request $request)
    {
       $credentials = ['email' => $request->email];
        $response = Password::sendResetLink($credentials, function (Message $message) {
            $message->subject('Your Password Reset Link');
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()->back()->with('success', trans($response));
            case Password::INVALID_USER:
                return redirect()->back()->withErrors(['email' => trans($response)]);
        }
    }
    public function getCities(Request $request)
    {
        $state_code = $request->state_code;
        $city = (isset($request->city))?$request->city:'';

        if ($state_code != "") {
            $cities = Cities::getStateCities($state_code);
            
            if (count($cities) > 0) {
                
                foreach ($cities as $row) {
                    $checked = '';
                    if($city==$row->id)
                    {
                        $checked = 'checked';
                    }
                    echo '<div class="form-group">
                    <input type="radio"  name="cities" value="' . $row->id . '" id="' . $row->city . '" ' .$checked. '/>
                    <mark></mark>
                    <label for="' . $row->city . '">'. $row->city . '</label>
                    </div>';
                }
            }
        }
    }
    public function gallery() {
        $page_title = "Gallery";
        $states = Cities::getStates();
        $areas = ServiceArea::where('status', '=', 1)
                 ->orderby("id", "desc")->limit(12)->get();
        
        return view('front.gallery',compact("page_title","areas", "states"));
    }
    public function result(Request $request)
    {
        $areas = $request->input('areas');
        $states = $request->input('states');
        $county = $request->input('counties');
        
        $search['areas'] = $areas;
        $search['states'] = $states;
        $search['county'] = $county;
        
        $model = Gallery::searchGallery($search);
       
        if ($request->ajax()) {
            return view('front.result',compact("model"))->render();  
        }
    }
    public function viewProfile($id)
    {
        //$id = $request->id;
        $model = ServiceProvider::find($id);
        
        return view('front.common.popup',compact("model"));
    }
    
}
