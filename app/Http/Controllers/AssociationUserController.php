<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Input as Input;
use Validator,
    Redirect,
    Session,
    Config,
    Mail;
use App\Functions\Functions;
use App\Association;
use App\AdditionalUser;
use App\Content;
use App\Category;

class AssociationUserController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | AdditionalUserController 
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */
    protected $session_id;
    public function __construct() {
        $this->middleware('auth');
        $this->session_id=session_id();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    public function index() 
    {
        $page_title = "User Search";
        
        $user_id = Auth::user()->id;
        $association_id = Association::getAssociationId($user_id);
        
        $model = AdditionalUser::with("modified")->with("user")
        ->where("association_id","=",$association_id)
        ->orderby("id","desc")->paginate(20);
        
        return view('front.association.users.index',compact("model" ,"page_title"));
    }
    public function search(Request $request)
    {
        $page_title = "User Search";
        
        $user_id = Auth::user()->id;
        
        $association_id = Association::getAssociationId($user_id);
        
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $status = $request->input('status');
       
        $search['first_name']=$first_name;
        $search['last_name']=$last_name;
        $search['status']=$status;
        $search['association_id']=$association_id;
       
        $model = AdditionalUser::search($search);
        return view('front.association.users.index', compact('model','page_title','first_name','last_name','status'));
    }
    public function create() 
    {
        $page_title = "Add User";
        
        $categories = Category::where("type","=",1)->get();
        
        $positions = DB::table('positions')->where("type","=",1)->get();
        
        $roles = DB::table('organization_roles')->where("type","=",1)->get();
        
        $sessionId = $this->session_id;
        $category_id = '';
        $position_id = '';
        $roles_id = '';
        
        $keys = "member".$sessionId;
        $data = Session::get($keys);
        if(count($data)>0)
        {
            $category_id = $data["category"];
            $position_id = $data["position"];    
        }
        
        $keys2 = "roles".$sessionId;
        $data2 = Session::get($keys2);  
        if(count($data2)>0)
        {
            $roles_id = $data2["role"];
        }
        
        return view('front.association.users.create',compact("page_title","category_id","position_id","roles_id","categories","positions","roles"));
    }
    
    public function insert(Request $request) 
    {
        $input = $request->all();
        $validation = array(
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|confirmed|email|max:255|unique:additional_user',
            );
        
        $validator = Validator::make($request->all(), $validation);
        
        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $sessionId = $this->session_id;
            $keys = "member".$sessionId;
            $data = Session::get($keys);
            $keys2 = "roles".$sessionId;
            $data2 = Session::get($keys2);  
            if(count($data)==0 || count($data2)==0)
            {
                return redirect()->back()->with("error","No roles added. Please add at least one role membership")
                        ->withInput();  
            }
            
            $user_id = Auth::user()->id;
            $association_id = Association::getAssociationId($user_id);
            
            $name = $request->first_name." ".$request->last_name;
            $email = $request->email;
            $password = Functions::generateRandomString();
            
            $user = new User;
            $user->name = $name;
            $user->email = $email;
            $user->password = bcrypt($password);
            $user->role_id = 4;
            $user->status = 1;
            $user->save();
            $userid = $user->id;
            
            $addit = new AdditionalUser;
            $addit->association_id = $association_id;
            $addit->user_id = $userid;
            $addit->category_id = $request->category;
            $addit->position_id = $request->position;
            $addit->roles_id = $request->roles;
            $addit->first_name = $request->first_name;   
            $addit->last_name = $request->last_name; 
            $addit->username = $request->username;
            $addit->email = $request->email;
            $addit->modified_by = $user_id;
            $addit->status = 1;
            $addit->save();
            
            $key = "member".$sessionId;
            Session::forget($key);
            $key = "roles".$sessionId;
            Session::forget($key);
            
            $from = Config::get("params.server_email");
            
            $content = Content::where("code","=","register")->get();
            
            $replaces['NAME'] = $name;
            $replaces['EMAIL'] = $email;
            $replaces['PASSWORD'] = $password;
            
            $template = Functions::setEmailTemplate($content, $replaces);
            
            $subject = $template["subject"];
            $body = $template["body"];
            
            /*$data= [
            'from' =>$from,
            'body' => $body,
            'subject' => $subject,
            'email_send' =>$email
                   
            ];

            Mail::send('email.register',['data' => $data], function($message) use ($data) {

             $message->from($data['from']);
             $message->to($data['email_send'])->subject($data['subject']);
            }); */
            $send_email = Functions::sendEmail($email,$subject,$body,'',$from);
            
            
            \Session::flash('success', 'Additional User Added Successfully!');
            return redirect("association/users");
        }
        
    }
    public function edit($id) 
    {
        $model = AdditionalUser::findOrFail($id); 
        $page_title = "Edit User";
        $categories = Category::where("type","=",1)->get();
        $positions = DB::table('positions')->where("type","=",1)->get();
        $roles = DB::table('organization_roles')->where("type","=",1)->get();
        
        if(count($model)>0)
        {
            $category_id = $model->category_id;
            $position_id = $model->position_id;
            $roles_id = $model->roles_id;
          
        }
        return view('front.association.users.edit', compact('id','model',"category_id","position_id","roles_id",'page_title',"categories","positions","roles"));
    }
    public function update($id, Request $request) 
    {
        $model = AdditionalUser::findOrFail($id);
        $input1 = $request->all();
        unset($input1['_token']);
        
        $validation = array(
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|confirmed|email|max:255|unique:additional_user,email,' . $model->id,
        );
        $validator = Validator::make($request->all(), $validation);
        
        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $user_id = Auth::user()->id;
            $input["category_id"] = $request->category;
            $input["position_id"] = $request->position;
            $input["roles_id"] = $request->roles;
            $input["first_name"] = $request->first_name;   
            $input["last_name"] = $request->last_name; 
            $input["username"] = $request->username;
            $input["email"] = $request->email;
            $input["modified_by"] = $user_id;
            $input["status"] = (isset($request->status))?1:0;
            
            $input_user["name"] = $request->first_name." ".$request->last_name;
            $input_user["email"] = $request->email;
          
            $update = AdditionalUser::where("id","=",$id)->update($input);
            $user_update = User::where("id","=",$model->user_id)->update($input_user);
            
            \Session::flash('success', 'Additional User updated successfully!');
            return redirect("association/users");
        }
       
        
    }
    public function roles(Request $request) 
    {
        $id = (isset($request->id))?$request->id:'';
        if($id!="")
        {
            $data = AdditionalUser::findOrFail($id);
            $categories = Category::find($data->category_id);
            $positions = DB::table('positions')->where("id","=",$data->position_id)->first();
            $roles = DB::table('organization_roles')->where("id","=",$data->roles_id)->first();
        }
        else
        {
            $sessionId = $this->session_id;

            $keys = "member".$sessionId;

            $data = Session::get($keys);

            if(count($data)>0)
            {
                $category_id = $data["category"];
                $position_id = $data["position"];

                $categories = Category::find($category_id);
                $positions = DB::table('positions')->where("id","=",$position_id)->first();

            }

            $keys2 = "roles".$sessionId;

            $data2 = Session::get($keys2);

            if(count($data2)>0)
            {
                $roles_id = $data2["role"];
                $roles = DB::table('organization_roles')->where("id","=",$roles_id)->first();
            }
        }
        
        return view('front.association.users.roles',compact("id","data","data2","categories","positions","roles"));
    }
    public function createRoles(Request $request) 
    {
        $sessionId = $this->session_id;
        $type = $request->type;
        
        if($type==1)
        {
            $validation = array(
            'category' => 'required',
            'position' => 'required',
            
            );
        }
        elseif($type==2)
        {
            $validation = array(
            'roles' => 'required',
            
            );
        }
        $response['error'] = 0;
        $validator = Validator::make($request->all(), $validation);
        
        if ($validator->fails()) {
            
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
          
        }
        else
        {
            if($type==1)
            {
                $item = [
                    'category' => $request->category,
                    'position' => $request->position,
                  ];
                
                $key = "member".$sessionId;
                Session::put($key,$item);
                Session::save();
            }
            elseif($type==2)
            {
                $item = [
                    'role' => $request->roles,
                  ];
                
                $key = "roles".$sessionId;
                Session::put($key,$item);
                Session::save();
            }
               
        }
        if($response['error']==1)
        {
            foreach($response['errors']->all() as $error)
            {
                echo '<p>'.$error.'</p>';
                
            }     
        }
        else 
        {
           echo '1';
        }
        
    }
    public function deleteRoles(Request $request) 
    {
        $code = $request->code;
        $sessionId = $this->session_id;
        $id = (isset($request->id))?$request->id:'';
        if($code=='member')
        {
            $key = "member".$sessionId;
            Session::forget($key);
            if($id!="")     
            {
               $update = AdditionalUser::where("id","=",$id)->update(["category_id"=>0,"position_id"=>0]);
               echo '1';
               exit;
            }
        }
        elseif($code=='roles')
        {
            $key = "roles".$sessionId;
            Session::forget($key);
            if($id!="")     
            {
               $update = AdditionalUser::where("id","=",$id)->update(["roles_id"=>0]);
               echo '1';
               exit;
            }
        }
        return redirect("association/users/create");
    }
    public function view($id)
    {
        $model = AdditionalUser::find($id);
        if(count($model)>0)
        {
            $categories = Category::find($model->category_id);
            $positions = DB::table('positions')->where("id","=",$model->position_id)->first();
            $roles = DB::table('organization_roles')->where("id","=",$model->roles_id)->first();
        }
        
        return view('front.association.users.view',compact("model","id","categories","positions","roles"));
    }
    public function registerEmail($id)
    {
        $model = AdditionalUser::find($id);
        $name = $model->first_name." ".$model->last_name;
        $email = $model->email;
        $password = "";
        $from = Config::get("params.server_email");
            
        $content = Content::where("code","=","register")->get();

        $replaces['NAME'] = $name;
        $replaces['EMAIL'] = $email;
        $replaces['PASSWORD'] = $password;

        $template = Functions::setEmailTemplate($content, $replaces);

        $subject = $template["subject"];
        $body = $template["body"];

        $send_email = Functions::sendEmail($email,$subject,$body,'',$from);


        \Session::flash('success', 'Registration email has been send successfully!');
        return redirect("association/users");
    }
    
}
