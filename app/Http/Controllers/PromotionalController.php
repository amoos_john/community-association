<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Input as Input;
use Validator,
    Redirect,
    Session,
    Config;
use App\Functions\Functions;
use App\ServiceProvider;
use App\Promotional;
use App\Cities;
use App\ServiceArea;
use App\Packages;
use App\PackageCombination;
use App\States;

class PromotionalController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Promotional Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index() {
        $page_title = "Promotional Announcement Status";

        $user_id = Auth::user()->id;

        $serviceprovider_id = ServiceProvider::getServiceProviderId($user_id);

        $model = Promotional::where("serviceprovider_id", "=", $serviceprovider_id)->where("deleted", "!=", 1)
                        ->orderby("id", "desc")->paginate(10);

        return view('front.serviceprovider.promotional.index', compact("model", "page_title"));
    }

    public function create() {
        $page_title = "Promotional Announcement";
        $user_id = Auth::user()->id;

        //$combin = PackageCombination::where("user_id","=",$user_id)->get();
        $serviceprovider_id = ServiceProvider::getServiceProviderId($user_id);

        $result = ServiceProvider::with("packages")->with("package_combination")->find($serviceprovider_id);
        if ($result) {
            $area = '';
            $state = array();
            foreach ($result->package_combination as $row) {
                $state[] = $row->states;
                //$county = $row->county;
                $area .= $row->service_areas;
            }
            $get_area = explode(",", $area);
            $ids = array_map('intval', $get_area);
        }

        $categories = ServiceArea::where('status', '=', 1)->whereIn("id", $ids)
                        ->orderby("name", "asc")->get();
        $states = States::whereIn("state_code", $state)->orderby("state", "asc")->get();

        return view('front.serviceprovider.promotional.create', compact("page_title", "states", "categories", "length"));
    }

    public function insert(Request $request) {
        $input = $request->all();
        $validation = array(
            'category' => 'required',
            'areas' => 'required',
            'title' => 'required|max:255',
            'contact_name' => 'required|max:255',
            'company_name' => 'required|max:255',
            'start_date' => 'required|date|max:20',
            'end_date' => 'required|date|max:20',
            'phone' => 'required|max:20',
        );

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $user_id = Auth::user()->id;

            $serviceprovider_id = ServiceProvider::getServiceProviderId($user_id);

            $category = "";
            if (isset($request->category)) {
                $category = implode(",", $request->category);
            }
            $areas = "";
            if (isset($request->areas)) {
                $areas = implode(",", $request->areas);
            }

            $promo = new Promotional;
            $promo->serviceprovider_id = $serviceprovider_id;
            $promo->user_id = $user_id;
            $promo->category = $category;
            $promo->areas = $areas;
            $promo->title = $request->title;
            $promo->contact_name = $request->contact_name;
            $promo->company_name = $request->company_name;
            $promo->phone = $request->phone;
            $promo->start_date = $request->start_date;
            $promo->end_date = $request->end_date;
            $promo->pkg_inclusion = $request->pkg_inclusion;
            $promo->comments = $request->comments;
            $promo->status = 1;
            $promo->save();

            \Session::flash('success', 'Promotional Added Successfully!');
            return redirect("vendor/promotional");
        }
    }

    public function edit($id) {
        $model = Promotional::findOrFail($id);
        $page_title = "Promotional Announcement";
        $states = Cities::getStates();
        $categories = ServiceArea::where('status', '=', 1)
                        ->orderby("id", "desc")->limit(12)->get();
        $user_id = Auth::user()->id;
        $serviceprovider_id = ServiceProvider::getServiceProviderId($user_id);
        $result = ServiceProvider::with("packages")->with("package_combination")->find($serviceprovider_id);

        $length = 0;
        $sel_category = explode(",",$model->category);
        $sel_areas = explode(",",$model->areas);
        if ($result) {
            $area = '';
            $state = array();
            foreach ($result->package_combination as $row) {
                $state[] = $row->states;
                //$county = $row->county;
                $area .= $row->service_areas;
            }
            $get_area = explode(",", $area);
            $ids = array_map('intval', $get_area);
        }

        $categories = ServiceArea::where('status', '=', 1)->whereIn("id", $ids)
                        ->orderby("name", "asc")->get();
        $states = States::whereIn("state_code", $state)->orderby("state", "asc")->get();

        return view('front.serviceprovider.promotional.edit', compact('id', 'states', 'categories', 'sel_category', 'sel_areas', 'model', 'states', 'categories', 'length', 'page_title'));
    }

    public function update($id, Request $request) {
        $model = Promotional::findOrFail($id);
        $input = $request->all();
        unset($input['_token']);

        $validation = array(
            'category' => 'required',
            'areas' => 'required',
            'title' => 'required|max:255',
            'contact_name' => 'required|max:255',
            'company_name' => 'required|max:255',
            'start_date' => 'required|date|max:20',
            'end_date' => 'required|date|max:20',
            'phone' => 'required|max:20',
        );

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $category = "";
            if (isset($request->category)) {
                unset($input['category']);
                $input['category'] = implode(",", $request->category);
            }
            $areas = "";
            if (isset($request->areas)) {
                unset($input['areas']);
                $input['areas'] = implode(",", $request->areas);
            }

            /* $input['title'] = $request->title;
              $input['location'] = $request->location;
              $input['job_description'] = $request->job_description;
              $input['apply_job'] = $request->apply_job;
              $input['name'] = $request->name;
              $input['email'] = $request->email;
              $input['phone'] = $request->phone; */

            $update = Promotional::where("id", "=", $id)->update($input);

            \Session::flash('success', 'Promotional updated successfully!');
            return redirect('vendor/promotional');
        }
    }

    public function delete($id) {
        $row = Promotional::where('id', '=', $id)->update(["deleted" => 1]);
        \Session::flash('success', 'Promotional Deleted Successfully!');
        return redirect('vendor/promotional');
    }

    public function view($id) {
        $model = Promotional::find($id);

        return view('front.serviceprovider.promotional.view', compact("model", "page_title"));
    }

}
