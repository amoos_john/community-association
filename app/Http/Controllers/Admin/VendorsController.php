<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Redirect;
use App\ServiceProvider;
use App\User;
use App\Functions\Functions;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use Auth,
    DB;
use App\Cities;
use App\States;
use Illuminate\Support\Facades\Input as Input;
use App\AdditionalUser;

class VendorsController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $model = ServiceProvider::search('');
        $status = User::$status;

        return view('admin.vendors.index', compact('model', 'status'));
    }

    public function search(Request $request) {
        $vendor_name = $request->input('vendor_name');
        $email = $request->input('email');
        $status_id = $request->input('status');

        $search['name'] = $vendor_name;
        $search['email'] = $email;
        $search['status'] = $status_id;

        $model = ServiceProvider::search($search);

        $status = User::$status;

        return view('admin.vendors.index', compact('model', 'email', 'status', 'vendor_name', 'status_id'));
    }

    public function view($id) {
        $model = ServiceProvider::with('users')->findOrFail($id);
        $status = User::$status;
        $states = States::orderby("state", "ASC")->lists("state", "state_code")->prepend('Select', '');

        return view('admin.vendors.view', compact('model', 'status', 'states','id'));
    }

    public function edit($id) {
        $model = ServiceProvider::with('users')->findOrFail($id);
        $status = User::$status;

        $states = States::orderby("state", "ASC")->lists("state", "state_code")->prepend('Select', '');
        $cities = Cities::getAllCities();

        return view('admin.vendors.edit', compact('model', 'status', 'cities', 'states'))->with('id', $id);
    }

    public function update($id, Request $request) {
        $id = $request->id;
        $model = ServiceProvider::findOrFail($id);
        $input = $request->all();
        unset($input['_token']);
        // 'first_name' => 'required|max:50',
        $validation = array(
            'login_name' => 'required|max:50',
            'email' => 'required|max:50|email|unique:users,email,' . $model->user_id,
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'phone' => 'required',
            'status' => 'required',
        );
        $message = [];
        if ($input['password'] != '') {
            $validation = array(
                'password' => 'required|confirmed|min:6',
            );
        }
        if (isset($input["logo"])) {
            $validation['logo'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg|max:5000';
            $message = [
                'logo.max' => 'logo size must be less or equal to 5MB.'
            ];
        }
        if (isset($input["license"])) {
            $validation['license'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg,
                doc,docx,ppt,pptx,txt,xls,xlsx,csv,ods,pdf|max:5000';
            $message = [
                'license.max' => 'License size must be less or equal to 5MB.'
            ];
        }
        if (isset($input["certificate"])) {
            $validation['certificate'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg,
                doc,docx,ppt,pptx,txt,xls,xlsx,csv,ods,pdf|max:5000';
            $message = [
                'certificate.max' => 'Certificate size must be less or equal to 5MB.'
            ];
        }
        if (isset($input["reference"])) {
            $validation['reference'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg,
                doc,docx,ppt,pptx,txt,xls,xlsx,csv,ods,pdf|max:5000';
            $message = [
                'reference.max' => 'Reference size must be less or equal to 5MB.'
            ];
        }
        if (isset($input["document"])) {
            $validation['document'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg,
                doc,docx,ppt,pptx,txt,xls,xlsx,csv,ods,pdf|max:5000';
            $message = [
                'document.max' => 'Document size must be less or equal to 5MB.'
            ];
        }
        $validator = Validator::make($request->all(), $validation, $message);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            if ($input["password"] != '') {
                $input_user['password'] = bcrypt($request->password);
            }
            $input_user['first_name'] = $request->first_name;
            $input_user['last_name'] = $request->last_name;
            $input_user['email'] = $request->email;
            $input_user['name'] = $request->login_name;
            $input_user['status'] = $request->status;
            $input_user['phone'] = $request->user_phone;
            $input_user['website_url'] = $request->user_website_url;

            $update = User::where('id', '=', $model->user_id)->update($input_user);

            unset($input['first_name']);
            unset($input['last_name']);
            unset($input['email']);
            unset($input['login_name']);
            unset($input['user_phone']);
            unset($input['user_website_url']);
            unset($input['password']);
            unset($input['password_confirmation']);

            if (isset($input["logo"])) {
                $logo = Input::file('logo');
                $logoPath = 'uploads/serviceprovider/logo/';
                $destinationPath = public_path() . '/' . $logoPath;
                $logoName = Functions::saveImage($logo, $destinationPath, "");
                $input['logo'] = $logoPath . $logoName;
            }
            if (isset($input["license"])) {
                $file = Input::file('license');
                $path = 'uploads/serviceprovider/license/';
                $destinationPath = public_path() . '/' . $path;
                $licenseName = Functions::saveImage($file, $destinationPath, "");
                $input['license'] = $path . $licenseName;
            }
            if (isset($input["certificate"])) {
                $file = Input::file('certificate');
                $path = 'uploads/serviceprovider/certificate/';
                $destinationPath = public_path() . '/' . $path;
                $docName = Functions::saveImage($file, $destinationPath, "");
                $input['certificate'] = $path . $docName;
            }
            if (isset($input["reference"])) {
                $file = Input::file('reference');
                $path = 'uploads/serviceprovider/reference/';
                $destinationPath = public_path() . '/' . $path;
                $docName = Functions::saveImage($image, $destinationPath, "");
                $input['reference'] = $path . $docName;
            }
            if (isset($input["document"])) {
                $document = Input::file('document');
                $docPath = 'uploads/serviceprovider/documents/';
                $destinationPath = public_path() . '/' . $docPath;
                $documentName = Functions::saveImage($document, $destinationPath);
                $input['document'] = $docPath . $documentName;
            }

            $affectedRows = ServiceProvider::where("id", "=", $id)->update($input);
        }

        \Session::flash('success', 'Vendor Updated Successfully!');
        return redirect('admin/vendors');
    }

    public function delete($id) {
        $model = ServiceProvider::find($id);
        $update = User::where('id','=',$model->user_id)->update(['status'=>0]);
        
        $row = ServiceProvider::destroy($id);
        \Session::flash('success', 'Vendor Deleted Successfully!');
        return redirect('admin/vendors');
    }
    public function indexUsers() {
        $model = AdditionalUser::with('user')->with('modified')
                ->with('service_provider')->where('serviceprovider_id','!=',0)
                ->orderby('id','desc')->paginate(20);
        $status = User::$status;
        $vendors = ServiceProvider::allServiceProvider();
        
        return view('admin.vendors.users', compact('model', 'status','vendors'));
    }
    public function searchUsers(Request $request) {
        
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $email = $request->input('email');
        $status_id = $request->input('status');
        $vendor_id = $request->input('vendor_id');

        $search['first_name'] = $first_name;
        $search['last_name'] = $last_name;
        $search['email'] = $email;
        $search['status'] = $status_id;
        $search['serviceprovider_id'] = $vendor_id;

        $model = AdditionalUser::searchUsers($search);
        $vendors = ServiceProvider::allServiceProvider();
        $status = User::$status;
        $query = $request->query();
        return view('admin.vendors.users', compact('model','query','first_name','last_name','vendors','vendor_id', 'email', 'status', 'vendor_name', 'status_id'));
       
        
    }
    public function deleteUser($id) {
        $model = AdditionalUser::find($id);
        $del = User::where('id','=',$model->user_id)->delete();
        
        $delete = AdditionalUser::where('id','=',$id)->delete();
        \Session::flash('success', 'Additional User Deleted Successfully!');
        return redirect('admin/vendors/users');
      
    }
    public function statusUser($id) {
        $model = AdditionalUser::find($id);
        if($model->status==1)
        {
            $input["status"] = 0;
            $update = User::where('id','=',$model->user_id)->update($input);
            $update_user = AdditionalUser::where('id','=',$id)->update($input);
        }
        elseif($model->status==0) {
            
            $input["status"] = 1;
            $update = User::where('id','=',$model->user_id)->update($input);
            $update_user = AdditionalUser::where('id','=',$id)->update($input);

        }
         
        \Session::flash('success', 'User status changed successfully!');
        return redirect('admin/vendors/users');
      
    }
}
