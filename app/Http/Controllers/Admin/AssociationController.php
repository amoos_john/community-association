<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use App\Association;
use App\User;
use App\Functions\Functions;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use Auth,
    DB;
use App\AdditionalUser;
use App\Cities;
use App\States;

class AssociationController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $model = Association::search('');
        $status = User::$status;

        return view('admin.association.index', compact('model', 'status'));
    }

    public function search(Request $request) {
        $assoc_name = $request->input('assoc_name');
        $email = $request->input('email');
        $status_id = $request->input('status');

        $search['name'] = $assoc_name;
        $search['email'] = $email;
        $search['status'] = $status_id;

        $model = Association::search($search);

        $status = User::$status;

        return view('admin.association.index', compact('model', 'email', 'status', 'vendor_name', 'status_id'));
    }

    public function view($id) {
        $model = Association::with('users')->findOrFail($id);
        $getState = States::where("state_code", "=",$model->state)->first();
        $getCity = Cities::where("id", "=",$model->city)->first();
        return view('admin.association.view', compact('model','id', 'getState','getCity'));
    }

    public function edit($id) {
        $model = Association::with('users')->findOrFail($id);
        $status = User::$status;
        $states = States::orderby("state", "ASC")->lists("state", "state_code")->prepend('Select', '');

        return view('admin.association.edit', compact('model', 'status', 'states'))->with('id', $id);
    }

    public function update($id, Request $request) {
        $id = $request->id;
        $model = Association::findOrFail($id);
        $input = $request->all();
        unset($input['_token']);

        $validation = array(
            'login_name' => 'required|max:50',
            'user_email' => 'required|max:50|email|unique:users,email,' . $model->user_id,
            'name' => 'required|max:255',
            'phone' => 'required',
            'status' => 'required',
        );

        if ($input['password'] != '') {
            $validation = array(
                'password' => 'required|confirmed|min:6',
            );
        }
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            if ($input["password"] != '') {
                $input_user['password'] = bcrypt($request->password);
            }
            $input_user['email'] = $request->user_email;
            $input_user['name'] = $request->login_name;
            $input_user['status'] = $request->status;
            $input_user['phone'] = $request->user_phone;
            $update = User::where('id', '=', $model->user_id)->update($input_user);

            unset($input['user_email']);
            unset($input['login_name']);
            unset($input['user_phone']);
            unset($input['position_pro']);
            unset($input['password']);
            unset($input['password_confirmation']);

            $input['name'] = $request->name;
            $input['city'] =$request->city;
            $input['state'] = $request->state;
            $input['zip_code'] = $request->zip_code;
            $input['contact_person'] = $request->contact_person;
            $input['position'] = $request->position;
            $input['phone'] = $request->phone;
            $input['phone2'] = $request->phone2;
            $input['email'] = $request->email;
            $input['website_url'] = $request->website_url;
            $input['address'] = $request->address;
            $input['google_map'] = $request->google_map;
            $input['no_of_bulidings'] = $request->no_of_bulidings;
            $input['no_of_units'] = $request->no_of_units;
            $input['association_type'] = $request->association_type;
            $input['advertisement'] = (isset($request->advertisement)) ? $request->advertisement : 0;
            $affectedRows = Association::where("id", "=", $id)->update($input);
            
        }
        \Session::flash('success', 'Association Updated Successfully!');
        return redirect('admin/association');
    }

    public function delete($id) {
        $model = Association::find($id);
        $update = User::where('id', '=', $model->user_id)->update(['status' => 0]);

        $row = Association::destroy($id);
        \Session::flash('success', 'Association Deleted Successfully!');
        return redirect('admin/association');
    }

    public function indexUsers() {
        $model = AdditionalUser::with('user')->with('modified')
                        ->with('association')->where('association_id', '!=', 0)
                        ->orderby('id', 'desc')->where('user_id', '!=', 0)->paginate(20);

        $status = User::$status;
        $associations = Association::allAssociation();

        return view('admin.association.users', compact('model', 'status', 'associations'));
    }

    public function searchUsers(Request $request) {

        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $email = $request->input('email');
        $status_id = $request->input('status');
        $assoc_id = $request->input('assoc_id');

        $search['first_name'] = $first_name;
        $search['last_name'] = $last_name;
        $search['email'] = $email;
        $search['status'] = $status_id;
        $search['association_id'] = $assoc_id;

        $model = AdditionalUser::searchUsers($search);

        $associations = Association::allAssociation();
        $status = User::$status;
        $query = $request->query();
        return view('admin.association.users', compact('model', 'query', 'first_name', 'last_name', 'associations', 'assoc_id', 'email', 'status', 'status_id'));
    }

    public function deleteUser($id) {
        $model = AdditionalUser::find($id);
        $del = User::where('id', '=', $model->user_id)->delete();

        $delete = AdditionalUser::where('id', '=', $id)->delete();
        \Session::flash('success', 'User Deleted Successfully!');
        return redirect('admin/association/users');
    }

    public function statusUser($id) {
        $model = AdditionalUser::find($id);
        if ($model->status == 1) {
            $input["status"] = 0;
            $update = User::where('id', '=', $model->user_id)->update($input);
            $update_user = AdditionalUser::where('id', '=', $id)->update($input);
        } elseif ($model->status == 0) {

            $input["status"] = 1;
            $update = User::where('id', '=', $model->user_id)->update($input);
            $update_user = AdditionalUser::where('id', '=', $id)->update($input);
        }

        \Session::flash('success', 'User status changed successfully!');
        return redirect('admin/association/users');
    }

}
