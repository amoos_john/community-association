<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use App\User;
use App\Functions\Functions;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;

class UsersController extends AdminController {

    public function __construct() 
    {
        parent::__construct();
    }

    public function index() {
        $model = User::where("role_id","=",1)->paginate(10);
        return view('admin.users.index', ['model' => $model]);
    }

    public function create() {
        $status = User::$status;
        return view('admin.users.create', compact('status'));
    }

    public function insert(Request $request) {
        
        $validation = array(
            'name' => 'required|max:50',
            'email' => 'required|email|max:50|unique:users',
            'password' => 'required|confirmed|min:6',

            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->status = $request->status;
            $user->role_id = 1;
            $user->save();

            \Session::flash('success', 'User Added Successfully!');
            return redirect('admin/users');
        }
        
       
    }

    public function edit($id) {
        $model = User::findOrFail($id);
        $status = User::$status;
        return view('admin.users.edit', compact('model','status'))->with('id', $id);
    }

    public function update($id, Request $request) {
        $id = $request->id;
        $user = User::findOrFail($id);
        $input = $request->all();
        unset($input['_token']);
        
        if($input["password"]=='')
        {
            unset($input['password']);
            unset($input['password_confirmation']);
            $affectedRows = User::where('id', '=', $id)->update($input);
        }
        else
        {
             $validation = array(
            'name' => 'required|max:50',
            'email' => 'required|max:50|email|unique:users,email,' . $user->id,
            'password' => 'required|confirmed|min:6',

            );
       
            $validator = Validator::make($request->all(), $validation);

            if ($validator->fails()) {

                return redirect()->back()->withErrors($validator->errors())->withInput();

            }
            
            unset($input['password_confirmation']);

	    $input['password'] = bcrypt($request->password);

            $affectedRows = User::where('id', '=', $id)->update($input);
        }
        
        \Session::flash('success', 'Updated Successfully!');
        return redirect('admin/users');
    }

    public function delete($id) {
        $row = User::where('id', '=', $id)->delete();
        \Session::flash('success', 'User Deleted Successfully!');
        return redirect('admin/users');
    }

}
