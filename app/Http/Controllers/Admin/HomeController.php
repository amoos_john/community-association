<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Validator, Input, Redirect;
use DB;
use App\User;
use Auth;
use Session;
use Illuminate\Http\Request;
use App\Cities;

class HomeController extends AdminController {

    public function __construct() 
    {
        parent::__construct();
    }

    public function index() {
        
        if(Auth::user()->role_id==1)
        {
            return view('admin.home');
        }
        else
        {
            return redirect('/');
        }

    }
    public function getCities(Request $request)
    {
        $state_code = $request->state_code;
        $city = (isset($request->city))?$request->city:'';

        if ($state_code != "") {
            $cities = Cities::getStateCities($state_code);
            echo '<option value="">Select City</option>';
            if (count($cities) > 0) {
                
                foreach ($cities as $row) {
                    $selected = '';
                    if($city==$row->id)
                    {
                        $selected = 'selected';
                    }
                    echo '<option value="' . $row->id . '" ' .$selected. '>'. $row->city . '</option>';
                    
                }
            }
        }
    }

    
}
