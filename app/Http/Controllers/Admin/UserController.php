<?php
/**
 * Created by PhpStorm.
 * User: amir
 * Date: 1/13/2016
 * Time: 12:39 PM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Input;
use View;
use DB;
use Validator, Redirect;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class UserController extends Controller {

    function __construct()
    {

    }

    public function getLogin()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $validation = array(
            'email' => 'required|max:50',
            'password' => 'required|min:6',

       );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $userdata = array(
                'email'     => $request->email,
                'password'  => bycrpt($request->password)
            );
            if (Auth::attempt($userdata)) 
            {
                
            }
            else
            {
                
            }

        }
        
       
    }

    public function getLogOut()
    {
        HelperClass::destroySession('admin');
        return Redirect::to('admin/login');
    }


}