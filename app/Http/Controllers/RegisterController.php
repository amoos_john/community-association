<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator,
    Input,
    Redirect;
use App\Functions\Functions;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\RedirectResponse;
use Session;


class RegisterController extends Controller {
    
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $loginPath = 'login';
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() 
    {
        //$this->auth = $auth;
        //$this->registrar = $registrar;
        $this->middleware('guest', ['except' => 'getLogout']);
    }
   
    public function index() {
        
        $page_title = "Login";
        return view('front.login');
    }
    public function postLogin(Request $request) 
    {
        $this->validate($request, [
            'email' => 'required|email|max:50|exists:users,email,status,1',
            'password' => 'required|min:6',
            
        ]);

        $credentials = $request->only('email', 'password');

        if(Auth::attempt($credentials, $request->has('remember'))) 
        {
            $role_id = Auth::user()->role_id;
            if($role_id==1)
            {
                return redirect('admin');
            }
            if($role_id==2)
            {
                return redirect('association/dashboard');
            }
            if($role_id==3)
            {
                return redirect('vendor/dashboard');
            } 
            if($role_id==4)
            {
                return redirect('user/dashboard');
            } 
        }

        return redirect('login')
                        ->withInput($request->only('email', 'remember'))
                        ->withErrors([
                            'email' => $this->getFailedLoginMessage(),
        ]);
    }
    public function register() 
    {
        return view('front.register');
    }
    public function logout() 
    {
        Auth::logout;
        return redirect('/');
    }
    
}
