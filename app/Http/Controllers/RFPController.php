<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Input as Input;
use Validator,
    Redirect,
    Session,
    Config,
    Mail;
use App\Functions\Functions;
use App\Association;
use App\RFP;
use App\RFPVendors;
use App\ServiceProvider;
use App\Content;
use App\AdditionalUser;
use App\AssociationSubscriptions;

class RFPController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | RFPController 
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index() 
    {
        $user_id = Auth::user()->id;
        $page_title = "RFP Status";
        $statuses = RFP::$status;
        $date = date('Y-m-d');
        
        if(Auth::user()->role_id==2)
        {
            $accociation = Association::where("user_id","=",$user_id)->first();
            $association_id = $accociation->id;
            $model = RFP::where("association_id","=",$association_id)->where("deleted","!=",1)
            ->orderby("id","DESC")->paginate(10);
        }
        elseif(Auth::user()->role_id==4) {
            
           $accociation = AdditionalUser::where("user_id","=",$user_id)->first();
           $association_id = $accociation->association_id; 
           $model = RFP::where("created_by","=",$user_id)
                   ->where("association_id","=",$association_id)
                   ->where("deleted","!=",1)->orderby("id","DESC")->paginate(10);
          

        }
        
        return view('front.association.rfp.index',compact("page_title","model","statuses","date"));
    }

    public function create() 
    {
        $user_id = Auth::user()->id;
        if(Auth::user()->role_id==2)
        {
            $assoc = AssociationSubscriptions::getSubcription($user_id);
            if(count($assoc)>0)
            {
                $page_title = "RFP Form"; 
                return view('front.association.rfp.create',compact("page_title"));
            }
            else
            {
                return redirect("association/rfp")->with("message","Your Package has been expire!");
            }
        }
        elseif(Auth::user()->role_id==4) {
            
            $users = AdditionalUser::where("user_id","=",$user_id)->first();
            $parent_id=$users->modified_by;
            $assoc = AssociationSubscriptions::getSubcription($parent_id);
            if(count($assoc)>0)
            {
                $page_title = "RFP Form"; 
                return view('front.association.rfp.create',compact("page_title"));
            }

        }
        else
        {
            return redirect("association/rfp")->with("message","Your Package has been expire!");
        }
    }
    
    
    public function insert(Request $request) 
    {
        $input = $request->all();
        $validation = array(
            'title' => 'required|max:255',
            'name' => 'required|max:255',
            'submission_date' => 'required|max:255',
            'job_description' => 'required',
            'requirement' => 'required',
            'project_specification' => 'required',
            );
        
        $validation['image'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg|max:5000';
        $message = [
            'image.max' => 'Image size must be less or equal to 5MB.'
        ];
        if(isset($input["images2"]))
        {
            $validation['image2'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg|max:5000';
            $message = [
                'image2.max' => 'Image size must be less or equal to 5MB.'
            ];
        }
        if(isset($input["document"]))
        {
            $validation['document'] = 'required|mimes:doc,docx,ppt,pptx,txt,xls,xlsx,csv,ods,pdf|max:5000';
            $message = [
                'document.max' => 'File size must be less or equal to 5MB.'
            ];
        }
        $validator = Validator::make($request->all(), $validation,$message);
        
        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $parent_id = Auth::user()->id;
            if(Auth::user()->role_id==2)
            {
              $assoc = AssociationSubscriptions::getSubcription($parent_id);
            }
            elseif(Auth::user()->role_id==4) {
                $users = AdditionalUser::where("user_id","=",$parent_id)->first();
                $parent_id=$users->modified_by;
                $assoc = AssociationSubscriptions::getSubcription($parent_id);
            }
            
            if(count($assoc)>0)
            {

            $accociation = Association::where("user_id","=",$parent_id)->get();
            $user_id = Auth::user()->id;

            $association_id = $accociation[0]->id;  
            
            $image = Input::file('image');
            
            $path='uploads/association/images/';
            $destinationPath = public_path() . '/'.$path;
            $imageName = Functions::saveImage($image, $destinationPath,"");
            $upload = Image::make($destinationPath . $imageName)->fit(280)->save($destinationPath . $imageName);
            
            $rfp = new RFP;
            $rfp->title = $request->title;
            $rfp->name = $request->name;
            $rfp->association_id = $association_id;
            $rfp->location = $request->location;
            $rfp->submission_date = $request->submission_date;
            $rfp->point_contact = $request->point_contact;
            $rfp->point_contact_phone = $request->point_contact_phone;
            $rfp->job_description = $request->job_description;
            $rfp->requirement = $request->requirement;
            $rfp->project_specification = $request->project_specification;
            $rfp->image_name = $request->image_name;
            $rfp->image = $path.$imageName;
            $rfp->image_name2 = $request->image_name2;
            $rfp->created_by = $user_id;
            
            if(isset($input["images2"]))
            {
                $image2 = Input::file('image2');
                $destinationPath = public_path() . '/'.$path;
                $imageName2 = Functions::saveImage($image2, $destinationPath);
                $upload = Image::make($destinationPath . $imageName2)->fit(280)->save($destinationPath . $imageName2);
                $rfp->image2 = $path.$imageName2;
            }
            $rfp->document_name = $request->document_name;
            if(isset($input["document"]))
            {
                $document = Input::file('document');
                $docPath='uploads/association/documents/';
                $destinationPath = public_path() . '/'.$docPath;
                $documentName = Functions::saveImage($document, $destinationPath);
                $rfp->document = $docPath.$documentName;
            }
            $rfp->status = 1;
            $rfp->save();
            
            $id = $rfp->id;
            $total_rfp = $assoc->total_rfp - 1;
            $update = AssociationSubscriptions::where('id','=',$assoc->id)->update(["total_rfp"=>$total_rfp]);
            
            return redirect("association/rfp/vendor/".$id)->with("id",$id);
            }
            else
            {
                return redirect("association/rfp")->with("message","Your Package has been expired!");
            }
        }
        
    }
    public function rfpVendors(Request $request) 
    {
        $page_title = "RFP To Your Desire Vendors";
        $id = $request->id;
        $user_id = Auth::user()->id;
        $rfp_vendor = RFPVendors::where('rfp_id','=',$id)->first();

        if(Auth::user()->role_id==4) {
            $users = AdditionalUser::where("user_id","=",$user_id)->first();
            $user_id=$users->modified_by;
        }
        if(count($rfp_vendor)==0)
        {
            $result = Association::totalRfps($user_id);
            $total_rfps = $result->total_rfp;
            $model = ServiceProvider::searchVendor();
            $rfp = RFP::find($id);

            return view('front.association.rfp.vendors',compact("page_title","rfp","model","id","total_rfps"));
        }
        else {
            return redirect('association/rfp');
        }
    }
    public function storeSignature(Request $request)
    {
        $filname = '';
          if (isset($request->defaultimage) && $request->defaultimage != '') {
               $imagdata = $request->defaultimage;
               $type = $request->type;
               $rfp_id = $request->id ? $request->id : 0;
               $sign_date = date('Y-m-d H:i:s');

               $filname = $this->saveImage($imagdata, $type, $rfp_id, $sign_date);
          } else {
               $imagdata = '';
          }


          echo $linkurl = ($filname);
          die;
    }
    public function rfpVendorsInsert(Request $request,$id) 
    {
        $input = $request->all();
        $validation = array(
            'vendor_id' => 'required',
           
        );
        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
       
        else
        {
            $vendors = $request->vendor_id;
            
            $data = array();
            foreach($vendors as $vendor)
            {
                $data[] = array('rfp_id'=>$id, 'serviceprovider_id'=> $vendor,
                                'created_at'=>date('Y-m-d H:i:s'));
                 
            }
            if(count($data)>0)
            {
                RFPVendors::insert($data);
                
                $cc = Config::get("params.admin_email");
                $from = Config::get("params.server_email");

                $content = Content::where("code","=","send_rfp")->get();
                $result = RFP::with("association")->find($id);
                
                $replaces['ASSC'] = $result->association->name;
                $replaces['TITLE'] = $result->title;
                $replaces['LOCATION'] = $result->location;
                $replaces['DATE'] = $result->submission_date;

                $template = Functions::setEmailTemplate($content, $replaces);

                $subject = $template["subject"];
                $body = $template["body"];
                
                $ids = array_map('intval', $vendors);
                $providers = ServiceProvider::with("users")->whereIn("id",$ids)->get();
                foreach($providers as $pro)
                {
                    $to = $pro->users->email;
                    $send_email = Functions::sendEmail($to,$subject,$body,'',$from);
                    /*$data= [
                    'from' =>$from,
                    'body' => $body,
                    'subject' => $subject,
                    'email_send' =>$to,
                    ];
                    Mail::send(['html' => 'email.register'],['data' => $data], function($message) use ($data) { 
                     $message->from($data['from']);
                     $message->to($data['email_send'])->subject($data['subject']);
                    });*/

                } 
            }
            
            return redirect("association/rfp")->with('success','RFPs has been send to vendors successfully!');
            
        } 
        
        
    }
    public function edit($id) 
    {
        $model = RFP::findOrFail($id);
        
        $page_title = "Edit RFP Form";
        
        return view('front.association.rfp.edit', compact('id','model','page_title'));
    }
    public function update($id, Request $request) 
    {
        $model = RFP::findOrFail($id);
        $input = $request->all();
        unset($input['_token']);
        
        $validation = array(
            'title' => 'required|max:255',
            'name' => 'required|max:255',
            'submission_date' => 'required|max:255',
            'job_description' => 'required',
            'requirement' => 'required',
            'project_specification' => 'required',
            );
        $message = [];
        if(isset($input["image"]))
        {
            $validation['image'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg|max:5000';
            $message = [
                'image.max' => 'Image size must be less or equal to 5MB.'
            ];
        }
        if(isset($input["images2"]))
        {
            $validation['image2'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg|max:5000';
            $message = [
                'image2.max' => 'File size must be less or equal to 5MB.'
            ];
        }
        if(isset($input["document"]))
        {
            $validation['document'] = 'required|mimes:doc,docx,ppt,pptx,txt,xls,xlsx,csv,ods,pdf|max:5000';
            $message = [
                'document.max' => 'File size must be less or equal to 5MB.'
            ];
        }
        $validator = Validator::make($request->all(), $validation,$message);
        
        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $input['title'] = $request->title;
            $input['name'] = $request->name; 
            $input['location'] = $request->location;
            $input['submission_date'] = $request->submission_date;
            $input['point_contact'] = $request->point_contact;
            $input['point_contact_phone'] = $request->point_contact_phone;
            $input['job_description'] = $request->job_description;
            $input['requirement'] = $request->requirement;
            $input['project_specification'] = $request->project_specification;
            $input['image_name'] = $request->image_name;
            $input['image_name2'] = $request->image_name2;
            $input['document_name'] = $request->document_name;
            
            $path='uploads/association/images/';
            $destinationPath = public_path() . '/'.$path;
            if(isset($input["image"]))
            {
                $delete = public_path().'/'.$model->image; 
                if(file_exists($delete) && $model->image!=""){
                    unlink($delete);
                }
                $image = Input::file('image');  
                $imageName = Functions::saveImage($image, $destinationPath);
                $upload = Image::make($destinationPath . $imageName)->fit(280)->save($destinationPath . $imageName);
                $input['image'] = $path.$imageName;
            }
            
            if(isset($input["image2"]))
            {
                $delete =  public_path().'/'.$model->image2;
              
                if(file_exists($delete) && $model->image2!=""){
                    
                    unlink($delete);
                }
                $image2 = Input::file('image2');
                $imageName2 = Functions::saveImage($image2, $destinationPath);
                $upload = Image::make($destinationPath . $imageName2)->fit(280)->save($destinationPath . $imageName2);
                $input['image2'] = $path.$imageName2;
            }
            if(isset($input["document"]))
            {
                $delete = public_path() .'/'.$model->document;
                if(file_exists($delete) && $model->document!=""){
                    unlink($delete);
                }
                $document = Input::file('document');
                $docPath='uploads/association/documents/';
                $destinationPath = public_path() . '/'.$docPath;
                $documentName = Functions::saveImage($document, $destinationPath);
                $input["document"] = $docPath.$documentName;
            }
            
            $update = RFP::where("id","=",$id)->update($input);
            
            \Session::flash('success', 'RFP updated successfully!');
            return redirect('association/rfp');
        }
       
        
    }
    public function delete($id)
    {
        $row = RFP::where('id', '=', $id)->update(["deleted"=>1]);
        \Session::flash('success', 'RFP Deleted Successfully!');
        return redirect('association/rfp');
    }
    public function view($id)
    {
        $model = RFP::find($id);
        
        $submission_date = (count($model)>0)?Functions::dateFormat($model->submission_date):'';
        
        return view('front.association.rfp.view',compact("model","page_title","submission_date"));

    }
    
    function saveImage($base64img, $type, $rfp_id, $sign_date = 0) {
          $pathimages = public_path('uploads/association/signature_images') . '/';
          $tempImageInput = array();
          $base64img = str_replace('image/png;base64,', '', $base64img);
          $newfilename = round(microtime(true));
          $tempImageInput['name'] = $newfilename;
          $tempImageInput['ext'] = 'png';
          $tempImageInput['path'] = 'uploads/association/signature_images/';

          $data = base64_decode($base64img);
          $file = $tempImageInput['path'] . $tempImageInput['name'] . '.' . $tempImageInput['ext'];
          if (file_put_contents($file, $data)) {
               
                    $params = array();
                    $params['sign'] = $tempImageInput['path'] . $tempImageInput['name'] . '.' . $tempImageInput['ext'];
                    $params['updated_at'] = date('Y-m-d H:i:s');
                    $params['rfp_id'] = $rfp_id;
                    
                    $find = RFP::find($rfp_id);
                    //dd($params);
                    if($find->signature!="")
                    {
                        if (file_exists(public_path($find->signature))) {
                              unlink(public_path($find->signature));
                         }
                         
                         $update = RFP::updateSignature($params);
                         return $file;
                    }
                    else
                    {
                        $update = RFP::updateSignature($params);
                        return $file;
                    }
      
               }
          else {
               return null;
          }
     }
    /* Servcie Provider*/ 
    public function indexRfp() 
    {
        $page_title = "Received RFPs";
        $user_id = Auth::user()->id; 
        $serviceprovider_id = ServiceProvider::getServiceProviderId($user_id);
        $model = RFPVendors::getRfpVendor($serviceprovider_id);
        $date = date('Y-m-d');
        $statuses = RFPVendors::$status;
        
        return view('front.serviceprovider.rfp.index',compact("model","statuses","page_title","date"));
            
    }
    public function viewRfp($id)
    {
        $result = RFPVendors::find($id);
        $model = RFP::find($result->rfp_id);
        $submission_date = (count($model)>0)?Functions::dateFormat($model->submission_date):'';
        $close_date = date("Y-m-d",strtotime($model->submission_date));
        $date = date('Y-m-d');
        
        return view('front.serviceprovider.rfp.view',compact("model","result","id","page_title","submission_date","date","close_date"));
    }
    public function insertRfp($id, Request $request) 
    {
        //$model = RFPVendors::findOrFail($id);
        $user_id = Auth::user()->id;
        $date = date("Y-m-d");
        $row = RFPVendors::where('id', '=', $id)->update(["status"=>1,
            "submitted_date"=>$date,"submitted_by"=>$user_id]);
        \Session::flash('success', 'Received RFP Submitted Successfully!');
        return redirect('vendor/rfp');
    }
    public function deleteRfp($id)
    {
        $row = RFPVendors::where('id', '=', $id)->update(["status"=>0]);
        \Session::flash('success', 'Received RFP Deleted Successfully!');
        return redirect('vendor/rfp');
    } 
    public function searchVendor(Request $request)
    {
        $name = $request->search;
        $per_page = $request->per_page;
        $search['search'] = $name;
        $search['per_page'] = $per_page;
        $model = ServiceProvider::searchVendor($search);
        
        return view('front.association.rfp.search',compact("model"));

    }
    
}
