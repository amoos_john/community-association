<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input as Input;
use Intervention\Image\Facades\Image as Image;
use Validator,
    Redirect,
    Session,
    Config,
    Mail;
use App\Functions\Functions;
use App\JobBids;
use App\Jobs;
use App\Content;

class JobBidsController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | AssociationRegister Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    public function index() 
    {
        $page_title = "Open Job Bids";
        
        $user_id = Auth::user()->id;
        
        $model = Jobs::getActiveJobs();
        
        $statuses = JobBids::$status;
        
        return view('front.serviceprovider.jobs.index',compact("model","user_id","statuses","page_title"));
    }
    
    public function reply(Request $request) 
    {
        $input = $request->all();
        $validation = array(
            'email' => 'required|email|max:50',
            'message' => 'required|max:200',
            );
        
        $message = [];
        if($input["file"]!="")
        {
            $validation['file'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg,
                doc,docx,ppt,pptx,txt,xls,xlsx,csv,ods,pdf|max:5000';
            $message = [
                'file.max' => 'File size must be less or equal to 5MB.'
            ];
        }
        $response['error'] = 0;
        $validator = Validator::make($request->all(), $validation,$message);
        
        if ($validator->fails()) {
            
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
          
        }
        else
        {
            $user_id = Auth::user()->id;
            $id = $request->id;
            $from = $request->email;
            $message = $request->message;
            
            
            $bids = new JobBids;
            $bids->job_id  = $id;
            $bids->user_id  = $user_id;
            $bids->email  = $from;
            $bids->message  = $message;
            $pathToFile = "";
            if($input["file"]!="")
            {
                $file = Input::file('file');
                $path='uploads/serviceprovider/jobs/';
                $destinationPath = public_path() . '/'.$path;
                $fileName = Functions::saveImage($file, $destinationPath,"");
                $bids->document  = $path.$fileName;
                $pathToFile = $destinationPath. '/'.$fileName;
            }
            $bids->status  = 1;
            $bids->save();
            
            $result = Jobs::findorFail($id);
            $to = $result->email;
            $title = $result->title;
            $location = $result->location;
            
            $cc = Config::get("params.admin_email");
            $email = Config::get("params.admin_email");
            
            $content = Content::where("code","=","job_bids")->get();
            
            $replaces['TITLE'] = $title;
            $replaces['LOCATION'] = $location;
            
            $replaces['EMAIL'] = $from;
            $replaces['MESSAGE'] = $message; 
            $replaces['DATE'] = date("Y-m-d");
            
            $template = Functions::setEmailTemplate($content, $replaces);
            
            $subject = $template["subject"];
            $body = $template["body"];
            
            $data= [
            'from' =>$email,
            'body' => $body,
            'subject' => $subject,
            'email_send' =>$to,
            'cc' => $cc,
            'document' =>$pathToFile
                   
            ];
            
            Mail::send(['html' => 'email.job'],['data' => $data], function($message) use ($data) {
             $message->attach($data['document']);   
             $message->from($data['from']);
             $message->cc($data['cc']);
             $message->to($data['email_send'])->subject($data['subject']);
            }); 
            
            
        }
        if($response['error']==1)
        {
            foreach($response['errors']->all() as $error)
            {
                echo '<p>'.$error.'</p>';
                
            }     
        }
        else 
        {
           echo '1';
        }
    }
    
    public function delete($id)
    {
        $row = JobBids::where('id', '=', $id)->update(["status"=>0]);
        \Session::flash('success', 'Job Bid Deleted Successfully!');
        return redirect('vendor/jobs');
    }
    public function view($id)
    {
        $user_id = Auth::user()->id;
        $model = Jobs::find($id);
        $job_bid = JobBids::where(['job_id'=>$id,'user_id'=>$user_id])
                   ->where("deleted","!=",1)->where("status","=",1)->first();
        
        return view('front.serviceprovider.jobs.view',compact("model","page_title","id","job_bid"));

    }
    
    
}
