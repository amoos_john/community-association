<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Input as Input;
use Validator,
    Redirect,
    Session,
    Config;
use App\Functions\Functions;
use App\Association;
use App\Jobs;
use App\AssociationSubscriptions;

class JobsController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Jobs Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    public function index() 
    {
        $page_title = "Open Jobs";
        
        $user_id = Auth::user()->id;
        
        $association_id = Association::getAssociationId($user_id);
         
        $model = Jobs::where("association_id","=",$association_id)->where("deleted","!=",1)
        ->orderby("id","desc")->paginate(10);
        
        $statuses = Jobs::$status;
        
        return view('front.association.jobs.index',compact("model","statuses","page_title"));
    }
    public function create() 
    {   
        $user_id = Auth::user()->id;
        $assoc = AssociationSubscriptions::getSubcription($user_id);
        if(count($assoc)>0)
        {
            $page_title = "Post An Open Job";
            return view('front.association.jobs.create',compact("page_title"));
        }
        else
        {
            return redirect("association/jobs")->with("message","Your Package has been expire!");
        }
        
    }
    
    public function insert(Request $request) 
    {
        $input = $request->all();
        $validation = array(
            'title' => 'required|max:255',
            'location' => 'required|max:255',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required',
            );
        
        $validator = Validator::make($request->all(), $validation);
        
        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $user_id = Auth::user()->id;
        
            $association_id = Association::getAssociationId($user_id);
            
            $jobs = new Jobs;
            $jobs->association_id = $association_id;
            $jobs->title = $request->title;
            $jobs->location = $request->location;
            $jobs->job_description = $request->job_description;
            $jobs->apply_job = $request->apply_job;
            $jobs->name = $request->name;    
            $jobs->email = $request->email;
            $jobs->phone = $request->phone;
            $jobs->status = 1;
            $jobs->save();
            
            $assoc = AssociationSubscriptions::getSubcription($user_id);
            if(count($assoc)>0)
            {
                $total_rfp = $assoc->total_rfp - 1;
                $update = AssociationSubscriptions::where('id','=',$assoc->id)->update(["total_rfp"=>$total_rfp]);
            }
            
            \Session::flash('success', 'Job Added Successfully!');
            return redirect("association/jobs");
        }
        
    }
    public function edit($id) 
    {
        $model = Jobs::findOrFail($id);
        
        $page_title = "Edit Open Job";
        
        return view('front.association.jobs.edit', compact('id','model','page_title'));
    }
    public function update($id, Request $request) 
    {
        $model = Jobs::findOrFail($id);
        $input = $request->all();
        unset($input['_token']);
        
        $validation = array(
            'title' => 'required|max:255',
            'location' => 'required|max:255',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required',
            );
        
        $validator = Validator::make($request->all(), $validation);
        
        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $input['title'] = $request->title;
            $input['location'] = $request->location;
            $input['job_description'] = $request->job_description;
            $input['apply_job'] = $request->apply_job;
            $input['name'] = $request->name;    
            $input['email'] = $request->email;
            $input['phone'] = $request->phone;
            
            $update = Jobs::where("id","=",$id)->update($input);
            
            \Session::flash('success', 'Job updated successfully!');
            return redirect('association/jobs');
        }
       
        
    }
    public function delete($id)
    {
        $row = Jobs::where('id', '=', $id)->update(["deleted"=>1]);
        \Session::flash('success', 'Job Deleted Successfully!');
        return redirect('association/jobs');
    }
    public function view($id)
    {
        $model = Jobs::find($id);
        
        return view('front.association.jobs.view',compact("model","page_title"));

    }
    
    
}
