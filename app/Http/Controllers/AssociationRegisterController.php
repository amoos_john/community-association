<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator,
    Input,
    Redirect,
    Session,
    Config,
    Mail;
use App\Functions\Functions;
use App\Association;
use App\Cities;
use App\Packages;
use App\AdditionalUser;
use App\Content;

class AssociationRegisterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | AssociationRegister Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout', 'getLogout']]);     
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    public function register() 
    {
        $states = Cities::getStates();
        
        //$cities = Cities::getCities();
        
        $page_title = "Association Registration";
        
        return view('front.association.register',compact("page_title","states","cities"));
    }
    
    public function insert(Request $request) 
    {
        
        $validation = array(
            'name' => 'required|max:255',
            'phone' => 'required|max:255',
            'email' => 'required|email|max:255|unique:association',

            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $name=$request->name;
            $asso = new Association;
            $asso->name = $request->name;
            if (isset($request->cities)) {
                //$cities = implode(",", $request->cities);
                $asso->city = $request->cities;
            }
            if (isset($request->states)) {
                //$states= implode(",", $request->states);
                $asso->state = $request->states;
            }
            $asso->zip_code = $request->zip_code;
            $asso->contact_person = $request->contact_person;
            $asso->position = $request->position;
            $asso->phone = $request->phone;
            $asso->phone2 = $request->phone2;
            $asso->email = $request->email;
            $asso->advertisement = (isset($request->advertisement))?$request->advertisement:0;
            $asso->status = 0;
            $asso->save();
            $id = $asso->id;
            
            Session::put("id",$id);
            Session::put("name",$name);
            
            return redirect('association/profile');
        }
        
       
    }
    public function profile() 
    {
        $page_title = "Association Profile";
        
        $id = Session::get("id");
        $name = Session::get("name");
        
        return view('front.association.profile',compact("page_title","id","name"));
    }
    public function profileInsert(Request $request) 
    {
        $id=$request->id;
        $assoc = Association::findOrFail($id);
        $input = $request->all();
        unset($input['_token']);
        $validation = array(
            'id' => 'required|max:255',
            'name' => 'required|max:255', 

            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $input["name"] = $request->name;
            $input["website_url"] = $request->website_url;
            $input["address"] = $request->address;
            $input["google_map"] = $request->google_map;
            $input["no_of_bulidings"] = $request->no_of_bulidings;
            $input["no_of_units"] = $request->no_of_units;
            $input["association_type"] = $request->association_type;
            
            
            if($input["username"]!="")
            {
                
                Session::put("email",$input["email"]);
                Session::put("username",$input["username"]);
                
            }
            unset($input["username"]);
            unset($input["email"]);
            
            $affectedRows = Association::where('id', '=', $id)->update($input);
            
            Session::put("id",$id);
            Session::put("name",$input["name"]);
            
            return redirect('association/additional-user');
        }
        
       
    }
    public function additionalUser() 
    {
        $page_title = "Additional User";
        
        $id = Session::get("id");
        $name = Session::get("name");
        
        $email = Session::get("email");
        $username = Session::get("username");
        
        return view('front.association.additional',compact("page_title","id","name","email","username"));
    }
    public function additionalInsert(Request $request) 
    {
        $input = $request->all();
         
        $validation = array(
            'username' => 'required|max:255',
            'phone' => 'required|max:255',
            'email' => 'required|email|max:255|unique:additional_user',

            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $email = $request->email;
            $name = $request->username;
            
            $user = new User;
            $user->name = $name;
            $user->email = $email;
            $user->status = 0;
            $user->role_id = 2;
            $user->save();
            $user_id = $user->id;
            
            if(isset($request->add_user))
            {
                if($request->add_user==1)
                {
                    $addit = new AdditionalUser;
                    $addit->username = $request->username;
                    $addit->email = $request->email;
                    $addit->association_id = $request->id;
                    $addit->user_id = $user_id;
                    $addit->phone = $request->phone;
                    $addit->association_name = $request->association_name;
                    $addit->position = $request->position;
                    $addit->add_user = (isset($request->add_user))?$request->add_user:0; 
                    $addit->status = 0;
                    $addit->save();              
                }
            }
            $update = Association::where("id", "=", $request->id)
                    ->update(["user_id" => $user_id]);
            
            return redirect('association/packages');
        }
        
        /*else
        {
            return redirect('association/packages');
        }*/
        
       
    }
    public function packages() 
    {
        $page_title = "Membership Packages";
        
        $id = Session::get("id");
        
        $date = date("m-d-Y",strtotime('+1 months'));
        
        $packages = Packages::getAssociatePackage();
        
        return view('front.association.packages',compact("page_title","id","date","packages"));
    }

    
}
