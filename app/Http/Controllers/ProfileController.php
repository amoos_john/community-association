<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Input as Input;
use Validator,
    Redirect,
    Session,
    Config;
use App\Functions\Functions;
use App\Association;
use App\Profile;
use App\Cities;
use App\ServiceProvider;
use App\ServiceArea;

class ProfileController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | ProfileController Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function edit(Request $request) 
    {
        $page_title = "Contact Information";
        
        $user_id = Auth::user()->id;
        
        $association_id = Association::getAssociationId($user_id);
        
        $model = Association::find($association_id);
        
        $states = Cities::getStates();
        /*$cities = Cities::getCities();
        $sel_cities = explode(",",$model->city);
        $sel_states = explode(",",$model->state);*/
        
        return view('front.association.profile.edit',compact("model","sel_cities","sel_states","page_title","states","cities"));
    }
    
    public function update(Request $request,$id) 
    {
        $input = $request->all();
        
        $assoc = Association::findOrFail($id);
        unset($input['_token']);
        if($input["password"]=='')
        {
           $validation = array(
            'name' => 'required|max:255',
            'phone' => 'required|max:20',
            'email' => 'required|email|max:255|unique:association,email,' . $assoc->id,

            );
           $validator = Validator::make($request->all(), $validation);
           
            if ($validator->fails()) {

                return redirect()->back()->withErrors($validator->errors())->withInput();

            }
            unset($input['password']);
            unset($input['password_confirmation']);
            $input['name'] = $request->name;
            if (isset($request->cities)) {
                //$cities = implode(",", $request->cities);
                $input['city'] = $request->cities;
                unset($input['cities']);
            }
            if (isset($request->states)) {
                //$states= implode(",", $request->states);
                $input['state'] = $request->states;
                unset($input['states']);
            }
            $input['zip_code'] = $request->zip_code;
            $input['contact_person'] = $request->contact_person;
            $input['position'] = $request->position;
            $input['phone'] = $request->phone;
            $input['phone2'] = $request->phone2;
            $input['email'] = $request->email;
            $input['advertisement'] = (isset($request->advertisement))?$request->advertisement:0;
            
            $update = Association::where("id","=",$id)->update($input);
            \Session::flash('success', 'Profile Update Successfully!');
            return redirect("association/profile/edit");
           
        }
        else
        {
            $validation = array(
            'name' => 'required|max:255',
            'phone' => 'required|max:20',
            'email' => 'required|email|max:255|unique:association,email,' . $assoc->id,
            'password' => 'required|confirmed|min:6',

            );
       
            $validator = Validator::make($request->all(), $validation);

            if ($validator->fails()) {

                return redirect()->back()->withErrors($validator->errors())->withInput();

            }
            unset($input['password']);
            unset($input['password_confirmation']);
            $input['name'] = $request->name;
            if (isset($request->cities)) {
                $cities = implode(",", $request->cities);
                $input['city'] = $cities;
                unset($input['cities']);
            }
            if (isset($request->states)) {
                $states= implode(",", $request->states);
                $input['state'] = $states;
                unset($input['states']);
            }
            $input['zip_code'] = $request->zip_code;
            $input['contact_person'] = $request->contact_person;
            $input['position'] = $request->position;
            $input['phone'] = $request->phone;
            $input['phone2'] = $request->phone2;
            $input['email'] = $request->email;
            $input['advertisement'] = (isset($request->advertisement))?$request->advertisement:0;
            
            $update = Association::where("id","=",$id)->update($input);
            

	    $input_user['password'] = bcrypt($request->password);
            
            $affectedRows = User::where('id', '=', $assoc->user_id)->update($input_user);
            
            
            \Session::flash('success', 'Profile Update Successfully!');
            return redirect("association/profile/edit");
        }
        
    }
    
    public function editProvider(Request $request) 
    {
        $page_title = "Service Provider Profile";
        
        $user_id = Auth::user()->id;
        $provider_id = ServiceProvider::getServiceProviderId($user_id);
        
        $model = User::find($user_id);
        $provider = ServiceProvider::find($provider_id);
        $areas = ServiceArea::where('status', '=', 1)
                 ->orderby("id", "name")->get();
        
        return view('front.serviceprovider.profile.edit',compact("model","areas","page_title","provider"));
    }
    public function updateProvider(Request $request,$id) 
    {
        $input = $request->all();
        
        $id = $request->id;
        $user = User::findOrFail($id);
        unset($input['_token']);
        
        
        $message = [];
        if(isset($input["logo"]))
        {
            $validation['logo'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg|max:5000';
            $message = [
                'logo.max' => 'logo size must be less or equal to 5MB.'
            ];
        }
        
        if($input["password"]=='')
        {
           $validation = array(
            'name' => 'required|max:255',
            'first_name' => 'required|max:255',     

            );
           $validator = Validator::make($request->all(), $validation,$message);
           
            if ($validator->fails()) {

                return redirect()->back()->withErrors($validator->errors())->withInput();

            }
            unset($input['password']);
            unset($input['password_confirmation']);
            unset($input['logo']);
            unset($input['service_id']);
            $affectedRows = User::where('id', '=', $id)->update($input);
        }
        else
        {
            $validation = array(
            'name' => 'required|max:255',
            'first_name' => 'required|max:255',     
            'password' => 'required|confirmed|min:6',

            );
       
            $validator = Validator::make($request->all(), $validation,$message);

            if ($validator->fails()) {

                return redirect()->back()->withErrors($validator->errors())->withInput();

            }
            
            unset($input['password_confirmation']);

	    $input['password'] = bcrypt($request->password);
            unset($input['logo']);
            $affectedRows = User::where('id', '=', $id)->update($input);
            
            
        }
        if($request->logo!="")
        {
            $logo = Input::file('logo');
            $path='uploads/serviceprovider/logo/';
            $destinationPath = public_path() . '/'.$path;
            $logoName = Functions::saveImage($logo, $destinationPath,"");

            $input2["logo"] = $path.$logoName; 
            
        }
        $input2["service_id"] = (isset($request->service_id))?$request->service_id:0; 
        $update = ServiceProvider::where("user_id", '=', $id)->update($input2);
        
        \Session::flash('success', 'Profile Update Successfully!');
        return redirect("vendor/profile/edit");
        
    }
}
