<?php

namespace App\Http\Controllers\Payments;

use Session;
use DB,Auth,Config,Mail;
use App\Billing;
use App\Content;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator, Input, Redirect; 
use App\Payments;
use App\Packages;
use App\Functions\Functions;
use Illuminate\Http\Request;
use App\AssociationSubscriptions;
use App\VendorSubscriptions;
use App\User;
use App\Association;
use App\ServiceProvider;

class PaymentController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   
    public function index(Request $request) {
        
        $billing_id = Session::get('bill_id');
        $subscription_id = Session::get('sub_id');
        $check = Payments::where('billing_id','=',$billing_id)->count();
        
        if($check==0)
        {
            $billing = Billing::with('packages')->find($billing_id);
            $charge = Session::get('card_info');
            $vendor = Session::get('vendor');
            $model=new Payments();
            $model->billing_id=$billing_id;
            $model->subscription_id=$subscription_id;
            $model->card_no=$charge['card_no'];
            $model->card_name=$charge['card_name'];
            $model->month=$charge['month'];
            $model->year=$charge['year'];
            $model->cvc=$charge['cvc_no'];
            $model->save();
            
            $affectedRows = Billing::where('id','=',$billing_id)->update(array('payment_status' => 'success'));
            
            if(!isset($vendor))
            {
                if(isset(Auth::user()->id))
                {
                    $updateRows = AssociationSubscriptions::where("user_id","=",$billing->user_id)->update(["subscription_date"=>2]);
                }
                else
                {
                    $update = Association::where("user_id", "=", $billing->user_id)
                    ->update(["status" => 1]);
                }
                $date = date("Y-m-d", strtotime('+1 months'));
                $sub = new AssociationSubscriptions;
                $sub->package_id = $billing->package_id;
                $sub->billing_id = $billing_id;
                $sub->user_id = $billing->user_id;
                $sub->total_rfp = (count($billing->packages)>0)?$billing->packages->total_rfp:0;
                $sub->subscription_date = $date;
                $sub->subscription_status = 1;
                $sub->save();
                
            }
            elseif(isset($vendor))
            {
                if(isset(Auth::user()->id))
                {
                    $updateRows = VendorSubscriptions::where("user_id","=",$billing->user_id)>update(["subscription_date"=>2]);
                }
                else
                {
                    $update = ServiceProvider::where("user_id", "=", $billing->user_id)
                    ->update(["status" => 1]);
                }
                $date = date("Y-m-d", strtotime('+1 months'));
                $sub = new VendorSubscriptions;
                $sub->package_id = $billing->package_id;
                $sub->billing_id = $billing_id;
                $sub->user_id = $billing->user_id;
                $sub->type = 1;
                $sub->subscription_date = $date;
                $sub->subscription_status = 1;
                $sub->save();
                
                Session::forget("vendor");
            }
            $from = Config::get("params.server_email");
            if(isset(Auth::user()->id))
            {
                $package = Packages::find($billing->package_id);
                $name = Auth::user()->name;
                $email = Auth::user()->email;
                $content = Content::where("code","=","package_upgrade")->get();
                $replaces['NAME'] = $name;
                $replaces['PACKAGE'] = $package->package_name;
                $replaces['PRICE'] = $package->price;

                $template = Functions::setEmailTemplate($content, $replaces);

                $subject = $template["subject"];
                $body = $template["body"];

                $send_email = Functions::sendEmail($email,$subject,$body,'',$from);
            }
            else
            {
                $password = Functions::generateRandomString();
                $model = User::where("id", "=", $billing->user_id)
                    ->update(["password" => bcrypt($password),"status" => 1]);
                
                
                $user = User::find($billing->user_id);
                $content = Content::where("code", "=", "register")->get();
                $email = $user->email;
                $replaces['NAME'] = $user->name;
                $replaces['EMAIL'] = $email;
                $replaces['PASSWORD'] = $password;

                $template = Functions::setEmailTemplate($content, $replaces);

                $subject = $template["subject"];
                $body = $template["body"];
                /* $data= [
                  'from' =>$from,
                  'body' => $body,
                  'subject' => $subject,
                  'email_send' =>$email

                  ];

                  Mail::send('email.register',['data' => $data], function($message) use ($data) {

                  $message->from($data['from']);
                  $message->to($data['email_send'])->subject($data['subject']);
                  }); */
                $send_email = Functions::sendEmail($email, $subject, $body, '', $from);
            }
            Session::forget("bill_id");
            Session::forget("sub_id");
            Session::forget("card_info");
        }
        
        return view('front.payments.thankyou');
    }
    public function cancel() {

        return view('front.payments.cancel');
    }

}
