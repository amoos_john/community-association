<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator,
    Input,
    Redirect,
    Session,
    Config;
use App\Functions\Functions;
use App\Properties;
use App\GalleryImage;
use App\Amenities;
use App\RatesandAvailability;

class SearchController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */
    protected $layout = 'layouts.search';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    public function index(Request $request) {
        
        
        $city = $request->input('city');
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $guest = $request->input('guest');
        $results_per_page=$request->input('guest');
       
        $search['city']=$city;
        $search['start_date']=$start_date;
        $search['end_date']=$end_date;
        $search['guest']=$guest;
        $search['results_per_page']=$results_per_page;
        
        $properties=Properties::getProperties($search);
        
        $page_title='Search Vacation Rentals';
        
        if($start_date!='')
        {
            //$request->session()->put('start_date', $start_date);
            Session::put('start_date',$start_date);
        }
        else
        {
            Session::forget('start_date');
        }
        if($end_date!='')
        {
            Session::put('end_date',$end_date);
        }
        else
        {
            Session::forget('end_date');
        }
        if($guest!='')
        {
            Session::put('guest',$guest);
        }
        else
        {
            Session::forget('guest');
        }
        
        $parents = Amenities::with("child")->get();
        
       
        
        return view('front.rentals',compact("properties","page_title","city","start_date","end_date","guest","results_per_page","parents"));
        
    }
    public function result(Request $request) {
        
        $city = $request->input('city');
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $guest = $request->input('guest');
        $price_max = $request->input('price_max');
        $price_min = $request->input('price_min');
        $results_per_page=$request->input('results_per_page');
        $sort = $request->input('sort');
       
        $search['city']=$city;
        $search['start_date']=$start_date;
        $search['end_date']=$end_date;
        $search['guest']=$guest;
        $search['results_per_page']=$results_per_page;
        $search['price_min']=$price_min;
        $search['price_max']=$price_max;
        $search['sort']=$sort;
        
        if($start_date!='')
        {
            //$request->session()->put('start_date', $start_date);
            Session::put('start_date',$start_date);
        }
        else
        {
            Session::forget('start_date');
        }
        if($end_date!='')
        {
            Session::put('end_date',$end_date);
        }
        else
        {
            Session::forget('end_date');
        }
        if($guest!='')
        {
            Session::put('guest',$guest);
        }
        else
        {
            Session::forget('guest');
        }
        
        $properties=Properties::getProperties($search);
        
        
        if ($request->ajax()) {
            return view('front.result',compact("properties","page_title","city","start_date","end_date","guest","results_per_page"))->render();  
        }
        
        return view('front.result',compact("properties","page_title","city","start_date","end_date","guest","results_per_page"));
        
    }
    public function detail(Request $request) {
        
        $id=$request->id;
        $property=Properties::with("categories")->with("property_amentity")->where("status","=",1)
        ->where("id","=",$id)->get();
        
        $gallery_id=(count($property)>0)?$property[0]->gallery_id:'';
        
        $gallery_images=GalleryImage::where("gallery_id","=",$gallery_id)->orderby("display_order","ASC")->get();
        
        $parents = Amenities::with("child")->get();
        
        $page_title=(count($property)>0)?$property[0]->public_headline:'Details';
        
        $avails=RatesandAvailability::where("property_id",$id)->where("availability",0)->get();
        
        $start_date=Session::get('start_date');
        $end_date=Session::get('end_date');
        $guest=(Session::get('guest'))?Session::get('guest'):'2';
        
        $date1=date_create($start_date);
        $date2=date_create($end_date);
        $diff=date_diff($date1,$date2);
        $date_diff=$diff->format("%a");
        $min_stay=(count($property)>0)?$property[0]->min_stay:'';
        
        return view('front.details',compact("property","start_date","end_date","date_diff","min_stay","guest","gallery_images","id","parents","page_title","avails"));
    }
    public function bookingval(Request $request) 
    {
        $id=$request->id;
        $start_date=$request->start_date;
        $end_date=$request->end_date;
        $guest=$request->guest;
        
        $property=Properties::where("status","=",1)
        ->where("id","=",$id)->get();
        
        $date1=date_create($start_date);
        $date2=date_create($end_date);
        $diff=date_diff($date1,$date2);
        $date_diff=$diff->format("%a");
        $min_stay=(count($property)>0)?$property[0]->min_stay:'';
        
        if($start_date=='' &&  $end_date=='')
        {
            echo '<button class="btn btn-primary btn-detail">
            Please specify your dates of stay.
            </button>';
        }
        elseif($end_date<=$start_date)
        {
            echo '<button class="btn btn-primary btn-detail">
            CheckOut must be greater than CheckIn.            
            </button>';
        }
        elseif($date_diff<$min_stay)
        {
            echo '<button class="btn btn-primary btn-detail">
            This Property has a minimum stay of '.$min_stay.' nights.
            </button>';
        }
        else
        {
            echo '1';
        }
        
    }
    
}
