<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Input as Input;
use Validator,
    Redirect,
    Session,
    Config;
use App\Functions\Functions;
use App\ServiceProvider;
use App\Packages;
use App\VendorSubscriptions;
use App\Gallery;
use App\PackageCombination;

class GalleryController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Gallery Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    public function index() 
    {
        if(Auth::user()->role_id==3)
        {
            $page_title = "Gallery";
            $user_id = Auth::user()->id;
            $symbol = Config::get("params.symbol");
           
            $packages = Packages::where("status","=",1)->where("type","=",3)->get();
            $provider = ServiceProvider::where("user_id","=",$user_id)->first();
                        
            return view('front.serviceprovider.gallery.index',compact("page_title","provider","packages","symbol"));
        }
        else 
        {
            
          return redirect('vendor/dashboard');

        }
        
    }
    public function create() 
    {
        $user_id = Auth::user()->id;
        $subcribe = VendorSubscriptions::getGallerySubcription($user_id);
      
        if(count($subcribe)==0)
        {
            $page_title = "Create Gallery";
            $photos = (count($subcribe)>0)?$subcribe->total_rfp:0;
            
            return view('front.serviceprovider.gallery.create',compact("page_title","photos"));
        }
        else
        {
            return redirect('vendor/gallery');
        }
        
    }
    public function insert(Request $request) 
    {
        $input = $request->all();
        $validation = array(
            'title' => 'required|max:255',
            'description' => 'required|max:500',
            );
        $validation['image'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg|max:5000';
        $message = [
            'image.max' => 'Image size must be less or equal to 5MB.'
        ];
        $validator = Validator::make($request->all(), $validation,$message);
        
        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $user_id = Auth::user()->id;
            /*$subcribe = VendorSubscriptions::getGallerySubcription($user_id);
            $package = PackageCombination::where("billing_id","=",$subcribe->billing_id)
                       ->orderby("id","desc")->first();*/
            $image = Input::file('image');
            $path='uploads/serviceprovider/gallery_images/';
            $destinationPath = public_path() . '/'.$path;
            $imageName = Functions::saveImage($image, $destinationPath,"");
            
            $gallery = new Gallery;
            $gallery->title = $request->title;
            $gallery->packagecombine_id = 2;//$package->id            
            $gallery->description = $request->description;
            $gallery->image = $path.$imageName;
            $gallery->created_by = $user_id;
            $gallery->status = 1;
            $gallery->save();
            
            /*$total_rfp = $subcribe->total_rfp - 1;
            $update = VendorSubscriptions::where('id','=',$subcribe->id)->update(["total_rfp"=>$total_rfp]);
             * 
             */
            
            return redirect('vendor/gallery')->with('success','Gallery created successfully!');
            
        }
    }
    
    
    

    
}
