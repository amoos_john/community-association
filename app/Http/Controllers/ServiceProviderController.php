<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Input as Input;
use Validator,
    Redirect,
    Session,
    Config,
    Mail;
use App\Functions\Functions;
use App\ServiceProvider;
use App\Cities;
use App\Packages;
use App\Content;
use App\ServiceArea;
use App\Cart;
use App\PackageCart;
use App\AdditionalUser;

class ServiceProviderController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | ServiceProvider Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    protected $session_id;

    public function __construct() {
        //$this->middleware('guest', ['except' => ['logout', 'getLogout']]);
        session_start();
        $this->session_id = session_id();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function packages(Request $request) {
        $package_id = (isset($request->package_id)) ? $request->package_id : '';
        $package = Packages::where("type","=",2)->where('id','=',$package_id)->first();
        
        $page_title = "Services Provider Packages";
        $date = date("m-d-Y", strtotime('+1 months'));
        $packages = Packages::getServiceProviderPackage();

        if (count($package)>0) {
            
            $areas = ServiceArea::where('status', '=', 1)
                            ->orderby("id", "desc")->limit(12)->get();

            $sessionId = $this->session_id;

            $states = Cities::getStates();
            $gallery_packages = Packages::where("status", "=", 1)->where("type", "=", 3)->get();

            $pack = Packages::find($package_id);

            $length = $pack->total_rfp;
            
            $session_id = $this->session_id;
            $package_cart = Cart::where("session_id", "=", $sessionId)->get();
           
            if (count($package_cart) > 0) {
                if ($package_cart[0]->package_id != $package_id) {
                    PackageCart::where("session_id", "=", $sessionId)->delete();
                    Cart::where("session_id", "=", $sessionId)->delete();
                    session_regenerate_id();
                   
                }
            }
           return view('front.serviceprovider.packages', compact("page_title", "gallery_packages", "packages", "length", "package_id", "areas", "states", "cities", "date", "packages"));

        } elseif($package_id=='') {
           return view('front.serviceprovider.packages', compact("page_title", "gallery_packages", "packages", "length", "package_id", "areas", "states", "cities", "date", "packages"));

        }
    else {
        return redirect('/');

    }

    }

    public function createPackage(Request $request) {
        $sessionId = $this->session_id;
        $package_id = $request->package_id;
        $validation = array(
            'areas' => 'required',
            'states' => 'required',
        );
        $response['error'] = 0;
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {

            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
        } else {
            $states = $request->states;
            $areas = $request->areas;
            $counties = $request->counties;
            $created_at = date("Y-m-d H:i:s");

            /* $data = array(); */
            $i = 0;
            foreach ($states as $state) {
                $gallery = "";
                if (isset($request->gallery)) {

                    $gallery = ($request->gallery != 0) ? $request->gallery : 0;
                }
                $county = "";
                if (isset($counties)) {
                    $county = implode(",", $counties);
                }
                $area = "";
                if (isset($areas)) {
                    $area = implode(",", $areas);
                }
                $cart = new Cart;
                $cart->session_id = $sessionId;
                $cart->package_id = $package_id;
                $cart->service_areas = $area;
                $cart->states = $state;
                $cart->county = $county;
                $cart->gallery = $gallery;
                $cart->save();
                /* $data = ["session_id"=>$sessionId,"states"=>$state,
                  "county"=>$county,"created_at"=>$created_at]; */
                $i++;
            }

            /*
             * if(count($data)>0)
              {
              Cart::create($data);
              } */
        }
        if ($response['error'] == 1) {
            foreach ($response['errors']->all() as $error) {
                echo '<p>' . $error . '</p>';
            }
        } else {
            echo '1';
        }
    }

    public function insertPackage(Request $request, $package_id) {
        $sessionId = $this->session_id;

        if ($package_id == "") {

            return redirect()->back()->withInput();
        } else {
            $package = Packages::find($package_id);

            $cart = new PackageCart;
            $cart->session_id = $sessionId;
            $cart->package_id = $package_id;
            $cart->grand_total = $package->price;
            $cart->save();

            if (isset(Auth::user()->id)) {
                return redirect('vendor/billing');
            } else {
                return redirect('vendor/register');
            }
        }
    }

    public function getCart(Request $request) {
        $sessionId = $this->session_id;
        $package_id = $request->package_id;
        $model = Cart::with("getStates")->where("session_id", "=", $sessionId)->get();
        $total_cart = count($model);

        $pack = Packages::find($package_id);

        $length = $pack->total_rfp - $total_cart;

        $area_count = ($length != 0) ? $pack->total_rfp : 0;

        return view('front.serviceprovider.packagecart', compact("model", "area_count", "package_id", "pack", "length"));
    }

    public function getCounties(Request $request) {
        $state_code = $request->state_code;

        if ($state_code != "") {
            $counties = Cities::getCounties($state_code);

            if (count($counties) > 0) {
                foreach ($counties as $row) {
                    echo '<div class="form-group">
                    <input type="checkbox"  name="counties[]" value="' . $row->county . '" id="' . $row->county . '" /><mark></mark>
                    <label for="' . $row->county . '">' . $row->county . '</label>
                    </div>';
                }
            }
        }
    }

    public function index() {
        $page_title = "Service Provider Registration";

        $sessionId = $this->session_id;

        $states = Cities::getStates();

        //$cities = Cities::getCities();

        $cart_package = PackageCart::with("package")->where("session_id", "=", $sessionId)->first();

        return view('front.serviceprovider.register', compact("page_title", "cart_package", "states"));
    }

    public function insert(Request $request) {

        $validation = array(
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'phone' => 'required',
        );

        $validation['logo'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg|max:5000';
        $message = [
            'logo.max' => 'logo size must be less or equal to 5MB.'
        ];

        $validation['license'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg,
                doc,docx,ppt,pptx,txt,xls,xlsx,csv,ods,pdf|max:5000';
        $message = [
            'license.max' => 'License size must be less or equal to 5MB.'
        ];
        if (isset($input["certificate"])) {
            $validation['certificate'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg,
                doc,docx,ppt,pptx,txt,xls,xlsx,csv,ods,pdf|max:5000';
            $message = [
                'certificate.max' => 'Certificate size must be less or equal to 5MB.'
            ];
        }
        if (isset($input["reference"])) {
            $validation['reference'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg,
                doc,docx,ppt,pptx,txt,xls,xlsx,csv,ods,pdf|max:5000';
            $message = [
                'reference.max' => 'Reference size must be less or equal to 5MB.'
            ];
        }
        if (isset($input["document"])) {
            $validation['document'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg,
                doc,docx,ppt,pptx,txt,xls,xlsx,csv,ods,pdf|max:5000';
            $message = [
                'document.max' => 'Document size must be less or equal to 5MB.'
            ];
        }
        $validator = Validator::make($request->all(), $validation, $message);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
           
            $logo = Input::file('logo');
            $logoPath = 'uploads/serviceprovider/logo/';
            $destinationPath = public_path() . '/' . $logoPath;
            $logoName = Functions::saveImage($logo, $destinationPath, "");

            $file = Input::file('license');
            $path = 'uploads/serviceprovider/license/';
            $destinationPath = public_path() . '/' . $path;
            $licenseName = Functions::saveImage($file, $destinationPath, "");

            $name = $request->name;
            $service = new ServiceProvider;
            $service->name = $name;
            $service->address = $request->address;
            $service->address2 = $request->address2;
            if (isset($request->cities)) {
                //$cities = implode(",", $request->cities);
                $service->city = $request->cities;
            }
            if (isset($request->states)) {
                //$states = implode(",", $request->states);
                $service->state = $request->states;
            }
            $service->zip_code = $request->zip_code;
            $service->phone = $request->phone;
            $service->emergency_phone = $request->emergency_phone;
            $service->fax = $request->fax;
            $service->website_url = $request->website_url;
            $service->description = $request->description;
            $service->logo = $logoPath . $logoName;
            $service->license_name = $request->license_name;
            $service->license = $path . $licenseName;
            $service->certificate_name = $request->certificate_name;
            $service->reference_name = $request->reference_name;
            $service->document_name = $request->document_name;
            if (isset($input["certificate"])) {
                $file = Input::file('certificate');
                $path = 'uploads/serviceprovider/certificate/';
                $destinationPath = public_path() . '/' . $path;
                $docName = Functions::saveImage($file, $destinationPath, "");
                $service->certificate = $path . $docName;
            }
            if (isset($input["reference"])) {
                $file = Input::file('reference');
                $path = 'uploads/serviceprovider/reference/';
                $destinationPath = public_path() . '/' . $path;
                $docName = Functions::saveImage($image, $destinationPath, "");
                $service->reference = $path . $docName;
            }
            if (isset($input["document"])) {
                $document = Input::file('document');
                $docPath = 'uploads/serviceprovider/documents/';
                $destinationPath = public_path() . '/' . $docPath;
                $documentName = Functions::saveImage($document, $destinationPath);
                $service->document = $docPath . $documentName;
            }

            $service->status = 1;
            $service->save();
            $id = $service->id;
            Session::put("id", $id);
            Session::put("name", $name);
            
            /*$data = array();
            $username = (count($request->username)>0)?$request->username:0;
            if(count($username)>0)
            {
                $contact_phone = $request->contact_phone;
                $cell_phone = $request->cell_phone;
                $email = $request->email;
                $i = 0;
                foreach($username as $user)
                {
                    $contact_phone = $contact_phone[$i];
                    $cell_phone = $cell_phone[$i];
                    $email = $email[$i];
                    
                    $data[] = array('serviceprovider_id' => $id, 'username' => $user,
                        'created_at' => date('Y-m-d H:i:s'));
                    $i++;
                }
            }
           
            if (count($data) > 0) {
            AdditionalUser::insert($data);
            }
            */
            return redirect('vendor/profile');
        }
    }

    public function profile() {
        $page_title = "Service Provider Profile";

        $id = Session::get("id");
        $name = Session::get("name");

        return view('front.serviceprovider.profile', compact("page_title", "id", "name"));
    }

    public function profileInsert(Request $request) {
        $input = $request->all();

        $validation = array(
            'company_name' => 'required|max:255',
            'address' => 'required|max:255',
            'phone' => 'required|max:20',
            'email' => 'required|email|max:255|unique:users',
        );

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $email = $request->email;
            $name = $request->company_name;

            $user = new User;
            $user->name = $name;
            $user->email = $email;
            //$user->password = bcrypt($password);
            $user->phone = $request->phone;
            $user->address = $request->address;
            $user->website_url = $request->website_url;
            $user->role_id = 3;
            $user->save();
            $user_id = $user->id;
            Session::put("user_id", $user_id);
            Session::save();

            $update = ServiceProvider::where("id", "=", $request->id)
                    ->update(["user_id" => $user_id]);
            return redirect('vendor/billing');
        }

        /* else
          {
          return redirect('vendor/packages');
          } */
    }

}
