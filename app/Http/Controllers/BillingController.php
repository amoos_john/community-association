<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator,
    Input,
    Redirect,
    Session,
    Config,
    Mail;
use App\Functions\Functions;
use App\Association;
use App\Billing;
use App\Content;
use App\User;
use App\Packages;
use App\BillingInformation;
use App\PackageCart;
use App\Cart;
use App\ServiceProvider;
use App\Functions\AuthorizeNet;
use App\PackageCombination;

class BillingController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | BillingController
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    protected $session_id;

    public function __construct() {
        session_start();
        $this->session_id = session_id();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index(Request $request) {
        $page_title = "Billing Information";
        $months = Functions::getMonths();
        $year = date('Y');

        if (isset(Auth::user()->id)) {
            $user_id = Auth::user()->id;
            $association_id = Association::getAssociationId($user_id);

            $assoc = Association::find($association_id);
            $model = BillingInformation::join("billing","billing.id","=","billing_information.billing_id")
                    ->select("billing.user_id","billing_information.*")
                    ->where('billing.user_id','=',$user_id)->orderby('billing_information.id','desc')->first();
            
            $id = $association_id;
            $name = $assoc->name;
        } else {
            $id = Session::get("id");
            $name = Session::get("name");
        }

        $package_id = $request->package_id;

        return view('front.association.billing', compact("page_title","user_id","model", "months", "year", "id", "name", "package_id"));
    }

    public function insert(Request $request, $package_id) {
        $validation = array(
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            
            'g-recaptcha-response' => 'required|captcha',
            'terms' => 'required',
            'payment_type' => 'required|min:1',
        );
        //'password' => 'required|confirmed|min:6', 'email' => 'required|email|max:255',
        if ($request->payment_type == 1) {
            $validation = array(
                'card_name' => 'required|max:50',
                'card_no' => 'required|max:19',
                'cvc_no' => 'required',
                'month' => 'required',
                'year' => 'required',
            );
        }
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            DB::beginTransaction();
            try {
                $payment_method = Billing::$payment_method;
                if (array_key_exists($request->payment_type, $payment_method)) {
                   $payment_type = $payment_method[$request->payment_type];
                }
                $assoc_id = $request->id;
                $package = Packages::find($package_id);
                $assoc = Association::find($assoc_id);
                
                $bill = new Billing;
                $bill->user_id = $assoc->user_id;
                $bill->package_id = $package_id;
                $bill->grand_total = $package->price;
                $bill->payment_method = $payment_type;
                $bill->payment_status = 'pending';
                $bill->status = 0;
                $bill->save();

                $bill_id = $bill->id;

                $info = new BillingInformation;
                $info->billing_id = $bill_id;
                $info->username = $request->username;
                //$info->email = $request->email;
                //$info->password = bcrypt($request->password);
                $info->address = $request->address;
                $info->address2 = $request->address2;
                $info->phone = $request->phone;
                $info->city = $request->city;
                $info->state = $request->state;
                $info->zip_code = $request->zip_code;
                $info->save();

                $card_no = str_replace(" ", "", $request->card_no);
                $data = ["card_name" => $request->card_name, "card_no" => $card_no,
                    "cvc_no" => $request->cvc_no, "month" => $request->month, "year" => $request->year,
                    "package_name" => $package->package_name, "total" => $package->price,
                    "description" => $package->description];

                if ($request->payment_type == 1) {
                    $response = AuthorizeNet::createSubscription($data);
                    if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
                        
                        DB::commit();
                        Session::forget("id");
                        Session::forget("name");
                        $subscription_id = $response->getSubscriptionId();
                        Session::put("bill_id", $bill_id);
                        Session::put("sub_id", $subscription_id);
                        Session::put("card_info", $data);
                        Session::save();
                        $message = "SUCCESS: Subscription ID : " . $subscription_id . "\n";
                        return redirect('payment/thank-you')->with("success", $message);
                    } else {
                        DB::rollBack();
                        $error = "ERROR :  Invalid response\n";
                        $errorMessages = $response->getMessages()->getMessage();
                        $error .= "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
                        return redirect('payment/cancel')->with("error", $error);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return redirect('billing/cancel');
            }
        }
    }

    /* Vendor */

    public function indexPackage(Request $request) {

        $page_title = "Billing Information";
        $months = Functions::getMonths();
        $year = date('Y');

        if (isset(Auth::user()->id)) {
            $user_id = Auth::user()->id;
            $sessionId = $this->session_id;
            $package = PackageCart::where("session_id", "=", $sessionId)->first();
            $package_id = $package->package_id;
            $code = $request->code;
            $provider = ServiceProvider::where("user_id", "=", $user_id)->first();
            $model = BillingInformation::join("billing","billing.id","=","billing_information.billing_id")
                    ->select("billing.user_id","billing_information.*")
                    ->where('billing.user_id','=',$user_id)->orderby('billing_information.id','desc')->first();
            
            $id = $provider->id;
            $name = $provider->name;
        } else {
            $id = Session::get("id");
            $name = Session::get("name");

            $sessionId = $this->session_id;
            $package = PackageCart::where("session_id", "=", $sessionId)->first();
            $package_id = $package->package_id;
        }

        return view('front.serviceprovider.billing', compact("page_title","model", "user_id","months", "year", "id", "name", "package_id"));
    }

    public function insertPackage(Request $request, $package_id) {
        $validation = array(
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed|min:6',
            'g-recaptcha-response' => 'required|captcha',
            'terms' => 'required',
            'payment_type' => 'required',
        );

        if ($request->payment_type == 2) {
            $validation = array(
                'card_name' => 'required|max:50',
                'card_no' => 'required|max:19',
                'cvc_no' => 'required',
                'month' => 'required',
                'year' => 'required',
            );
        }

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            DB::beginTransaction();
            try {
                $payment_type = $request->payment_type;
                $payment_method = Billing::$payment_method;
                if (array_key_exists($request->payment_type, $payment_method)) {
                   $payment_name = $payment_method[$request->payment_type];
                }
                $serviceprovider_id = $request->id;
                $user_id = (isset(Auth::user()->id)) ? Auth::user()->id : Session::get("user_id");
                $package = Packages::find($package_id);
                $bill = new Billing;
                $bill->user_id = $user_id;
                $bill->package_id = $package_id;
                $bill->grand_total = $package->price;
                $bill->payment_method = $payment_name;
                $bill->payment_status = 'pending';
                $bill->status = 0;
                $bill->save();

                $bill_id = $bill->id;

                $info = new BillingInformation;
                $info->billing_id = $bill_id;
                $info->username = $request->username;
                $info->email = $request->email;
                $info->password = bcrypt($request->password);
                $info->address = $request->address;
                $info->address2 = $request->address2;
                $info->phone = $request->phone;
                $info->city = $request->city;
                $info->state = $request->state;
                $info->zip_code = $request->zip_code;
                $info->save();

                if (isset(Auth::user()->id)) {
                    if (isset($request->code)) {
                        if ($request->code != "gallery") {
                            /*$update = ServiceProvider::where("id", "=", $serviceprovider_id)
                                    ->update(["package_id" => $package_id]);*/
                        } else {
                            /*$update = ServiceProvider::where("id", "=", $serviceprovider_id)
                                    ->update(["gallerypackage_id" => $package_id]);*/
                        }
                    }

                    $name = Auth::user()->name;
                    $email = Auth::user()->email;

                    $from = Config::get("params.admin_email");

                    $content = Content::where("code", "=", "package_purchase")->get();
                    $replaces['NAME'] = $name;
                    $replaces['PACKAGE'] = $package->package_name;
                    $replaces['PRICE'] = $package->price;

                    $template = Functions::setEmailTemplate($content, $replaces);

                    $subject = $template["subject"];
                    $body = $template["body"];

                    //$send_email = Functions::sendEmail($email,$subject,$body,'',$from);
                }
                $card_no = str_replace(" ", "", $request->card_no);
                $data = ["card_name" => $request->card_name, "card_no" => $card_no,
                    "cvc_no" => $request->cvc_no, "month" => $request->month, "year" => $request->year,
                    "package_name" => $package->package_name, "total" => $package->price,
                    "description" => $package->description];

                if ($payment_type == 1) {
                    $response = AuthorizeNet::createSubscription($data);
                    if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
                        $sessionId = $this->session_id;
                        $carts = Cart::where("session_id", "=", $sessionId)
                                        ->where("package_id", "=", $package_id)->get();
                        $package_item = array();
                        $date = date("Y-m-d H:i:s");
                        foreach ($carts as $item) {
                            $package_item[] = array('billing_id' => $bill_id, 'user_id' => $user_id,
                                'service_areas' => $item->service_areas,
                                'states' => $item->states, 'county' => $item->county,
                                'gallery' => $item->gallery, 'created_at' => $date);
                        }
                        if (count($package_item) > 0) {
                            PackageCombination::insert($package_item);
                        }

                        
                        PackageCart::where("session_id", "=", $sessionId)->delete();
                        Cart::where("session_id", "=", $sessionId)->delete();

                        Session::forget("id");
                        Session::forget("user_id");
                        Session::forget("name");
                        DB::commit();
                        $subscription_id = $response->getSubscriptionId();
                        Session::put("bill_id", $bill_id);
                        Session::put("sub_id", $subscription_id);
                        Session::put("card_info", $data);
                        Session::put("vendor", "1");
                        Session::save();
                        session_regenerate_id();
                        $message = "SUCCESS: Subscription ID : " . $subscription_id . "\n";
                        return redirect('payment/thank-you')->with("success", $message);
                    } else {
                        DB::rollBack();
                        $error = "ERROR :  Invalid response\n";
                        $errorMessages = $response->getMessages()->getMessage();
                        $error .= "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
                        return redirect('payment/cancel')->with("error", $error);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return redirect('billing/cancel');
            }
        }
    }

    public function indexGallery(Request $request) {
        $package_id = ($request->package_id != '') ? $request->package_id : '';
        if (isset(Auth::user()->id)) {
            if ($package_id != '') {
                $page_title = "Billing Information";
                $months = Functions::getMonths();
                $year = date('Y');
                $user_id = Auth::user()->id;
                $code = $request->code;
                $provider = ServiceProvider::where("user_id", "=", $user_id)->first();
                $id = $provider->id;
                $name = $provider->name;
                $code = $request->code;
                return view('front.serviceprovider.billing', compact("page_title","code", "months", "year", "id", "name", "package_id"));
            }
            else
            {
                return redirect('/');
            }
        }
    }

}
