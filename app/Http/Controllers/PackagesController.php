<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input as Input;
use Validator,
    Redirect,
    Session,
    Config;
use App\Functions\Functions;
use App\Association;
use App\Packages;
use App\ServiceProvider;
use App\AssociationSubscriptions;
use App\VendorSubscriptions;

class PackagesController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | AssociationRegister Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */
    protected $session_id;
    public function __construct() {
        $this->middleware('auth');
        session_start();
	$this->session_id=session_id();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    public function index() 
    {
        $page_title = "Current Package";
        $user_id = Auth::user()->id;
        //$association_id = Association::getAssociationId($user_id);
        $model = AssociationSubscriptions::with("packages")
        ->where('user_id','=',$user_id)->orderby('id','desc')->first();
        $package_id = (count($model)>0)?$model->package_id:0;
        $package_name = (count($model)>0)?$model->packages->package_name:'';
        $exp_date = (count($model)>0)?date("d-m-Y",strtotime($model->subscription_date)):''; 
        $packages = Packages::where("id","!=",$package_id)->where("type","=",1)->get();
        $date = date("Y-m-d",strtotime('+1 months'));
        $curr_date = date("Y-m-d");
       
        return view('front.association.packages.index',compact("model","exp_date","package_id","date","curr_date","package_name","page_title","packages"));
    }
    public function indexVendor() 
    {
        $page_title = "Current Package";

        $user_id = Auth::user()->id;
        $model = VendorSubscriptions::with("packages")
        ->where('user_id','=',$user_id)->orderby('id','desc')->first();
        $package_id = (count($model)>0)?$model->package_id:0;
        $package_name = (count($model)>0)?$model->packages->package_name:'';
        $exp_date = (count($model)>0)?date("d-m-Y",strtotime($model->subscription_date)):''; 
        $packages = Packages::where("id","!=",$package_id)->where("type","=",2)->get();
        $date = date("Y-m-d",strtotime('+1 months'));
        $curr_date = date("Y-m-d"); 

        return view('front.serviceprovider.packages.index', compact("model", "exp_date", "date", "package_name", "page_title", "packages"));
    }

}
