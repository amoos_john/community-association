<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model {
	
    protected $table='billing';
    
    public static $payment_method=[""=>"Select Method","1"=>"Authorize","2"=>"Paypal","3"=>"ACH (Transaction Express)"];
    
     public function packages()
    {
        return $this->hasOne('App\Packages', 'id', 'package_id');
    }
}
