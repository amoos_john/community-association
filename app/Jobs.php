<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model {
	
    protected $table='jobs';
    
    public static $status=["1"=>"Success","0"=>"Pending"];
    
    public function association()
    {
        return $this->hasOne('App\Association', 'id', 'association_id');
    } 
    public static function getActiveJobs()
    {
        $result = Jobs::with("association")->where("status","=",1)
                ->where("deleted","!=",1)
                ->orderby("id","desc")->paginate(10);
        
        return $result;
    }
    
}
